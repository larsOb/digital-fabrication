!function(r, h, f) {
    function p(e, t) {
        return typeof e === t
    }
    function e() {
        var e,
            t,
            n,
            i,
            r,
            o;
        for (var s in w) {
            if (e = [], (t = w[s]).name && (e.push(t.name.toLowerCase()), t.options && t.options.aliases && t.options.aliases.length))
                for (n = 0; n < t.options.aliases.length; n++)
                    e.push(t.options.aliases[n].toLowerCase());
            for (i = p(t.fn, "function") ? t.fn() : t.fn, r = 0; r < e.length; r++)
                1 === (o = e[r].split(".")).length ? C[o[0]] = i : (!C[o[0]] || C[o[0]] instanceof Boolean || (C[o[0]] = new Boolean(C[o[0]])), C[o[0]][o[1]] = i), x.push((i ? "" : "no-") + o.join("-"))
        }
    }
    function t(e) {
        var t = b.className,
            n = C._config.classPrefix || "";
        if (c && (t = t.baseVal), C._config.enableJSClass) {
            var i = new RegExp("(^|\\s)" + n + "no-js(\\s|$)");
            t = t.replace(i, "$1" + n + "js$2")
        }
        C._config.enableClasses && (t += " " + n + e.join(" " + n), c ? b.className.baseVal = t : b.className = t)
    }
    function d(e) {
        return "function" != typeof h.createElement ? h.createElement(e) : c ? h.createElementNS.call(h, "http://www.w3.org/2000/svg", e) : h.createElement.apply(h, arguments)
    }
    function g(e) {
        return e.replace(/([a-z])-([a-z])/g, function(e, t, n) {
            return t + n.toUpperCase()
        }).replace(/^-/, "")
    }
    function o(e, t) {
        return function() {
            return e.apply(t, arguments)
        }
    }
    function a(e, t, n) {
        var i;
        for (var r in e)
            if (e[r] in t)
                return !1 === n ? e[r] : p(i = t[e[r]], "function") ? o(i, n || t) : i;
        return !1
    }
    function m(e, t) {
        return !!~("" + e).indexOf(t)
    }
    function s(e) {
        return e.replace(/([A-Z])/g, function(e, t) {
            return "-" + t.toLowerCase()
        }).replace(/^ms-/, "-ms-")
    }
    function y() {
        var e = h.body;
        return e || ((e = d(c ? "svg" : "body")).fake = !0), e
    }
    function u(e, t, n, i) {
        var r,
            o,
            s,
            a,
            u = "modernizr",
            l = d("div"),
            c = y();
        if (parseInt(n, 10))
            for (; n--;)
                (s = d("div")).id = i ? i[n] : u + (n + 1), l.appendChild(s);
        return (r = d("style")).type = "text/css", r.id = "s" + u, (c.fake ? c : l).appendChild(r), c.appendChild(l), r.styleSheet ? r.styleSheet.cssText = e : r.appendChild(h.createTextNode(e)), l.id = u, c.fake && (c.style.background = "", c.style.overflow = "hidden", a = b.style.overflow, b.style.overflow = "hidden", b.appendChild(c)), o = t(l, e), c.fake ? (c.parentNode.removeChild(c), b.style.overflow = a, b.offsetHeight) : l.parentNode.removeChild(l), !!o
    }
    function v(e, t) {
        var n = e.length;
        if ("CSS" in r && "supports" in r.CSS) {
            for (; n--;)
                if (r.CSS.supports(s(e[n]), t))
                    return !0;
            return !1
        }
        if ("CSSSupportsRule" in r) {
            for (var i = []; n--;)
                i.push("(" + s(e[n]) + ":" + t + ")");
            return u("@supports (" + (i = i.join(" or ")) + ") { #modernizr { position: absolute; } }", function(e) {
                return "absolute" == getComputedStyle(e, null).position
            })
        }
        return f
    }
    function l(e, t, n, i) {
        function r() {
            s && (delete I.style, delete I.modElem)
        }
        if (i = !p(i, "undefined") && i, !p(n, "undefined")) {
            var o = v(e, n);
            if (!p(o, "undefined"))
                return o
        }
        for (var s, a, u, l, c, h = ["modernizr", "tspan"]; !I.style;)
            s = !0, I.modElem = d(h.shift()), I.style = I.modElem.style;
        for (u = e.length, a = 0; a < u; a++)
            if (l = e[a], c = I.style[l], m(l, "-") && (l = g(l)), I.style[l] !== f) {
                if (i || p(n, "undefined"))
                    return r(), "pfx" != t || l;
                try {
                    I.style[l] = n
                } catch (x) {}
                if (I.style[l] != c)
                    return r(), "pfx" != t || l
            }
        return r(), !1
    }
    function i(e, t, n, i, r) {
        var o = e.charAt(0).toUpperCase() + e.slice(1),
            s = (e + " " + L.join(o + " ") + o).split(" ");
        return p(t, "string") || p(t, "undefined") ? l(s, t, i, r) : a(s = (e + " " + S.join(o + " ") + o).split(" "), t, n)
    }
    function n(e, t, n) {
        return i(e, f, f, t, n)
    }
    var x = [],
        b = h.documentElement,
        c = "svg" === b.nodeName.toLowerCase(),
        w = [],
        z = {
            _version: "3.0.0",
            _config: {
                classPrefix: "",
                enableClasses: !0,
                enableJSClass: !0,
                usePrefixes: !0
            },
            _q: [],
            on: function(e, t) {
                var n = this;
                setTimeout(function() {
                    t(n[e])
                }, 0)
            },
            addTest: function(e, t, n) {
                w.push({
                    name: e,
                    fn: t,
                    options: n
                })
            },
            addAsyncTest: function(e) {
                w.push({
                    name: null,
                    fn: e
                })
            }
        },
        C = function() {};
    C.prototype = z, (C = new C).addTest("dataset", function() {
        var e = d("div");
        return e.setAttribute("data-a-b", "c"), !(!e.dataset || "c" !== e.dataset.aB)
    });
    var E = "Moz O ms Webkit",
        L = z._config.usePrefixes ? E.split(" ") : [];
    z._cssomPrefixes = L;
    var S = z._config.usePrefixes ? E.toLowerCase().split(" ") : [];
    z._domPrefixes = S;
    var T = {
        elem: d("modernizr")
    };
    C._q.push(function() {
        delete T.elem
    });
    var I = {
        style: T.elem.style
    };
    C._q.unshift(function() {
        delete I.style
    }), z.testAllProps = i, z.testAllProps = n, C.addTest("flexbox", n("flexBasis", "1px", !0)), e(), t(x), delete z.addTest, delete z.addAsyncTest;
    for (var O = 0; O < C._q.length; O++)
        C._q[O]();
    r.Modernizr = C
}(window, document), function(e, t) {
    "use strict";
    "object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function(e) {
        if (!e.document)
            throw new Error("jQuery requires a window with a document");
        return t(e)
    } : t(e)
}("undefined" != typeof window ? window : this, function(E, e) {
    "use strict";
    function g(e, t) {
        var n = (t = t || ne).createElement("script");
        n.text = e, t.head.appendChild(n).parentNode.removeChild(n)
    }
    function a(e) {
        var t = !!e && "length" in e && e.length,
            n = ge.type(e);
        return "function" !== n && !ge.isWindow(e) && ("array" === n || 0 === t || "number" == typeof t && 0 < t && t - 1 in e)
    }
    function l(e, t) {
        return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
    }
    function t(e, n, i) {
        return ge.isFunction(n) ? ge.grep(e, function(e, t) {
            return !!n.call(e, t, e) !== i
        }) : n.nodeType ? ge.grep(e, function(e) {
            return e === n !== i
        }) : "string" != typeof n ? ge.grep(e, function(e) {
            return -1 < ae.call(n, e) !== i
        }) : Le.test(n) ? ge.filter(n, e, i) : (n = ge.filter(n, e), ge.grep(e, function(e) {
            return -1 < ae.call(n, e) !== i && 1 === e.nodeType
        }))
    }
    function n(e, t) {
        for (; (e = e[t]) && 1 !== e.nodeType;)
            ;
        return e
    }
    function c(e) {
        var n = {};
        return ge.each(e.match(_e) || [], function(e, t) {
            n[t] = !0
        }), n
    }
    function h(e) {
        return e
    }
    function f(e) {
        throw e
    }
    function u(e, t, n, i) {
        var r;
        try {
            e && ge.isFunction(r = e.promise) ? r.call(e).done(t).fail(n) : e && ge.isFunction(r = e.then) ? r.call(e, t, n) : t.apply(undefined, [e].slice(i))
        } catch (e) {
            n.apply(undefined, [e])
        }
    }
    function i() {
        ne.removeEventListener("DOMContentLoaded", i), E.removeEventListener("load", i), ge.ready()
    }
    function r() {
        this.expando = ge.expando + r.uid++
    }
    function o(e) {
        return "true" === e || "false" !== e && ("null" === e ? null : e === +e + "" ? +e : Re.test(e) ? JSON.parse(e) : e)
    }
    function p(e, t, n) {
        var i;
        if (n === undefined && 1 === e.nodeType)
            if (i = "data-" + t.replace(De, "-$&").toLowerCase(), "string" == typeof (n = e.getAttribute(i))) {
                try {
                    n = o(n)
                } catch (r) {}
                je.set(e, t, n)
            } else
                n = undefined;
        return n
    }
    function d(e, t, n, i) {
        var r,
            o = 1,
            s = 20,
            a = i ? function() {
                return i.cur()
            } : function() {
                return ge.css(e, t, "")
            },
            u = a(),
            l = n && n[3] || (ge.cssNumber[t] ? "" : "px"),
            c = (ge.cssNumber[t] || "px" !== l && +u) && Fe.exec(ge.css(e, t));
        if (c && c[3] !== l)
            for (l = l || c[3], n = n || [], c = +u || 1; c /= o = o || ".5", ge.style(e, t, c + l), o !== (o = a() / u) && 1 !== o && --s;)
                ;
        return n && (c = +c || +u || 0, r = n[1] ? c + (n[1] + 1) * n[2] : +n[2], i && (i.unit = l, i.start = c, i.end = r)), r
    }
    function m(e) {
        var t,
            n = e.ownerDocument,
            i = e.nodeName,
            r = Be[i];
        return r || (t = n.body.appendChild(n.createElement(i)), r = ge.css(t, "display"), t.parentNode.removeChild(t), "none" === r && (r = "block"), Be[i] = r)
    }
    function y(e, t) {
        for (var n, i, r = [], o = 0, s = e.length; o < s; o++)
            (i = e[o]).style && (n = i.style.display, t ? ("none" === n && (r[o] = We.get(i, "display") || null, r[o] || (i.style.display = "")), "" === i.style.display && $e(i) && (r[o] = m(i))) : "none" !== n && (r[o] = "none", We.set(i, "display", n)));
        for (o = 0; o < s; o++)
            null != r[o] && (e[o].style.display = r[o]);
        return e
    }
    function v(e, t) {
        var n;
        return n = "undefined" != typeof e.getElementsByTagName ? e.getElementsByTagName(t || "*") : "undefined" != typeof e.querySelectorAll ? e.querySelectorAll(t || "*") : [], t === undefined || t && l(e, t) ? ge.merge([e], n) : n
    }
    function x(e, t) {
        for (var n = 0, i = e.length; n < i; n++)
            We.set(e[n], "globalEval", !t || We.get(t[n], "globalEval"))
    }
    function b(e, t, n, i, r) {
        for (var o, s, a, u, l, c, h = t.createDocumentFragment(), f = [], p = 0, d = e.length; p < d; p++)
            if ((o = e[p]) || 0 === o)
                if ("object" === ge.type(o))
                    ge.merge(f, o.nodeType ? [o] : o);
                else if (Je.test(o)) {
                    for (s = s || h.appendChild(t.createElement("div")), a = (Ve.exec(o) || ["", ""])[1].toLowerCase(), u = Ye[a] || Ye._default, s.innerHTML = u[1] + ge.htmlPrefilter(o) + u[2], c = u[0]; c--;)
                        s = s.lastChild;
                    ge.merge(f, s.childNodes), (s = h.firstChild).textContent = ""
                } else
                    f.push(t.createTextNode(o));
        for (h.textContent = "", p = 0; o = f[p++];)
            if (i && -1 < ge.inArray(o, i))
                r && r.push(o);
            else if (l = ge.contains(o.ownerDocument, o), s = v(h.appendChild(o), "script"), l && x(s), n)
                for (c = 0; o = s[c++];)
                    Ge.test(o.type || "") && n.push(o);
        return h
    }
    function s() {
        return !0
    }
    function w() {
        return !1
    }
    function z() {
        try {
            return ne.activeElement
        } catch (e) {}
    }
    function C(e, t, n, i, r, o) {
        var s,
            a;
        if ("object" == typeof t) {
            for (a in "string" != typeof n && (i = i || n, n = undefined), t)
                C(e, a, n, i, t[a], o);
            return e
        }
        if (null == i && null == r ? (r = n, i = n = undefined) : null == r && ("string" == typeof n ? (r = i, i = undefined) : (r = i, i = n, n = undefined)), !1 === r)
            r = w;
        else if (!r)
            return e;
        return 1 === o && (s = r, (r = function(e) {
            return ge().off(e), s.apply(this, arguments)
        }).guid = s.guid || (s.guid = ge.guid++)), e.each(function() {
            ge.event.add(this, t, r, i, n)
        })
    }
    function L(e, t) {
        return l(e, "table") && l(11 !== t.nodeType ? t : t.firstChild, "tr") && ge(">tbody", e)[0] || e
    }
    function S(e) {
        return e.type = (null !== e.getAttribute("type")) + "/" + e.type, e
    }
    function T(e) {
        var t = ot.exec(e.type);
        return t ? e.type = t[1] : e.removeAttribute("type"), e
    }
    function I(e, t) {
        var n,
            i,
            r,
            o,
            s,
            a,
            u,
            l;
        if (1 === t.nodeType) {
            if (We.hasData(e) && (o = We.access(e), s = We.set(t, o), l = o.events))
                for (r in delete s.handle, s.events = {}, l)
                    for (n = 0, i = l[r].length; n < i; n++)
                        ge.event.add(t, r, l[r][n]);
            je.hasData(e) && (a = je.access(e), u = ge.extend({}, a), je.set(t, u))
        }
    }
    function O(e, t) {
        var n = t.nodeName.toLowerCase();
        "input" === n && Ue.test(e.type) ? t.checked = e.checked : "input" !== n && "textarea" !== n || (t.defaultValue = e.defaultValue)
    }
    function _(n, i, r, o) {
        i = oe.apply([], i);
        var e,
            t,
            s,
            a,
            u,
            l,
            c = 0,
            h = n.length,
            f = h - 1,
            p = i[0],
            d = ge.isFunction(p);
        if (d || 1 < h && "string" == typeof p && !pe.checkClone && rt.test(p))
            return n.each(function(e) {
                var t = n.eq(e);
                d && (i[0] = p.call(this, e, t.html())), _(t, i, r, o)
            });
        if (h && (t = (e = b(i, n[0].ownerDocument, !1, n, o)).firstChild, 1 === e.childNodes.length && (e = t), t || o)) {
            for (a = (s = ge.map(v(e, "script"), S)).length; c < h; c++)
                u = e, c !== f && (u = ge.clone(u, !0, !0), a && ge.merge(s, v(u, "script"))), r.call(n[c], u, c);
            if (a)
                for (l = s[s.length - 1].ownerDocument, ge.map(s, T), c = 0; c < a; c++)
                    u = s[c], Ge.test(u.type || "") && !We.access(u, "globalEval") && ge.contains(l, u) && (u.src ? ge._evalUrl && ge._evalUrl(u.src) : g(u.textContent.replace(st, ""), l))
        }
        return n
    }
    function k(e, t, n) {
        for (var i, r = t ? ge.filter(t, e) : e, o = 0; null != (i = r[o]); o++)
            n || 1 !== i.nodeType || ge.cleanData(v(i)), i.parentNode && (n && ge.contains(i.ownerDocument, i) && x(v(i, "script")), i.parentNode.removeChild(i));
        return e
    }
    function H(e, t, n) {
        var i,
            r,
            o,
            s,
            a = e.style;
        return (n = n || lt(e)) && ("" !== (s = n.getPropertyValue(t) || n[t]) || ge.contains(e.ownerDocument, e) || (s = ge.style(e, t)), !pe.pixelMarginRight() && ut.test(s) && at.test(t) && (i = a.width, r = a.minWidth, o = a.maxWidth, a.minWidth = a.maxWidth = a.width = s, s = n.width, a.width = i, a.minWidth = r, a.maxWidth = o)), s !== undefined ? s + "" : s
    }
    function q(e, t) {
        return {
            get: function() {
                if (!e())
                    return (this.get = t).apply(this, arguments);
                delete this.get
            }
        }
    }
    function A(e) {
        if (e in gt)
            return e;
        for (var t = e[0].toUpperCase() + e.slice(1), n = dt.length; n--;)
            if ((e = dt[n] + t) in gt)
                return e
    }
    function W(e) {
        var t = ge.cssProps[e];
        return t || (t = ge.cssProps[e] = A(e) || e), t
    }
    function j(e, t, n) {
        var i = Fe.exec(t);
        return i ? Math.max(0, i[2] - (n || 0)) + (i[3] || "px") : t
    }
    function R(e, t, n, i, r) {
        var o,
            s = 0;
        for (o = n === (i ? "border" : "content") ? 4 : "width" === t ? 1 : 0; o < 4; o += 2)
            "margin" === n && (s += ge.css(e, n + Pe[o], !0, r)), i ? ("content" === n && (s -= ge.css(e, "padding" + Pe[o], !0, r)), "margin" !== n && (s -= ge.css(e, "border" + Pe[o] + "Width", !0, r))) : (s += ge.css(e, "padding" + Pe[o], !0, r), "padding" !== n && (s += ge.css(e, "border" + Pe[o] + "Width", !0, r)));
        return s
    }
    function D(e, t, n) {
        var i,
            r = lt(e),
            o = H(e, t, r),
            s = "border-box" === ge.css(e, "boxSizing", !1, r);
        return ut.test(o) ? o : (i = s && (pe.boxSizingReliable() || o === e.style[t]), "auto" === o && (o = e["offset" + t[0].toUpperCase() + t.slice(1)]), (o = parseFloat(o) || 0) + R(e, t, n || (s ? "border" : "content"), i, r) + "px")
    }
    function N(e, t, n, i, r) {
        return new N.prototype.init(e, t, n, i, r)
    }
    function F() {
        yt && (!1 === ne.hidden && E.requestAnimationFrame ? E.requestAnimationFrame(F) : E.setTimeout(F, ge.fx.interval), ge.fx.tick())
    }
    function P() {
        return E.setTimeout(function() {
            mt = undefined
        }), mt = ge.now()
    }
    function $(e, t) {
        var n,
            i = 0,
            r = {
                height: e
            };
        for (t = t ? 1 : 0; i < 4; i += 2 - t)
            r["margin" + (n = Pe[i])] = r["padding" + n] = e;
        return t && (r.opacity = r.width = e), r
    }
    function M(e, t, n) {
        for (var i, r = (V.tweeners[t] || []).concat(V.tweeners["*"]), o = 0, s = r.length; o < s; o++)
            if (i = r[o].call(n, t, e))
                return i
    }
    function B(e, t, n) {
        var i,
            r,
            o,
            s,
            a,
            u,
            l,
            c,
            h = "width" in t || "height" in t,
            f = this,
            p = {},
            d = e.style,
            g = e.nodeType && $e(e),
            m = We.get(e, "fxshow");
        for (i in n.queue || (null == (s = ge._queueHooks(e, "fx")).unqueued && (s.unqueued = 0, a = s.empty.fire, s.empty.fire = function() {
            s.unqueued || a()
        }), s.unqueued++, f.always(function() {
            f.always(function() {
                s.unqueued--, ge.queue(e, "fx").length || s.empty.fire()
            })
        })), t)
            if (r = t[i], bt.test(r)) {
                if (delete t[i], o = o || "toggle" === r, r === (g ? "hide" : "show")) {
                    if ("show" !== r || !m || m[i] === undefined)
                        continue;
                    g = !0
                }
                p[i] = m && m[i] || ge.style(e, i)
            }
        if ((u = !ge.isEmptyObject(t)) || !ge.isEmptyObject(p))
            for (i in h && 1 === e.nodeType && (n.overflow = [d.overflow, d.overflowX, d.overflowY], null == (l = m && m.display) && (l = We.get(e, "display")), "none" === (c = ge.css(e, "display")) && (l ? c = l : (y([e], !0), l = e.style.display || l, c = ge.css(e, "display"), y([e]))), ("inline" === c || "inline-block" === c && null != l) && "none" === ge.css(e, "float") && (u || (f.done(function() {
                d.display = l
            }), null == l && (c = d.display, l = "none" === c ? "" : c)), d.display = "inline-block")), n.overflow && (d.overflow = "hidden", f.always(function() {
                d.overflow = n.overflow[0], d.overflowX = n.overflow[1], d.overflowY = n.overflow[2]
            })), u = !1, p)
                u || (m ? "hidden" in m && (g = m.hidden) : m = We.access(e, "fxshow", {
                    display: l
                }), o && (m.hidden = !g), g && y([e], !0), f.done(function() {
                    for (i in g || y([e]), We.remove(e, "fxshow"), p)
                        ge.style(e, i, p[i])
                })), u = M(g ? m[i] : 0, i, f), i in m || (m[i] = u.start, g && (u.end = u.start, u.start = 0))
    }
    function U(e, t) {
        var n,
            i,
            r,
            o,
            s;
        for (n in e)
            if (r = t[i = ge.camelCase(n)], o = e[n], Array.isArray(o) && (r = o[1], o = e[n] = o[0]), n !== i && (e[i] = o, delete e[n]), (s = ge.cssHooks[i]) && "expand" in s)
                for (n in o = s.expand(o), delete e[i], o)
                    n in e || (e[n] = o[n], t[n] = r);
            else
                t[i] = r
    }
    function V(o, e, t) {
        var n,
            s,
            i = 0,
            r = V.prefilters.length,
            a = ge.Deferred().always(function() {
                delete u.elem
            }),
            u = function() {
                if (s)
                    return !1;
                for (var e = mt || P(), t = Math.max(0, l.startTime + l.duration - e), n = 1 - (t / l.duration || 0), i = 0, r = l.tweens.length; i < r; i++)
                    l.tweens[i].run(n);
                return a.notifyWith(o, [l, n, t]), n < 1 && r ? t : (r || a.notifyWith(o, [l, 1, 0]), a.resolveWith(o, [l]), !1)
            },
            l = a.promise({
                elem: o,
                props: ge.extend({}, e),
                opts: ge.extend(!0, {
                    specialEasing: {},
                    easing: ge.easing._default
                }, t),
                originalProperties: e,
                originalOptions: t,
                startTime: mt || P(),
                duration: t.duration,
                tweens: [],
                createTween: function(e, t) {
                    var n = ge.Tween(o, l.opts, e, t, l.opts.specialEasing[e] || l.opts.easing);
                    return l.tweens.push(n), n
                },
                stop: function(e) {
                    var t = 0,
                        n = e ? l.tweens.length : 0;
                    if (s)
                        return this;
                    for (s = !0; t < n; t++)
                        l.tweens[t].run(1);
                    return e ? (a.notifyWith(o, [l, 1, 0]), a.resolveWith(o, [l, e])) : a.rejectWith(o, [l, e]), this
                }
            }),
            c = l.props;
        for (U(c, l.opts.specialEasing); i < r; i++)
            if (n = V.prefilters[i].call(l, o, c, l.opts))
                return ge.isFunction(n.stop) && (ge._queueHooks(l.elem, l.opts.queue).stop = ge.proxy(n.stop, n)), n;
        return ge.map(c, M, l), ge.isFunction(l.opts.start) && l.opts.start.call(o, l), l.progress(l.opts.progress).done(l.opts.done, l.opts.complete).fail(l.opts.fail).always(l.opts.always), ge.fx.timer(ge.extend(u, {
            elem: o,
            anim: l,
            queue: l.opts.queue
        })), l
    }
    function G(e) {
        return (e.match(_e) || []).join(" ")
    }
    function Y(e) {
        return e.getAttribute && e.getAttribute("class") || ""
    }
    function X(n, e, i, r) {
        var t;
        if (Array.isArray(e))
            ge.each(e, function(e, t) {
                i || kt.test(n) ? r(n, t) : X(n + "[" + ("object" == typeof t && null != t ? e : "") + "]", t, i, r)
            });
        else if (i || "object" !== ge.type(e))
            r(n, e);
        else
            for (t in e)
                X(n + "[" + t + "]", e[t], i, r)
    }
    function Q(o) {
        return function(e, t) {
            "string" != typeof e && (t = e, e = "*");
            var n,
                i = 0,
                r = e.toLowerCase().match(_e) || [];
            if (ge.isFunction(t))
                for (; n = r[i++];)
                    "+" === n[0] ? (n = n.slice(1) || "*", (o[n] = o[n] || []).unshift(t)) : (o[n] = o[n] || []).push(t)
        }
    }
    function J(t, r, o, s) {
        function a(e) {
            var i;
            return u[e] = !0, ge.each(t[e] || [], function(e, t) {
                var n = t(r, o, s);
                return "string" != typeof n || l || u[n] ? l ? !(i = n) : void 0 : (r.dataTypes.unshift(n), a(n), !1)
            }), i
        }
        var u = {},
            l = t === Mt;
        return a(r.dataTypes[0]) || !u["*"] && a("*")
    }
    function K(e, t) {
        var n,
            i,
            r = ge.ajaxSettings.flatOptions || {};
        for (n in t)
            t[n] !== undefined && ((r[n] ? e : i || (i = {}))[n] = t[n]);
        return i && ge.extend(!0, e, i), e
    }
    function Z(e, t, n) {
        for (var i, r, o, s, a = e.contents, u = e.dataTypes; "*" === u[0];)
            u.shift(), i === undefined && (i = e.mimeType || t.getResponseHeader("Content-Type"));
        if (i)
            for (r in a)
                if (a[r] && a[r].test(i)) {
                    u.unshift(r);
                    break
                }
        if (u[0] in n)
            o = u[0];
        else {
            for (r in n) {
                if (!u[0] || e.converters[r + " " + u[0]]) {
                    o = r;
                    break
                }
                s || (s = r)
            }
            o = o || s
        }
        if (o)
            return o !== u[0] && u.unshift(o), n[o]
    }
    function ee(e, t, n, i) {
        var r,
            o,
            s,
            a,
            u,
            l = {},
            c = e.dataTypes.slice();
        if (c[1])
            for (s in e.converters)
                l[s.toLowerCase()] = e.converters[s];
        for (o = c.shift(); o;)
            if (e.responseFields[o] && (n[e.responseFields[o]] = t), !u && i && e.dataFilter && (t = e.dataFilter(t, e.dataType)), u = o, o = c.shift())
                if ("*" === o)
                    o = u;
                else if ("*" !== u && u !== o) {
                    if (!(s = l[u + " " + o] || l["* " + o]))
                        for (r in l)
                            if ((a = r.split(" "))[1] === o && (s = l[u + " " + a[0]] || l["* " + a[0]])) {
                                !0 === s ? s = l[r] : !0 !== l[r] && (o = a[0], c.unshift(a[1]));
                                break
                            }
                    if (!0 !== s)
                        if (s && e["throws"])
                            t = s(t);
                        else
                            try {
                                t = s(t)
                            } catch (h) {
                                return {
                                    state: "parsererror",
                                    error: s ? h : "No conversion from " + u + " to " + o
                                }
                            }
                }
        return {
            state: "success",
            data: t
        }
    }
    var te = [],
        ne = E.document,
        ie = Object.getPrototypeOf,
        re = te.slice,
        oe = te.concat,
        se = te.push,
        ae = te.indexOf,
        ue = {},
        le = ue.toString,
        ce = ue.hasOwnProperty,
        he = ce.toString,
        fe = he.call(Object),
        pe = {},
        de = "3.2.1",
        ge = function(e, t) {
            return new ge.fn.init(e, t)
        },
        me = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
        ye = /^-ms-/,
        ve = /-([a-z])/g,
        xe = function(e, t) {
            return t.toUpperCase()
        };
    ge.fn = ge.prototype = {
        jquery: de,
        constructor: ge,
        length: 0,
        toArray: function() {
            return re.call(this)
        },
        get: function(e) {
            return null == e ? re.call(this) : e < 0 ? this[e + this.length] : this[e]
        },
        pushStack: function(e) {
            var t = ge.merge(this.constructor(), e);
            return t.prevObject = this, t
        },
        each: function(e) {
            return ge.each(this, e)
        },
        map: function(n) {
            return this.pushStack(ge.map(this, function(e, t) {
                return n.call(e, t, e)
            }))
        },
        slice: function() {
            return this.pushStack(re.apply(this, arguments))
        },
        first: function() {
            return this.eq(0)
        },
        last: function() {
            return this.eq(-1)
        },
        eq: function(e) {
            var t = this.length,
                n = +e + (e < 0 ? t : 0);
            return this.pushStack(0 <= n && n < t ? [this[n]] : [])
        },
        end: function() {
            return this.prevObject || this.constructor()
        },
        push: se,
        sort: te.sort,
        splice: te.splice
    }, ge.extend = ge.fn.extend = function(e) {
        var t,
            n,
            i,
            r,
            o,
            s,
            a = e || {},
            u = 1,
            l = arguments.length,
            c = !1;
        for ("boolean" == typeof a && (c = a, a = arguments[u] || {}, u++), "object" == typeof a || ge.isFunction(a) || (a = {}), u === l && (a = this, u--); u < l; u++)
            if (null != (t = arguments[u]))
                for (n in t)
                    i = a[n], a !== (r = t[n]) && (c && r && (ge.isPlainObject(r) || (o = Array.isArray(r))) ? (o ? (o = !1, s = i && Array.isArray(i) ? i : []) : s = i && ge.isPlainObject(i) ? i : {}, a[n] = ge.extend(c, s, r)) : r !== undefined && (a[n] = r));
        return a
    }, ge.extend({
        expando: "jQuery" + (de + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function(e) {
            throw new Error(e)
        },
        noop: function() {},
        isFunction: function(e) {
            return "function" === ge.type(e)
        },
        isWindow: function(e) {
            return null != e && e === e.window
        },
        isNumeric: function(e) {
            var t = ge.type(e);
            return ("number" === t || "string" === t) && !isNaN(e - parseFloat(e))
        },
        isPlainObject: function(e) {
            var t,
                n;
            return !(!e || "[object Object]" !== le.call(e)) && (!(t = ie(e)) || "function" == typeof (n = ce.call(t, "constructor") && t.constructor) && he.call(n) === fe)
        },
        isEmptyObject: function(e) {
            var t;
            for (t in e)
                return !1;
            return !0
        },
        type: function(e) {
            return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? ue[le.call(e)] || "object" : typeof e
        },
        globalEval: function(e) {
            g(e)
        },
        camelCase: function(e) {
            return e.replace(ye, "ms-").replace(ve, xe)
        },
        each: function(e, t) {
            var n,
                i = 0;
            if (a(e))
                for (n = e.length; i < n && !1 !== t.call(e[i], i, e[i]); i++)
                    ;
            else
                for (i in e)
                    if (!1 === t.call(e[i], i, e[i]))
                        break;
            return e
        },
        trim: function(e) {
            return null == e ? "" : (e + "").replace(me, "")
        },
        makeArray: function(e, t) {
            var n = t || [];
            return null != e && (a(Object(e)) ? ge.merge(n, "string" == typeof e ? [e] : e) : se.call(n, e)), n
        },
        inArray: function(e, t, n) {
            return null == t ? -1 : ae.call(t, e, n)
        },
        merge: function(e, t) {
            for (var n = +t.length, i = 0, r = e.length; i < n; i++)
                e[r++] = t[i];
            return e.length = r, e
        },
        grep: function(e, t, n) {
            for (var i = [], r = 0, o = e.length, s = !n; r < o; r++)
                !t(e[r], r) !== s && i.push(e[r]);
            return i
        },
        map: function(e, t, n) {
            var i,
                r,
                o = 0,
                s = [];
            if (a(e))
                for (i = e.length; o < i; o++)
                    null != (r = t(e[o], o, n)) && s.push(r);
            else
                for (o in e)
                    null != (r = t(e[o], o, n)) && s.push(r);
            return oe.apply([], s)
        },
        guid: 1,
        proxy: function(e, t) {
            var n,
                i,
                r;
            return "string" == typeof t && (n = e[t], t = e, e = n), ge.isFunction(e) ? (i = re.call(arguments, 2), (r = function() {
                return e.apply(t || this, i.concat(re.call(arguments)))
            }).guid = e.guid = e.guid || ge.guid++, r) : undefined
        },
        now: Date.now,
        support: pe
    }), "function" == typeof Symbol && (ge.fn[Symbol.iterator] = te[Symbol.iterator]), ge.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function(e, t) {
        ue["[object " + t + "]"] = t.toLowerCase()
    });
    var be = function(n) {
        function b(e, t, n, i) {
            var r,
                o,
                s,
                a,
                u,
                l,
                c,
                h = t && t.ownerDocument,
                f = t ? t.nodeType : 9;
            if (n = n || [], "string" != typeof e || !e || 1 !== f && 9 !== f && 11 !== f)
                return n;
            if (!i && ((t ? t.ownerDocument || t : P) !== q && H(t), t = t || q, W)) {
                if (11 !== f && (u = ye.exec(e)))
                    if (r = u[1]) {
                        if (9 === f) {
                            if (!(s = t.getElementById(r)))
                                return n;
                            if (s.id === r)
                                return n.push(s), n
                        } else if (h && (s = h.getElementById(r)) && N(t, s) && s.id === r)
                            return n.push(s), n
                    } else {
                        if (u[2])
                            return K.apply(n, t.getElementsByTagName(e)), n;
                        if ((r = u[3]) && z.getElementsByClassName && t.getElementsByClassName)
                            return K.apply(n, t.getElementsByClassName(r)), n
                    }
                if (z.qsa && !V[e + " "] && (!j || !j.test(e))) {
                    if (1 !== f)
                        h = t, c = e;
                    else if ("object" !== t.nodeName.toLowerCase()) {
                        for ((a = t.getAttribute("id")) ? a = a.replace(we, ze) : t.setAttribute("id", a = F), o = (l = S(e)).length; o--;)
                            l[o] = "#" + a + " " + g(l[o]);
                        c = l.join(","), h = ve.test(e) && d(t.parentNode) || t
                    }
                    if (c)
                        try {
                            return K.apply(n, h.querySelectorAll(c)), n
                        } catch (p) {} finally {
                            a === F && t.removeAttribute("id")
                        }
                }
            }
            return I(e.replace(ae, "$1"), t, n, i)
        }
        function e() {
            function n(e, t) {
                return i.push(e + " ") > C.cacheLength && delete n[i.shift()], n[e + " "] = t
            }
            var i = [];
            return n
        }
        function u(e) {
            return e[F] = !0, e
        }
        function r(e) {
            var t = q.createElement("fieldset");
            try {
                return !!e(t)
            } catch (n) {
                return !1
            } finally {
                t.parentNode && t.parentNode.removeChild(t), t = null
            }
        }
        function t(e, t) {
            for (var n = e.split("|"), i = n.length; i--;)
                C.attrHandle[n[i]] = t
        }
        function l(e, t) {
            var n = t && e,
                i = n && 1 === e.nodeType && 1 === t.nodeType && e.sourceIndex - t.sourceIndex;
            if (i)
                return i;
            if (n)
                for (; n = n.nextSibling;)
                    if (n === t)
                        return -1;
            return e ? 1 : -1
        }
        function i(t) {
            return function(e) {
                return "input" === e.nodeName.toLowerCase() && e.type === t
            }
        }
        function o(n) {
            return function(e) {
                var t = e.nodeName.toLowerCase();
                return ("input" === t || "button" === t) && e.type === n
            }
        }
        function s(t) {
            return function(e) {
                return "form" in e ? e.parentNode && !1 === e.disabled ? "label" in e ? "label" in e.parentNode ? e.parentNode.disabled === t : e.disabled === t : e.isDisabled === t || e.isDisabled !== !t && Ee(e) === t : e.disabled === t : "label" in e && e.disabled === t
            }
        }
        function a(s) {
            return u(function(o) {
                return o = +o, u(function(e, t) {
                    for (var n, i = s([], e.length, o), r = i.length; r--;)
                        e[n = i[r]] && (e[n] = !(t[n] = e[n]))
                })
            })
        }
        function d(e) {
            return e && "undefined" != typeof e.getElementsByTagName && e
        }
        function c() {}
        function g(e) {
            for (var t = 0, n = e.length, i = ""; t < n; t++)
                i += e[t].value;
            return i
        }
        function h(a, e, t) {
            var u = e.dir,
                l = e.next,
                c = l || u,
                h = t && "parentNode" === c,
                f = M++;
            return e.first ? function(e, t, n) {
                for (; e = e[u];)
                    if (1 === e.nodeType || h)
                        return a(e, t, n);
                return !1
            } : function(e, t, n) {
                var i,
                    r,
                    o,
                    s = [$, f];
                if (n) {
                    for (; e = e[u];)
                        if ((1 === e.nodeType || h) && a(e, t, n))
                            return !0
                } else
                    for (; e = e[u];)
                        if (1 === e.nodeType || h)
                            if (r = (o = e[F] || (e[F] = {}))[e.uniqueID] || (o[e.uniqueID] = {}), l && l === e.nodeName.toLowerCase())
                                e = e[u] || e;
                            else {
                                if ((i = r[c]) && i[0] === $ && i[1] === f)
                                    return s[2] = i[2];
                                if ((r[c] = s)[2] = a(e, t, n))
                                    return !0
                            }
                return !1
            }
        }
        function f(r) {
            return 1 < r.length ? function(e, t, n) {
                for (var i = r.length; i--;)
                    if (!r[i](e, t, n))
                        return !1;
                return !0
            } : r[0]
        }
        function v(e, t, n) {
            for (var i = 0, r = t.length; i < r; i++)
                b(e, t[i], n);
            return n
        }
        function w(e, t, n, i, r) {
            for (var o, s = [], a = 0, u = e.length, l = null != t; a < u; a++)
                (o = e[a]) && (n && !n(o, i, r) || (s.push(o), l && t.push(a)));
            return s
        }
        function x(p, d, g, m, y, e) {
            return m && !m[F] && (m = x(m)), y && !y[F] && (y = x(y, e)), u(function(e, t, n, i) {
                var r,
                    o,
                    s,
                    a = [],
                    u = [],
                    l = t.length,
                    c = e || v(d || "*", n.nodeType ? [n] : n, []),
                    h = !p || !e && d ? c : w(c, a, p, n, i),
                    f = g ? y || (e ? p : l || m) ? [] : t : h;
                if (g && g(h, f, n, i), m)
                    for (r = w(f, u), m(r, [], n, i), o = r.length; o--;)
                        (s = r[o]) && (f[u[o]] = !(h[u[o]] = s));
                if (e) {
                    if (y || p) {
                        if (y) {
                            for (r = [], o = f.length; o--;)
                                (s = f[o]) && r.push(h[o] = s);
                            y(null, f = [], r, i)
                        }
                        for (o = f.length; o--;)
                            (s = f[o]) && -1 < (r = y ? ee(e, s) : a[o]) && (e[r] = !(t[r] = s))
                    }
                } else
                    f = w(f === t ? f.splice(l, f.length) : f), y ? y(null, t, f, i) : K.apply(t, f)
            })
        }
        function p(e) {
            for (var r, t, n, i = e.length, o = C.relative[e[0].type], s = o || C.relative[" "], a = o ? 1 : 0, u = h(function(e) {
                    return e === r
                }, s, !0), l = h(function(e) {
                    return -1 < ee(r, e)
                }, s, !0), c = [function(e, t, n) {
                    var i = !o && (n || t !== O) || ((r = t).nodeType ? u(e, t, n) : l(e, t, n));
                    return r = null, i
                }]; a < i; a++)
                if (t = C.relative[e[a].type])
                    c = [h(f(c), t)];
                else {
                    if ((t = C.filter[e[a].type].apply(null, e[a].matches))[F]) {
                        for (n = ++a; n < i && !C.relative[e[n].type]; n++)
                            ;
                        return x(1 < a && f(c), 1 < a && g(e.slice(0, a - 1).concat({
                            value: " " === e[a - 2].type ? "*" : ""
                        })).replace(ae, "$1"), t, a < n && p(e.slice(a, n)), n < i && p(e = e.slice(n)), n < i && g(e))
                    }
                    c.push(t)
                }
            return f(c)
        }
        function m(m, y) {
            var v = 0 < y.length,
                x = 0 < m.length,
                e = function(e, t, n, i, r) {
                    var o,
                        s,
                        a,
                        u = 0,
                        l = "0",
                        c = e && [],
                        h = [],
                        f = O,
                        p = e || x && C.find.TAG("*", r),
                        d = $ += null == f ? 1 : Math.random() || .1,
                        g = p.length;
                    for (r && (O = t === q || t || r); l !== g && null != (o = p[l]); l++) {
                        if (x && o) {
                            for (s = 0, t || o.ownerDocument === q || (H(o), n = !W); a = m[s++];)
                                if (a(o, t || q, n)) {
                                    i.push(o);
                                    break
                                }
                            r && ($ = d)
                        }
                        v && ((o = !a && o) && u--, e && c.push(o))
                    }
                    if (u += l, v && l !== u) {
                        for (s = 0; a = y[s++];)
                            a(c, h, t, n);
                        if (e) {
                            if (0 < u)
                                for (; l--;)
                                    c[l] || h[l] || (h[l] = Q.call(i));
                            h = w(h)
                        }
                        K.apply(i, h), r && !e && 0 < h.length && 1 < u + y.length && b.uniqueSort(i)
                    }
                    return r && ($ = d, O = f), c
                };
            return v ? u(e) : e
        }
        var y,
            z,
            C,
            E,
            L,
            S,
            T,
            I,
            O,
            _,
            k,
            H,
            q,
            A,
            W,
            j,
            R,
            D,
            N,
            F = "sizzle" + 1 * new Date,
            P = n.document,
            $ = 0,
            M = 0,
            B = e(),
            U = e(),
            V = e(),
            G = function(e, t) {
                return e === t && (k = !0), 0
            },
            Y = {}.hasOwnProperty,
            X = [],
            Q = X.pop,
            J = X.push,
            K = X.push,
            Z = X.slice,
            ee = function(e, t) {
                for (var n = 0, i = e.length; n < i; n++)
                    if (e[n] === t)
                        return n;
                return -1
            },
            te = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            ne = "[\\x20\\t\\r\\n\\f]",
            ie = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
            re = "\\[" + ne + "*(" + ie + ")(?:" + ne + "*([*^$|!~]?=)" + ne + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + ie + "))|)" + ne + "*\\]",
            oe = ":(" + ie + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + re + ")*)|.*)\\)|)",
            se = new RegExp(ne + "+", "g"),
            ae = new RegExp("^" + ne + "+|((?:^|[^\\\\])(?:\\\\.)*)" + ne + "+$", "g"),
            ue = new RegExp("^" + ne + "*," + ne + "*"),
            le = new RegExp("^" + ne + "*([>+~]|" + ne + ")" + ne + "*"),
            ce = new RegExp("=" + ne + "*([^\\]'\"]*?)" + ne + "*\\]", "g"),
            he = new RegExp(oe),
            fe = new RegExp("^" + ie + "$"),
            pe = {
                ID: new RegExp("^#(" + ie + ")"),
                CLASS: new RegExp("^\\.(" + ie + ")"),
                TAG: new RegExp("^(" + ie + "|[*])"),
                ATTR: new RegExp("^" + re),
                PSEUDO: new RegExp("^" + oe),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + ne + "*(even|odd|(([+-]|)(\\d*)n|)" + ne + "*(?:([+-]|)" + ne + "*(\\d+)|))" + ne + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + te + ")$", "i"),
                needsContext: new RegExp("^" + ne + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + ne + "*((?:-\\d)?\\d*)" + ne + "*\\)|)(?=[^-]|$)", "i")
            },
            de = /^(?:input|select|textarea|button)$/i,
            ge = /^h\d$/i,
            me = /^[^{]+\{\s*\[native \w/,
            ye = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            ve = /[+~]/,
            xe = new RegExp("\\\\([\\da-f]{1,6}" + ne + "?|(" + ne + ")|.)", "ig"),
            be = function(e, t, n) {
                var i = "0x" + t - 65536;
                return i != i || n ? t : i < 0 ? String.fromCharCode(i + 65536) : String.fromCharCode(i >> 10 | 55296, 1023 & i | 56320)
            },
            we = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
            ze = function(e, t) {
                return t ? "\0" === e ? "\ufffd" : e.slice(0, -1) + "\\" + e.charCodeAt(e.length - 1).toString(16) + " " : "\\" + e
            },
            Ce = function() {
                H()
            },
            Ee = h(function(e) {
                return !0 === e.disabled && ("form" in e || "label" in e)
            }, {
                dir: "parentNode",
                next: "legend"
            });
        try {
            K.apply(X = Z.call(P.childNodes), P.childNodes), X[P.childNodes.length].nodeType
        } catch (Le) {
            K = {
                apply: X.length ? function(e, t) {
                    J.apply(e, Z.call(t))
                } : function(e, t) {
                    for (var n = e.length, i = 0; e[n++] = t[i++];)
                        ;
                    e.length = n - 1
                }
            }
        }
        for (y in z = b.support = {}, L = b.isXML = function(e) {
            var t = e && (e.ownerDocument || e).documentElement;
            return !!t && "HTML" !== t.nodeName
        }, H = b.setDocument = function(e) {
            var t,
                n,
                i = e ? e.ownerDocument || e : P;
            return i !== q && 9 === i.nodeType && i.documentElement && (A = (q = i).documentElement, W = !L(q), P !== q && (n = q.defaultView) && n.top !== n && (n.addEventListener ? n.addEventListener("unload", Ce, !1) : n.attachEvent && n.attachEvent("onunload", Ce)), z.attributes = r(function(e) {
                return e.className = "i", !e.getAttribute("className")
            }), z.getElementsByTagName = r(function(e) {
                return e.appendChild(q.createComment("")), !e.getElementsByTagName("*").length
            }), z.getElementsByClassName = me.test(q.getElementsByClassName), z.getById = r(function(e) {
                return A.appendChild(e).id = F, !q.getElementsByName || !q.getElementsByName(F).length
            }), z.getById ? (C.filter.ID = function(e) {
                var t = e.replace(xe, be);
                return function(e) {
                    return e.getAttribute("id") === t
                }
            }, C.find.ID = function(e, t) {
                if ("undefined" != typeof t.getElementById && W) {
                    var n = t.getElementById(e);
                    return n ? [n] : []
                }
            }) : (C.filter.ID = function(e) {
                var n = e.replace(xe, be);
                return function(e) {
                    var t = "undefined" != typeof e.getAttributeNode && e.getAttributeNode("id");
                    return t && t.value === n
                }
            }, C.find.ID = function(e, t) {
                if ("undefined" != typeof t.getElementById && W) {
                    var n,
                        i,
                        r,
                        o = t.getElementById(e);
                    if (o) {
                        if ((n = o.getAttributeNode("id")) && n.value === e)
                            return [o];
                        for (r = t.getElementsByName(e), i = 0; o = r[i++];)
                            if ((n = o.getAttributeNode("id")) && n.value === e)
                                return [o]
                    }
                    return []
                }
            }), C.find.TAG = z.getElementsByTagName ? function(e, t) {
                return "undefined" != typeof t.getElementsByTagName ? t.getElementsByTagName(e) : z.qsa ? t.querySelectorAll(e) : void 0
            } : function(e, t) {
                var n,
                    i = [],
                    r = 0,
                    o = t.getElementsByTagName(e);
                if ("*" !== e)
                    return o;
                for (; n = o[r++];)
                    1 === n.nodeType && i.push(n);
                return i
            }, C.find.CLASS = z.getElementsByClassName && function(e, t) {
                if ("undefined" != typeof t.getElementsByClassName && W)
                    return t.getElementsByClassName(e)
            }, R = [], j = [], (z.qsa = me.test(q.querySelectorAll)) && (r(function(e) {
                A.appendChild(e).innerHTML = "<a id='" + F + "'></a><select id='" + F + "-\r\\' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && j.push("[*^$]=" + ne + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || j.push("\\[" + ne + "*(?:value|" + te + ")"), e.querySelectorAll("[id~=" + F + "-]").length || j.push("~="), e.querySelectorAll(":checked").length || j.push(":checked"), e.querySelectorAll("a#" + F + "+*").length || j.push(".#.+[+~]")
            }), r(function(e) {
                e.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                var t = q.createElement("input");
                t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && j.push("name" + ne + "*[*^$|!~]?="), 2 !== e.querySelectorAll(":enabled").length && j.push(":enabled", ":disabled"), A.appendChild(e).disabled = !0, 2 !== e.querySelectorAll(":disabled").length && j.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), j.push(",.*:")
            })), (z.matchesSelector = me.test(D = A.matches || A.webkitMatchesSelector || A.mozMatchesSelector || A.oMatchesSelector || A.msMatchesSelector)) && r(function(e) {
                z.disconnectedMatch = D.call(e, "*"), D.call(e, "[s!='']:x"), R.push("!=", oe)
            }), j = j.length && new RegExp(j.join("|")), R = R.length && new RegExp(R.join("|")), t = me.test(A.compareDocumentPosition), N = t || me.test(A.contains) ? function(e, t) {
                var n = 9 === e.nodeType ? e.documentElement : e,
                    i = t && t.parentNode;
                return e === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(i)))
            } : function(e, t) {
                if (t)
                    for (; t = t.parentNode;)
                        if (t === e)
                            return !0;
                return !1
            }, G = t ? function(e, t) {
                if (e === t)
                    return k = !0, 0;
                var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
                return n || (1 & (n = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1) || !z.sortDetached && t.compareDocumentPosition(e) === n ? e === q || e.ownerDocument === P && N(P, e) ? -1 : t === q || t.ownerDocument === P && N(P, t) ? 1 : _ ? ee(_, e) - ee(_, t) : 0 : 4 & n ? -1 : 1)
            } : function(e, t) {
                if (e === t)
                    return k = !0, 0;
                var n,
                    i = 0,
                    r = e.parentNode,
                    o = t.parentNode,
                    s = [e],
                    a = [t];
                if (!r || !o)
                    return e === q ? -1 : t === q ? 1 : r ? -1 : o ? 1 : _ ? ee(_, e) - ee(_, t) : 0;
                if (r === o)
                    return l(e, t);
                for (n = e; n = n.parentNode;)
                    s.unshift(n);
                for (n = t; n = n.parentNode;)
                    a.unshift(n);
                for (; s[i] === a[i];)
                    i++;
                return i ? l(s[i], a[i]) : s[i] === P ? -1 : a[i] === P ? 1 : 0
            }), q
        }, b.matches = function(e, t) {
            return b(e, null, null, t)
        }, b.matchesSelector = function(e, t) {
            if ((e.ownerDocument || e) !== q && H(e), t = t.replace(ce, "='$1']"), z.matchesSelector && W && !V[t + " "] && (!R || !R.test(t)) && (!j || !j.test(t)))
                try {
                    var n = D.call(e, t);
                    if (n || z.disconnectedMatch || e.document && 11 !== e.document.nodeType)
                        return n
                } catch (Le) {}
            return 0 < b(t, q, null, [e]).length
        }, b.contains = function(e, t) {
            return (e.ownerDocument || e) !== q && H(e), N(e, t)
        }, b.attr = function(e, t) {
            (e.ownerDocument || e) !== q && H(e);
            var n = C.attrHandle[t.toLowerCase()],
                i = n && Y.call(C.attrHandle, t.toLowerCase()) ? n(e, t, !W) : undefined;
            return i !== undefined ? i : z.attributes || !W ? e.getAttribute(t) : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
        }, b.escape = function(e) {
            return (e + "").replace(we, ze)
        }, b.error = function(e) {
            throw new Error("Syntax error, unrecognized expression: " + e)
        }, b.uniqueSort = function(e) {
            var t,
                n = [],
                i = 0,
                r = 0;
            if (k = !z.detectDuplicates, _ = !z.sortStable && e.slice(0), e.sort(G), k) {
                for (; t = e[r++];)
                    t === e[r] && (i = n.push(r));
                for (; i--;)
                    e.splice(n[i], 1)
            }
            return _ = null, e
        }, E = b.getText = function(e) {
            var t,
                n = "",
                i = 0,
                r = e.nodeType;
            if (r) {
                if (1 === r || 9 === r || 11 === r) {
                    if ("string" == typeof e.textContent)
                        return e.textContent;
                    for (e = e.firstChild; e; e = e.nextSibling)
                        n += E(e)
                } else if (3 === r || 4 === r)
                    return e.nodeValue
            } else
                for (; t = e[i++];)
                    n += E(t);
            return n
        }, (C = b.selectors = {
            cacheLength: 50,
            createPseudo: u,
            match: pe,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(e) {
                    return e[1] = e[1].replace(xe, be), e[3] = (e[3] || e[4] || e[5] || "").replace(xe, be), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                },
                CHILD: function(e) {
                    return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || b.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && b.error(e[0]), e
                },
                PSEUDO: function(e) {
                    var t,
                        n = !e[6] && e[2];
                    return pe.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && he.test(n) && (t = S(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
                }
            },
            filter: {
                TAG: function(e) {
                    var t = e.replace(xe, be).toLowerCase();
                    return "*" === e ? function() {
                        return !0
                    } : function(e) {
                        return e.nodeName && e.nodeName.toLowerCase() === t
                    }
                },
                CLASS: function(e) {
                    var t = B[e + " "];
                    return t || (t = new RegExp("(^|" + ne + ")" + e + "(" + ne + "|$)")) && B(e, function(e) {
                            return t.test("string" == typeof e.className && e.className || "undefined" != typeof e.getAttribute && e.getAttribute("class") || "")
                        })
                },
                ATTR: function(n, i, r) {
                    return function(e) {
                        var t = b.attr(e, n);
                        return null == t ? "!=" === i : !i || (t += "", "=" === i ? t === r : "!=" === i ? t !== r : "^=" === i ? r && 0 === t.indexOf(r) : "*=" === i ? r && -1 < t.indexOf(r) : "$=" === i ? r && t.slice(-r.length) === r : "~=" === i ? -1 < (" " + t.replace(se, " ") + " ").indexOf(r) : "|=" === i && (t === r || t.slice(0, r.length + 1) === r + "-"))
                    }
                },
                CHILD: function(d, e, t, g, m) {
                    var y = "nth" !== d.slice(0, 3),
                        v = "last" !== d.slice(-4),
                        x = "of-type" === e;
                    return 1 === g && 0 === m ? function(e) {
                        return !!e.parentNode
                    } : function(e, t, n) {
                        var i,
                            r,
                            o,
                            s,
                            a,
                            u,
                            l = y !== v ? "nextSibling" : "previousSibling",
                            c = e.parentNode,
                            h = x && e.nodeName.toLowerCase(),
                            f = !n && !x,
                            p = !1;
                        if (c) {
                            if (y) {
                                for (; l;) {
                                    for (s = e; s = s[l];)
                                        if (x ? s.nodeName.toLowerCase() === h : 1 === s.nodeType)
                                            return !1;
                                    u = l = "only" === d && !u && "nextSibling"
                                }
                                return !0
                            }
                            if (u = [v ? c.firstChild : c.lastChild], v && f) {
                                for (p = (a = (i = (r = (o = (s = c)[F] || (s[F] = {}))[s.uniqueID] || (o[s.uniqueID] = {}))[d] || [])[0] === $ && i[1]) && i[2], s = a && c.childNodes[a]; s = ++a && s && s[l] || (p = a = 0) || u.pop();)
                                    if (1 === s.nodeType && ++p && s === e) {
                                        r[d] = [$, a, p];
                                        break
                                    }
                            } else if (f && (p = a = (i = (r = (o = (s = e)[F] || (s[F] = {}))[s.uniqueID] || (o[s.uniqueID] = {}))[d] || [])[0] === $ && i[1]), !1 === p)
                                for (; (s = ++a && s && s[l] || (p = a = 0) || u.pop()) && ((x ? s.nodeName.toLowerCase() !== h : 1 !== s.nodeType) || !++p || (f && ((r = (o = s[F] || (s[F] = {}))[s.uniqueID] || (o[s.uniqueID] = {}))[d] = [$, p]), s !== e));)
                                    ;
                            return (p -= m) === g || p % g == 0 && 0 <= p / g
                        }
                    }
                },
                PSEUDO: function(e, o) {
                    var t,
                        s = C.pseudos[e] || C.setFilters[e.toLowerCase()] || b.error("unsupported pseudo: " + e);
                    return s[F] ? s(o) : 1 < s.length ? (t = [e, e, "", o], C.setFilters.hasOwnProperty(e.toLowerCase()) ? u(function(e, t) {
                        for (var n, i = s(e, o), r = i.length; r--;)
                            e[n = ee(e, i[r])] = !(t[n] = i[r])
                    }) : function(e) {
                        return s(e, 0, t)
                    }) : s
                }
            },
            pseudos: {
                not: u(function(e) {
                    var i = [],
                        r = [],
                        a = T(e.replace(ae, "$1"));
                    return a[F] ? u(function(e, t, n, i) {
                        for (var r, o = a(e, null, i, []), s = e.length; s--;)
                            (r = o[s]) && (e[s] = !(t[s] = r))
                    }) : function(e, t, n) {
                        return i[0] = e, a(i, null, n, r), i[0] = null, !r.pop()
                    }
                }),
                has: u(function(t) {
                    return function(e) {
                        return 0 < b(t, e).length
                    }
                }),
                contains: u(function(t) {
                    return t = t.replace(xe, be), function(e) {
                        return -1 < (e.textContent || e.innerText || E(e)).indexOf(t)
                    }
                }),
                lang: u(function(n) {
                    return fe.test(n || "") || b.error("unsupported lang: " + n), n = n.replace(xe, be).toLowerCase(), function(e) {
                        var t;
                        do {
                            if (t = W ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang"))
                                return (t = t.toLowerCase()) === n || 0 === t.indexOf(n + "-")
                        } while ((e = e.parentNode) && 1 === e.nodeType);
                        return !1
                    }
                }),
                target: function(e) {
                    var t = n.location && n.location.hash;
                    return t && t.slice(1) === e.id
                },
                root: function(e) {
                    return e === A
                },
                focus: function(e) {
                    return e === q.activeElement && (!q.hasFocus || q.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                },
                enabled: s(!1),
                disabled: s(!0),
                checked: function(e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && !!e.checked || "option" === t && !!e.selected
                },
                selected: function(e) {
                    return e.parentNode && e.parentNode.selectedIndex, !0 === e.selected
                },
                empty: function(e) {
                    for (e = e.firstChild; e; e = e.nextSibling)
                        if (e.nodeType < 6)
                            return !1;
                    return !0
                },
                parent: function(e) {
                    return !C.pseudos.empty(e)
                },
                header: function(e) {
                    return ge.test(e.nodeName)
                },
                input: function(e) {
                    return de.test(e.nodeName)
                },
                button: function(e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && "button" === e.type || "button" === t
                },
                text: function(e) {
                    var t;
                    return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
                },
                first: a(function() {
                    return [0]
                }),
                last: a(function(e, t) {
                    return [t - 1]
                }),
                eq: a(function(e, t, n) {
                    return [n < 0 ? n + t : n]
                }),
                even: a(function(e, t) {
                    for (var n = 0; n < t; n += 2)
                        e.push(n);
                    return e
                }),
                odd: a(function(e, t) {
                    for (var n = 1; n < t; n += 2)
                        e.push(n);
                    return e
                }),
                lt: a(function(e, t, n) {
                    for (var i = n < 0 ? n + t : n; 0 <= --i;)
                        e.push(i);
                    return e
                }),
                gt: a(function(e, t, n) {
                    for (var i = n < 0 ? n + t : n; ++i < t;)
                        e.push(i);
                    return e
                })
            }
        }).pseudos.nth = C.pseudos.eq, {
            radio: !0,
            checkbox: !0,
            file: !0,
            password: !0,
            image: !0
        })
            C.pseudos[y] = i(y);
        for (y in {
            submit: !0,
            reset: !0
        })
            C.pseudos[y] = o(y);
        return c.prototype = C.filters = C.pseudos, C.setFilters = new c, S = b.tokenize = function(e, t) {
            var n,
                i,
                r,
                o,
                s,
                a,
                u,
                l = U[e + " "];
            if (l)
                return t ? 0 : l.slice(0);
            for (s = e, a = [], u = C.preFilter; s;) {
                for (o in n && !(i = ue.exec(s)) || (i && (s = s.slice(i[0].length) || s), a.push(r = [])), n = !1, (i = le.exec(s)) && (n = i.shift(), r.push({
                    value: n,
                    type: i[0].replace(ae, " ")
                }), s = s.slice(n.length)), C.filter)
                    !(i = pe[o].exec(s)) || u[o] && !(i = u[o](i)) || (n = i.shift(), r.push({
                        value: n,
                        type: o,
                        matches: i
                    }), s = s.slice(n.length));
                if (!n)
                    break
            }
            return t ? s.length : s ? b.error(e) : U(e, a).slice(0)
        }, T = b.compile = function(e, t) {
            var n,
                i = [],
                r = [],
                o = V[e + " "];
            if (!o) {
                for (t || (t = S(e)), n = t.length; n--;)
                    (o = p(t[n]))[F] ? i.push(o) : r.push(o);
                (o = V(e, m(r, i))).selector = e
            }
            return o
        }, I = b.select = function(e, t, n, i) {
            var r,
                o,
                s,
                a,
                u,
                l = "function" == typeof e && e,
                c = !i && S(e = l.selector || e);
            if (n = n || [], 1 === c.length) {
                if (2 < (o = c[0] = c[0].slice(0)).length && "ID" === (s = o[0]).type && 9 === t.nodeType && W && C.relative[o[1].type]) {
                    if (!(t = (C.find.ID(s.matches[0].replace(xe, be), t) || [])[0]))
                        return n;
                    l && (t = t.parentNode), e = e.slice(o.shift().value.length)
                }
                for (r = pe.needsContext.test(e) ? 0 : o.length; r-- && (s = o[r], !C.relative[a = s.type]);)
                    if ((u = C.find[a]) && (i = u(s.matches[0].replace(xe, be), ve.test(o[0].type) && d(t.parentNode) || t))) {
                        if (o.splice(r, 1), !(e = i.length && g(o)))
                            return K.apply(n, i), n;
                        break
                    }
            }
            return (l || T(e, c))(i, t, !W, n, !t || ve.test(e) && d(t.parentNode) || t), n
        }, z.sortStable = F.split("").sort(G).join("") === F, z.detectDuplicates = !!k, H(), z.sortDetached = r(function(e) {
            return 1 & e.compareDocumentPosition(q.createElement("fieldset"))
        }), r(function(e) {
            return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
        }) || t("type|href|height|width", function(e, t, n) {
            if (!n)
                return e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
        }), z.attributes && r(function(e) {
            return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
        }) || t("value", function(e, t, n) {
            if (!n && "input" === e.nodeName.toLowerCase())
                return e.defaultValue
        }), r(function(e) {
            return null == e.getAttribute("disabled")
        }) || t(te, function(e, t, n) {
            var i;
            if (!n)
                return !0 === e[t] ? t.toLowerCase() : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
        }), b
    }(E);
    ge.find = be, ge.expr = be.selectors, ge.expr[":"] = ge.expr.pseudos, ge.uniqueSort = ge.unique = be.uniqueSort, ge.text = be.getText, ge.isXMLDoc = be.isXML, ge.contains = be.contains, ge.escapeSelector = be.escape;
    var we = function(e, t, n) {
            for (var i = [], r = n !== undefined; (e = e[t]) && 9 !== e.nodeType;)
                if (1 === e.nodeType) {
                    if (r && ge(e).is(n))
                        break;
                    i.push(e)
                }
            return i
        },
        ze = function(e, t) {
            for (var n = []; e; e = e.nextSibling)
                1 === e.nodeType && e !== t && n.push(e);
            return n
        },
        Ce = ge.expr.match.needsContext,
        Ee = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i,
        Le = /^.[^:#\[\.,]*$/;
    ge.filter = function(e, t, n) {
        var i = t[0];
        return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === i.nodeType ? ge.find.matchesSelector(i, e) ? [i] : [] : ge.find.matches(e, ge.grep(t, function(e) {
            return 1 === e.nodeType
        }))
    }, ge.fn.extend({
        find: function(e) {
            var t,
                n,
                i = this.length,
                r = this;
            if ("string" != typeof e)
                return this.pushStack(ge(e).filter(function() {
                    for (t = 0; t < i; t++)
                        if (ge.contains(r[t], this))
                            return !0
                }));
            for (n = this.pushStack([]), t = 0; t < i; t++)
                ge.find(e, r[t], n);
            return 1 < i ? ge.uniqueSort(n) : n
        },
        filter: function(e) {
            return this.pushStack(t(this, e || [], !1))
        },
        not: function(e) {
            return this.pushStack(t(this, e || [], !0))
        },
        is: function(e) {
            return !!t(this, "string" == typeof e && Ce.test(e) ? ge(e) : e || [], !1).length
        }
    });
    var Se,
        Te = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
    (ge.fn.init = function(e, t, n) {
        var i,
            r;
        if (!e)
            return this;
        if (n = n || Se, "string" != typeof e)
            return e.nodeType ? (this[0] = e, this.length = 1, this) : ge.isFunction(e) ? n.ready !== undefined ? n.ready(e) : e(ge) : ge.makeArray(e, this);
        if (!(i = "<" === e[0] && ">" === e[e.length - 1] && 3 <= e.length ? [null, e, null] : Te.exec(e)) || !i[1] && t)
            return !t || t.jquery ? (t || n).find(e) : this.constructor(t).find(e);
        if (i[1]) {
            if (t = t instanceof ge ? t[0] : t, ge.merge(this, ge.parseHTML(i[1], t && t.nodeType ? t.ownerDocument || t : ne, !0)), Ee.test(i[1]) && ge.isPlainObject(t))
                for (i in t)
                    ge.isFunction(this[i]) ? this[i](t[i]) : this.attr(i, t[i]);
            return this
        }
        return (r = ne.getElementById(i[2])) && (this[0] = r, this.length = 1), this
    }).prototype = ge.fn, Se = ge(ne);
    var Ie = /^(?:parents|prev(?:Until|All))/,
        Oe = {
            children: !0,
            contents: !0,
            next: !0,
            prev: !0
        };
    ge.fn.extend({
        has: function(e) {
            var t = ge(e, this),
                n = t.length;
            return this.filter(function() {
                for (var e = 0; e < n; e++)
                    if (ge.contains(this, t[e]))
                        return !0
            })
        },
        closest: function(e, t) {
            var n,
                i = 0,
                r = this.length,
                o = [],
                s = "string" != typeof e && ge(e);
            if (!Ce.test(e))
                for (; i < r; i++)
                    for (n = this[i]; n && n !== t; n = n.parentNode)
                        if (n.nodeType < 11 && (s ? -1 < s.index(n) : 1 === n.nodeType && ge.find.matchesSelector(n, e))) {
                            o.push(n);
                            break
                        }
            return this.pushStack(1 < o.length ? ge.uniqueSort(o) : o)
        },
        index: function(e) {
            return e ? "string" == typeof e ? ae.call(ge(e), this[0]) : ae.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function(e, t) {
            return this.pushStack(ge.uniqueSort(ge.merge(this.get(), ge(e, t))))
        },
        addBack: function(e) {
            return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
        }
    }), ge.each({
        parent: function(e) {
            var t = e.parentNode;
            return t && 11 !== t.nodeType ? t : null
        },
        parents: function(e) {
            return we(e, "parentNode")
        },
        parentsUntil: function(e, t, n) {
            return we(e, "parentNode", n)
        },
        next: function(e) {
            return n(e, "nextSibling")
        },
        prev: function(e) {
            return n(e, "previousSibling")
        },
        nextAll: function(e) {
            return we(e, "nextSibling")
        },
        prevAll: function(e) {
            return we(e, "previousSibling")
        },
        nextUntil: function(e, t, n) {
            return we(e, "nextSibling", n)
        },
        prevUntil: function(e, t, n) {
            return we(e, "previousSibling", n)
        },
        siblings: function(e) {
            return ze((e.parentNode || {}).firstChild, e)
        },
        children: function(e) {
            return ze(e.firstChild)
        },
        contents: function(e) {
            return l(e, "iframe") ? e.contentDocument : (l(e, "template") && (e = e.content || e), ge.merge([], e.childNodes))
        }
    }, function(i, r) {
        ge.fn[i] = function(e, t) {
            var n = ge.map(this, r, e);
            return "Until" !== i.slice(-5) && (t = e), t && "string" == typeof t && (n = ge.filter(t, n)), 1 < this.length && (Oe[i] || ge.uniqueSort(n), Ie.test(i) && n.reverse()), this.pushStack(n)
        }
    });
    var _e = /[^\x20\t\r\n\f]+/g;
    ge.Callbacks = function(i) {
        i = "string" == typeof i ? c(i) : ge.extend({}, i);
        var r,
            e,
            t,
            n,
            o = [],
            s = [],
            a = -1,
            u = function() {
                for (n = n || i.once, t = r = !0; s.length; a = -1)
                    for (e = s.shift(); ++a < o.length;)
                        !1 === o[a].apply(e[0], e[1]) && i.stopOnFalse && (a = o.length, e = !1);
                i.memory || (e = !1), r = !1, n && (o = e ? [] : "")
            },
            l = {
                add: function() {
                    return o && (e && !r && (a = o.length - 1, s.push(e)), function n(e) {
                        ge.each(e, function(e, t) {
                            ge.isFunction(t) ? i.unique && l.has(t) || o.push(t) : t && t.length && "string" !== ge.type(t) && n(t)
                        })
                    }(arguments), e && !r && u()), this
                },
                remove: function() {
                    return ge.each(arguments, function(e, t) {
                        for (var n; -1 < (n = ge.inArray(t, o, n));)
                            o.splice(n, 1), n <= a && a--
                    }), this
                },
                has: function(e) {
                    return e ? -1 < ge.inArray(e, o) : 0 < o.length
                },
                empty: function() {
                    return o && (o = []), this
                },
                disable: function() {
                    return n = s = [], o = e = "", this
                },
                disabled: function() {
                    return !o
                },
                lock: function() {
                    return n = s = [], e || r || (o = e = ""), this
                },
                locked: function() {
                    return !!n
                },
                fireWith: function(e, t) {
                    return n || (t = [e, (t = t || []).slice ? t.slice() : t], s.push(t), r || u()), this
                },
                fire: function() {
                    return l.fireWith(this, arguments), this
                },
                fired: function() {
                    return !!t
                }
            };
        return l
    }, ge.extend({
        Deferred: function(e) {
            var o = [["notify", "progress", ge.Callbacks("memory"), ge.Callbacks("memory"), 2], ["resolve", "done", ge.Callbacks("once memory"), ge.Callbacks("once memory"), 0, "resolved"], ["reject", "fail", ge.Callbacks("once memory"), ge.Callbacks("once memory"), 1, "rejected"]],
                r = "pending",
                s = {
                    state: function() {
                        return r
                    },
                    always: function() {
                        return a.done(arguments).fail(arguments), this
                    },
                    "catch": function(e) {
                        return s.then(null, e)
                    },
                    pipe: function() {
                        var r = arguments;
                        return ge.Deferred(function(i) {
                            ge.each(o, function(e, t) {
                                var n = ge.isFunction(r[t[4]]) && r[t[4]];
                                a[t[1]](function() {
                                    var e = n && n.apply(this, arguments);
                                    e && ge.isFunction(e.promise) ? e.promise().progress(i.notify).done(i.resolve).fail(i.reject) : i[t[0] + "With"](this, n ? [e] : arguments)
                                })
                            }), r = null
                        }).promise()
                    },
                    then: function(t, n, i) {
                        function l(o, s, a, u) {
                            return function() {
                                var n = this,
                                    i = arguments,
                                    t = function() {
                                        var e,
                                            t;
                                        if (!(o < c)) {
                                            if ((e = a.apply(n, i)) === s.promise())
                                                throw new TypeError("Thenable self-resolution");
                                            t = e && ("object" == typeof e || "function" == typeof e) && e.then, ge.isFunction(t) ? u ? t.call(e, l(c, s, h, u), l(c, s, f, u)) : (c++, t.call(e, l(c, s, h, u), l(c, s, f, u), l(c, s, h, s.notifyWith))) : (a !== h && (n = undefined, i = [e]), (u || s.resolveWith)(n, i))
                                        }
                                    },
                                    r = u ? t : function() {
                                        try {
                                            t()
                                        } catch (e) {
                                            ge.Deferred.exceptionHook && ge.Deferred.exceptionHook(e, r.stackTrace), c <= o + 1 && (a !== f && (n = undefined, i = [e]), s.rejectWith(n, i))
                                        }
                                    };
                                o ? r() : (ge.Deferred.getStackHook && (r.stackTrace = ge.Deferred.getStackHook()), E.setTimeout(r))
                            }
                        }
                        var c = 0;
                        return ge.Deferred(function(e) {
                            o[0][3].add(l(0, e, ge.isFunction(i) ? i : h, e.notifyWith)), o[1][3].add(l(0, e, ge.isFunction(t) ? t : h)), o[2][3].add(l(0, e, ge.isFunction(n) ? n : f))
                        }).promise()
                    },
                    promise: function(e) {
                        return null != e ? ge.extend(e, s) : s
                    }
                },
                a = {};
            return ge.each(o, function(e, t) {
                var n = t[2],
                    i = t[5];
                s[t[1]] = n.add, i && n.add(function() {
                    r = i
                }, o[3 - e][2].disable, o[0][2].lock), n.add(t[3].fire), a[t[0]] = function() {
                    return a[t[0] + "With"](this === a ? undefined : this, arguments), this
                }, a[t[0] + "With"] = n.fireWith
            }), s.promise(a), e && e.call(a, a), a
        },
        when: function(e) {
            var n = arguments.length,
                t = n,
                i = Array(t),
                r = re.call(arguments),
                o = ge.Deferred(),
                s = function(t) {
                    return function(e) {
                        i[t] = this, r[t] = 1 < arguments.length ? re.call(arguments) : e, --n || o.resolveWith(i, r)
                    }
                };
            if (n <= 1 && (u(e, o.done(s(t)).resolve, o.reject, !n), "pending" === o.state() || ge.isFunction(r[t] && r[t].then)))
                return o.then();
            for (; t--;)
                u(r[t], s(t), o.reject);
            return o.promise()
        }
    });
    var ke = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
    ge.Deferred.exceptionHook = function(e, t) {
        E.console && E.console.warn && e && ke.test(e.name) && E.console.warn("jQuery.Deferred exception: " + e.message, e.stack, t)
    }, ge.readyException = function(e) {
        E.setTimeout(function() {
            throw e
        })
    };
    var He = ge.Deferred();
    ge.fn.ready = function(e) {
        return He.then(e)["catch"](function(e) {
            ge.readyException(e)
        }), this
    }, ge.extend({
        isReady: !1,
        readyWait: 1,
        ready: function(e) {
            (!0 === e ? --ge.readyWait : ge.isReady) || (ge.isReady = !0) !== e && 0 < --ge.readyWait || He.resolveWith(ne, [ge])
        }
    }), ge.ready.then = He.then, "complete" === ne.readyState || "loading" !== ne.readyState && !ne.documentElement.doScroll ? E.setTimeout(ge.ready) : (ne.addEventListener("DOMContentLoaded", i), E.addEventListener("load", i));
    var qe = function(e, t, n, i, r, o, s) {
            var a = 0,
                u = e.length,
                l = null == n;
            if ("object" === ge.type(n))
                for (a in r = !0, n)
                    qe(e, t, a, n[a], !0, o, s);
            else if (i !== undefined && (r = !0, ge.isFunction(i) || (s = !0), l && (s ? (t.call(e, i), t = null) : (l = t, t = function(e, t, n) {
                return l.call(ge(e), n)
            })), t))
                for (; a < u; a++)
                    t(e[a], n, s ? i : i.call(e[a], a, t(e[a], n)));
            return r ? e : l ? t.call(e) : u ? t(e[0], n) : o
        },
        Ae = function(e) {
            return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType
        };
    r.uid = 1, r.prototype = {
        cache: function(e) {
            var t = e[this.expando];
            return t || (t = {}, Ae(e) && (e.nodeType ? e[this.expando] = t : Object.defineProperty(e, this.expando, {
                value: t,
                configurable: !0
            }))), t
        },
        set: function(e, t, n) {
            var i,
                r = this.cache(e);
            if ("string" == typeof t)
                r[ge.camelCase(t)] = n;
            else
                for (i in t)
                    r[ge.camelCase(i)] = t[i];
            return r
        },
        get: function(e, t) {
            return t === undefined ? this.cache(e) : e[this.expando] && e[this.expando][ge.camelCase(t)]
        },
        access: function(e, t, n) {
            return t === undefined || t && "string" == typeof t && n === undefined ? this.get(e, t) : (this.set(e, t, n), n !== undefined ? n : t)
        },
        remove: function(e, t) {
            var n,
                i = e[this.expando];
            if (i !== undefined) {
                if (t !== undefined) {
                    n = (t = Array.isArray(t) ? t.map(ge.camelCase) : (t = ge.camelCase(t)) in i ? [t] : t.match(_e) || []).length;
                    for (; n--;)
                        delete i[t[n]]
                }
                (t === undefined || ge.isEmptyObject(i)) && (e.nodeType ? e[this.expando] = undefined : delete e[this.expando])
            }
        },
        hasData: function(e) {
            var t = e[this.expando];
            return t !== undefined && !ge.isEmptyObject(t)
        }
    };
    var We = new r,
        je = new r,
        Re = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
        De = /[A-Z]/g;
    ge.extend({
        hasData: function(e) {
            return je.hasData(e) || We.hasData(e)
        },
        data: function(e, t, n) {
            return je.access(e, t, n)
        },
        removeData: function(e, t) {
            je.remove(e, t)
        },
        _data: function(e, t, n) {
            return We.access(e, t, n)
        },
        _removeData: function(e, t) {
            We.remove(e, t)
        }
    }), ge.fn.extend({
        data: function(n, e) {
            var t,
                i,
                r,
                o = this[0],
                s = o && o.attributes;
            if (n !== undefined)
                return "object" == typeof n ? this.each(function() {
                    je.set(this, n)
                }) : qe(this, function(e) {
                    var t;
                    if (o && e === undefined)
                        return (t = je.get(o, n)) !== undefined ? t : (t = p(o, n)) !== undefined ? t : void 0;
                    this.each(function() {
                        je.set(this, n, e)
                    })
                }, null, e, 1 < arguments.length, null, !0);
            if (this.length && (r = je.get(o), 1 === o.nodeType && !We.get(o, "hasDataAttrs"))) {
                for (t = s.length; t--;)
                    s[t] && 0 === (i = s[t].name).indexOf("data-") && (i = ge.camelCase(i.slice(5)), p(o, i, r[i]));
                We.set(o, "hasDataAttrs", !0)
            }
            return r
        },
        removeData: function(e) {
            return this.each(function() {
                je.remove(this, e)
            })
        }
    }), ge.extend({
        queue: function(e, t, n) {
            var i;
            if (e)
                return t = (t || "fx") + "queue", i = We.get(e, t), n && (!i || Array.isArray(n) ? i = We.access(e, t, ge.makeArray(n)) : i.push(n)), i || []
        },
        dequeue: function(e, t) {
            t = t || "fx";
            var n = ge.queue(e, t),
                i = n.length,
                r = n.shift(),
                o = ge._queueHooks(e, t),
                s = function() {
                    ge.dequeue(e, t)
                };
            "inprogress" === r && (r = n.shift(), i--), r && ("fx" === t && n.unshift("inprogress"), delete o.stop, r.call(e, s, o)), !i && o && o.empty.fire()
        },
        _queueHooks: function(e, t) {
            var n = t + "queueHooks";
            return We.get(e, n) || We.access(e, n, {
                    empty: ge.Callbacks("once memory").add(function() {
                        We.remove(e, [t + "queue", n])
                    })
                })
        }
    }), ge.fn.extend({
        queue: function(t, n) {
            var e = 2;
            return "string" != typeof t && (n = t, t = "fx", e--), arguments.length < e ? ge.queue(this[0], t) : n === undefined ? this : this.each(function() {
                var e = ge.queue(this, t, n);
                ge._queueHooks(this, t), "fx" === t && "inprogress" !== e[0] && ge.dequeue(this, t)
            })
        },
        dequeue: function(e) {
            return this.each(function() {
                ge.dequeue(this, e)
            })
        },
        clearQueue: function(e) {
            return this.queue(e || "fx", [])
        },
        promise: function(e, t) {
            var n,
                i = 1,
                r = ge.Deferred(),
                o = this,
                s = this.length,
                a = function() {
                    --i || r.resolveWith(o, [o])
                };
            for ("string" != typeof e && (t = e, e = undefined), e = e || "fx"; s--;)
                (n = We.get(o[s], e + "queueHooks")) && n.empty && (i++, n.empty.add(a));
            return a(), r.promise(t)
        }
    });
    var Ne = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        Fe = new RegExp("^(?:([+-])=|)(" + Ne + ")([a-z%]*)$", "i"),
        Pe = ["Top", "Right", "Bottom", "Left"],
        $e = function(e, t) {
            return "none" === (e = t || e).style.display || "" === e.style.display && ge.contains(e.ownerDocument, e) && "none" === ge.css(e, "display")
        },
        Me = function(e, t, n, i) {
            var r,
                o,
                s = {};
            for (o in t)
                s[o] = e.style[o], e.style[o] = t[o];
            for (o in r = n.apply(e, i || []), t)
                e.style[o] = s[o];
            return r
        },
        Be = {};
    ge.fn.extend({
        show: function() {
            return y(this, !0)
        },
        hide: function() {
            return y(this)
        },
        toggle: function(e) {
            return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function() {
                $e(this) ? ge(this).show() : ge(this).hide()
            })
        }
    });
    var Ue = /^(?:checkbox|radio)$/i,
        Ve = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i,
        Ge = /^$|\/(?:java|ecma)script/i,
        Ye = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            thead: [1, "<table>", "</table>"],
            col: [2, "<table><colgroup>", "</colgroup></table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: [0, "", ""]
        };
    Ye.optgroup = Ye.option, Ye.tbody = Ye.tfoot = Ye.colgroup = Ye.caption = Ye.thead, Ye.th = Ye.td;
    var Xe,
        Qe,
        Je = /<|&#?\w+;/;
    Xe = ne.createDocumentFragment().appendChild(ne.createElement("div")), (Qe = ne.createElement("input")).setAttribute("type", "radio"), Qe.setAttribute("checked", "checked"), Qe.setAttribute("name", "t"), Xe.appendChild(Qe), pe.checkClone = Xe.cloneNode(!0).cloneNode(!0).lastChild.checked, Xe.innerHTML = "<textarea>x</textarea>", pe.noCloneChecked = !!Xe.cloneNode(!0).lastChild.defaultValue;
    var Ke = ne.documentElement,
        Ze = /^key/,
        et = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
        tt = /^([^.]*)(?:\.(.+)|)/;
    ge.event = {
        global: {},
        add: function(t, e, n, i, r) {
            var o,
                s,
                a,
                u,
                l,
                c,
                h,
                f,
                p,
                d,
                g,
                m = We.get(t);
            if (m)
                for (n.handler && (n = (o = n).handler, r = o.selector), r && ge.find.matchesSelector(Ke, r), n.guid || (n.guid = ge.guid++), (u = m.events) || (u = m.events = {}), (s = m.handle) || (s = m.handle = function(e) {
                    return void 0 !== ge && ge.event.triggered !== e.type ? ge.event.dispatch.apply(t, arguments) : undefined
                }), l = (e = (e || "").match(_e) || [""]).length; l--;)
                    p = g = (a = tt.exec(e[l]) || [])[1], d = (a[2] || "").split(".").sort(), p && (h = ge.event.special[p] || {}, p = (r ? h.delegateType : h.bindType) || p, h = ge.event.special[p] || {}, c = ge.extend({
                        type: p,
                        origType: g,
                        data: i,
                        handler: n,
                        guid: n.guid,
                        selector: r,
                        needsContext: r && ge.expr.match.needsContext.test(r),
                        namespace: d.join(".")
                    }, o), (f = u[p]) || ((f = u[p] = []).delegateCount = 0, h.setup && !1 !== h.setup.call(t, i, d, s) || t.addEventListener && t.addEventListener(p, s)), h.add && (h.add.call(t, c), c.handler.guid || (c.handler.guid = n.guid)), r ? f.splice(f.delegateCount++, 0, c) : f.push(c), ge.event.global[p] = !0)
        },
        remove: function(e, t, n, i, r) {
            var o,
                s,
                a,
                u,
                l,
                c,
                h,
                f,
                p,
                d,
                g,
                m = We.hasData(e) && We.get(e);
            if (m && (u = m.events)) {
                for (l = (t = (t || "").match(_e) || [""]).length; l--;)
                    if (p = g = (a = tt.exec(t[l]) || [])[1], d = (a[2] || "").split(".").sort(), p) {
                        for (h = ge.event.special[p] || {}, f = u[p = (i ? h.delegateType : h.bindType) || p] || [], a = a[2] && new RegExp("(^|\\.)" + d.join("\\.(?:.*\\.|)") + "(\\.|$)"), s = o = f.length; o--;)
                            c = f[o], !r && g !== c.origType || n && n.guid !== c.guid || a && !a.test(c.namespace) || i && i !== c.selector && ("**" !== i || !c.selector) || (f.splice(o, 1), c.selector && f.delegateCount--, h.remove && h.remove.call(e, c));
                        s && !f.length && (h.teardown && !1 !== h.teardown.call(e, d, m.handle) || ge.removeEvent(e, p, m.handle), delete u[p])
                    } else
                        for (p in u)
                            ge.event.remove(e, p + t[l], n, i, !0);
                ge.isEmptyObject(u) && We.remove(e, "handle events")
            }
        },
        dispatch: function(e) {
            var t,
                n,
                i,
                r,
                o,
                s,
                a = ge.event.fix(e),
                u = new Array(arguments.length),
                l = (We.get(this, "events") || {})[a.type] || [],
                c = ge.event.special[a.type] || {};
            for (u[0] = a, t = 1; t < arguments.length; t++)
                u[t] = arguments[t];
            if (a.delegateTarget = this, !c.preDispatch || !1 !== c.preDispatch.call(this, a)) {
                for (s = ge.event.handlers.call(this, a, l), t = 0; (r = s[t++]) && !a.isPropagationStopped();)
                    for (a.currentTarget = r.elem, n = 0; (o = r.handlers[n++]) && !a.isImmediatePropagationStopped();)
                        a.rnamespace && !a.rnamespace.test(o.namespace) || (a.handleObj = o, a.data = o.data, (i = ((ge.event.special[o.origType] || {}).handle || o.handler).apply(r.elem, u)) !== undefined && !1 === (a.result = i) && (a.preventDefault(), a.stopPropagation()));
                return c.postDispatch && c.postDispatch.call(this, a), a.result
            }
        },
        handlers: function(e, t) {
            var n,
                i,
                r,
                o,
                s,
                a = [],
                u = t.delegateCount,
                l = e.target;
            if (u && l.nodeType && !("click" === e.type && 1 <= e.button))
                for (; l !== this; l = l.parentNode || this)
                    if (1 === l.nodeType && ("click" !== e.type || !0 !== l.disabled)) {
                        for (o = [], s = {}, n = 0; n < u; n++)
                            s[r = (i = t[n]).selector + " "] === undefined && (s[r] = i.needsContext ? -1 < ge(r, this).index(l) : ge.find(r, this, null, [l]).length), s[r] && o.push(i);
                        o.length && a.push({
                            elem: l,
                            handlers: o
                        })
                    }
            return l = this, u < t.length && a.push({
                elem: l,
                handlers: t.slice(u)
            }), a
        },
        addProp: function(t, e) {
            Object.defineProperty(ge.Event.prototype, t, {
                enumerable: !0,
                configurable: !0,
                get: ge.isFunction(e) ? function() {
                    if (this.originalEvent)
                        return e(this.originalEvent)
                } : function() {
                    if (this.originalEvent)
                        return this.originalEvent[t]
                },
                set: function(e) {
                    Object.defineProperty(this, t, {
                        enumerable: !0,
                        configurable: !0,
                        writable: !0,
                        value: e
                    })
                }
            })
        },
        fix: function(e) {
            return e[ge.expando] ? e : new ge.Event(e)
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function() {
                    if (this !== z() && this.focus)
                        return this.focus(), !1
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    if (this === z() && this.blur)
                        return this.blur(), !1
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function() {
                    if ("checkbox" === this.type && this.click && l(this, "input"))
                        return this.click(), !1
                },
                _default: function(e) {
                    return l(e.target, "a")
                }
            },
            beforeunload: {
                postDispatch: function(e) {
                    e.result !== undefined && e.originalEvent && (e.originalEvent.returnValue = e.result)
                }
            }
        }
    }, ge.removeEvent = function(e, t, n) {
        e.removeEventListener && e.removeEventListener(t, n)
    }, ge.Event = function(e, t) {
        if (!(this instanceof ge.Event))
            return new ge.Event(e, t);
        e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || e.defaultPrevented === undefined && !1 === e.returnValue ? s : w, this.target = e.target && 3 === e.target.nodeType ? e.target.parentNode : e.target, this.currentTarget = e.currentTarget, this.relatedTarget = e.relatedTarget) : this.type = e, t && ge.extend(this, t), this.timeStamp = e && e.timeStamp || ge.now(), this[ge.expando] = !0
    }, ge.Event.prototype = {
        constructor: ge.Event,
        isDefaultPrevented: w,
        isPropagationStopped: w,
        isImmediatePropagationStopped: w,
        isSimulated: !1,
        preventDefault: function() {
            var e = this.originalEvent;
            this.isDefaultPrevented = s, e && !this.isSimulated && e.preventDefault()
        },
        stopPropagation: function() {
            var e = this.originalEvent;
            this.isPropagationStopped = s, e && !this.isSimulated && e.stopPropagation()
        },
        stopImmediatePropagation: function() {
            var e = this.originalEvent;
            this.isImmediatePropagationStopped = s, e && !this.isSimulated && e.stopImmediatePropagation(), this.stopPropagation()
        }
    }, ge.each({
        altKey: !0,
        bubbles: !0,
        cancelable: !0,
        changedTouches: !0,
        ctrlKey: !0,
        detail: !0,
        eventPhase: !0,
        metaKey: !0,
        pageX: !0,
        pageY: !0,
        shiftKey: !0,
        view: !0,
        "char": !0,
        charCode: !0,
        key: !0,
        keyCode: !0,
        button: !0,
        buttons: !0,
        clientX: !0,
        clientY: !0,
        offsetX: !0,
        offsetY: !0,
        pointerId: !0,
        pointerType: !0,
        screenX: !0,
        screenY: !0,
        targetTouches: !0,
        toElement: !0,
        touches: !0,
        which: function(e) {
            var t = e.button;
            return null == e.which && Ze.test(e.type) ? null != e.charCode ? e.charCode : e.keyCode : !e.which && t !== undefined && et.test(e.type) ? 1 & t ? 1 : 2 & t ? 3 : 4 & t ? 2 : 0 : e.which
        }
    }, ge.event.addProp), ge.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function(e, o) {
        ge.event.special[e] = {
            delegateType: o,
            bindType: o,
            handle: function(e) {
                var t,
                    n = this,
                    i = e.relatedTarget,
                    r = e.handleObj;
                return i && (i === n || ge.contains(n, i)) || (e.type = r.origType, t = r.handler.apply(this, arguments), e.type = o), t
            }
        }
    }), ge.fn.extend({
        on: function(e, t, n, i) {
            return C(this, e, t, n, i)
        },
        one: function(e, t, n, i) {
            return C(this, e, t, n, i, 1)
        },
        off: function(e, t, n) {
            var i,
                r;
            if (e && e.preventDefault && e.handleObj)
                return i = e.handleObj, ge(e.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this;
            if ("object" != typeof e)
                return !1 !== t && "function" != typeof t || (n = t, t = undefined), !1 === n && (n = w), this.each(function() {
                    ge.event.remove(this, e, n, t)
                });
            for (r in e)
                this.off(r, t, e[r]);
            return this
        }
    });
    var nt = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
        it = /<script|<style|<link/i,
        rt = /checked\s*(?:[^=]|=\s*.checked.)/i,
        ot = /^true\/(.*)/,
        st = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
    ge.extend({
        htmlPrefilter: function(e) {
            return e.replace(nt, "<$1></$2>")
        },
        clone: function(e, t, n) {
            var i,
                r,
                o,
                s,
                a = e.cloneNode(!0),
                u = ge.contains(e.ownerDocument, e);
            if (!(pe.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || ge.isXMLDoc(e)))
                for (s = v(a), i = 0, r = (o = v(e)).length; i < r; i++)
                    O(o[i], s[i]);
            if (t)
                if (n)
                    for (o = o || v(e), s = s || v(a), i = 0, r = o.length; i < r; i++)
                        I(o[i], s[i]);
                else
                    I(e, a);
            return 0 < (s = v(a, "script")).length && x(s, !u && v(e, "script")), a
        },
        cleanData: function(e) {
            for (var t, n, i, r = ge.event.special, o = 0; (n = e[o]) !== undefined; o++)
                if (Ae(n)) {
                    if (t = n[We.expando]) {
                        if (t.events)
                            for (i in t.events)
                                r[i] ? ge.event.remove(n, i) : ge.removeEvent(n, i, t.handle);
                        n[We.expando] = undefined
                    }
                    n[je.expando] && (n[je.expando] = undefined)
                }
        }
    }), ge.fn.extend({
        detach: function(e) {
            return k(this, e, !0)
        },
        remove: function(e) {
            return k(this, e)
        },
        text: function(e) {
            return qe(this, function(e) {
                return e === undefined ? ge.text(this) : this.empty().each(function() {
                    1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = e)
                })
            }, null, e, arguments.length)
        },
        append: function() {
            return _(this, arguments, function(e) {
                1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || L(this, e).appendChild(e)
            })
        },
        prepend: function() {
            return _(this, arguments, function(e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = L(this, e);
                    t.insertBefore(e, t.firstChild)
                }
            })
        },
        before: function() {
            return _(this, arguments, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this)
            })
        },
        after: function() {
            return _(this, arguments, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
            })
        },
        empty: function() {
            for (var e, t = 0; null != (e = this[t]); t++)
                1 === e.nodeType && (ge.cleanData(v(e, !1)), e.textContent = "");
            return this
        },
        clone: function(e, t) {
            return e = null != e && e, t = null == t ? e : t, this.map(function() {
                return ge.clone(this, e, t)
            })
        },
        html: function(e) {
            return qe(this, function(e) {
                var t = this[0] || {},
                    n = 0,
                    i = this.length;
                if (e === undefined && 1 === t.nodeType)
                    return t.innerHTML;
                if ("string" == typeof e && !it.test(e) && !Ye[(Ve.exec(e) || ["", ""])[1].toLowerCase()]) {
                    e = ge.htmlPrefilter(e);
                    try {
                        for (; n < i; n++)
                            1 === (t = this[n] || {}).nodeType && (ge.cleanData(v(t, !1)), t.innerHTML = e);
                        t = 0
                    } catch (r) {}
                }
                t && this.empty().append(e)
            }, null, e, arguments.length)
        },
        replaceWith: function() {
            var n = [];
            return _(this, arguments, function(e) {
                var t = this.parentNode;
                ge.inArray(this, n) < 0 && (ge.cleanData(v(this)), t && t.replaceChild(e, this))
            }, n)
        }
    }), ge.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(e, s) {
        ge.fn[e] = function(e) {
            for (var t, n = [], i = ge(e), r = i.length - 1, o = 0; o <= r; o++)
                t = o === r ? this : this.clone(!0), ge(i[o])[s](t), se.apply(n, t.get());
            return this.pushStack(n)
        }
    });
    var at = /^margin/,
        ut = new RegExp("^(" + Ne + ")(?!px)[a-z%]+$", "i"),
        lt = function(e) {
            var t = e.ownerDocument.defaultView;
            return t && t.opener || (t = E), t.getComputedStyle(e)
        };
    !function() {
        function e() {
            if (s) {
                s.style.cssText = "box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%", s.innerHTML = "", Ke.appendChild(o);
                var e = E.getComputedStyle(s);
                t = "1%" !== e.top, r = "2px" === e.marginLeft, n = "4px" === e.width, s.style.marginRight = "50%", i = "4px" === e.marginRight, Ke.removeChild(o), s = null
            }
        }
        var t,
            n,
            i,
            r,
            o = ne.createElement("div"),
            s = ne.createElement("div");
        s.style && (s.style.backgroundClip = "content-box", s.cloneNode(!0).style.backgroundClip = "", pe.clearCloneStyle = "content-box" === s.style.backgroundClip, o.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute", o.appendChild(s), ge.extend(pe, {
            pixelPosition: function() {
                return e(), t
            },
            boxSizingReliable: function() {
                return e(), n
            },
            pixelMarginRight: function() {
                return e(), i
            },
            reliableMarginLeft: function() {
                return e(), r
            }
        }))
    }();
    var ct = /^(none|table(?!-c[ea]).+)/,
        ht = /^--/,
        ft = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        },
        pt = {
            letterSpacing: "0",
            fontWeight: "400"
        },
        dt = ["Webkit", "Moz", "ms"],
        gt = ne.createElement("div").style;
    ge.extend({
        cssHooks: {
            opacity: {
                get: function(e, t) {
                    if (t) {
                        var n = H(e, "opacity");
                        return "" === n ? "1" : n
                    }
                }
            }
        },
        cssNumber: {
            animationIterationCount: !0,
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            "float": "cssFloat"
        },
        style: function(e, t, n, i) {
            if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                var r,
                    o,
                    s,
                    a = ge.camelCase(t),
                    u = ht.test(t),
                    l = e.style;
                if (u || (t = W(a)), s = ge.cssHooks[t] || ge.cssHooks[a], n === undefined)
                    return s && "get" in s && (r = s.get(e, !1, i)) !== undefined ? r : l[t];
                "string" === (o = typeof n) && (r = Fe.exec(n)) && r[1] && (n = d(e, t, r), o = "number"), null != n && n == n && ("number" === o && (n += r && r[3] || (ge.cssNumber[a] ? "" : "px")), pe.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (l[t] = "inherit"), s && "set" in s && (n = s.set(e, n, i)) === undefined || (u ? l.setProperty(t, n) : l[t] = n))
            }
        },
        css: function(e, t, n, i) {
            var r,
                o,
                s,
                a = ge.camelCase(t);
            return ht.test(t) || (t = W(a)), (s = ge.cssHooks[t] || ge.cssHooks[a]) && "get" in s && (r = s.get(e, !0, n)), r === undefined && (r = H(e, t, i)), "normal" === r && t in pt && (r = pt[t]), "" === n || n ? (o = parseFloat(r), !0 === n || isFinite(o) ? o || 0 : r) : r
        }
    }), ge.each(["height", "width"], function(e, s) {
        ge.cssHooks[s] = {
            get: function(e, t, n) {
                if (t)
                    return !ct.test(ge.css(e, "display")) || e.getClientRects().length && e.getBoundingClientRect().width ? D(e, s, n) : Me(e, ft, function() {
                        return D(e, s, n)
                    })
            },
            set: function(e, t, n) {
                var i,
                    r = n && lt(e),
                    o = n && R(e, s, n, "border-box" === ge.css(e, "boxSizing", !1, r), r);
                return o && (i = Fe.exec(t)) && "px" !== (i[3] || "px") && (e.style[s] = t, t = ge.css(e, s)), j(e, t, o)
            }
        }
    }), ge.cssHooks.marginLeft = q(pe.reliableMarginLeft, function(e, t) {
        if (t)
            return (parseFloat(H(e, "marginLeft")) || e.getBoundingClientRect().left - Me(e, {
                marginLeft: 0
            }, function() {
                return e.getBoundingClientRect().left
            })) + "px"
    }), ge.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(r, o) {
        ge.cssHooks[r + o] = {
            expand: function(e) {
                for (var t = 0, n = {}, i = "string" == typeof e ? e.split(" ") : [e]; t < 4; t++)
                    n[r + Pe[t] + o] = i[t] || i[t - 2] || i[0];
                return n
            }
        }, at.test(r) || (ge.cssHooks[r + o].set = j)
    }), ge.fn.extend({
        css: function(e, t) {
            return qe(this, function(e, t, n) {
                var i,
                    r,
                    o = {},
                    s = 0;
                if (Array.isArray(t)) {
                    for (i = lt(e), r = t.length; s < r; s++)
                        o[t[s]] = ge.css(e, t[s], !1, i);
                    return o
                }
                return n !== undefined ? ge.style(e, t, n) : ge.css(e, t)
            }, e, t, 1 < arguments.length)
        }
    }), (ge.Tween = N).prototype = {
        constructor: N,
        init: function(e, t, n, i, r, o) {
            this.elem = e, this.prop = n, this.easing = r || ge.easing._default, this.options = t, this.start = this.now = this.cur(), this.end = i, this.unit = o || (ge.cssNumber[n] ? "" : "px")
        },
        cur: function() {
            var e = N.propHooks[this.prop];
            return e && e.get ? e.get(this
            ) : N.propHooks._default.get(this)
        },
        run: function(e) {
            var t,
                n = N.propHooks[this.prop];
            return this.options.duration ? this.pos = t = ge.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : N.propHooks._default.set(this), this
        }
    }, N.prototype.init.prototype = N.prototype, N.propHooks = {
        _default: {
            get: function(e) {
                var t;
                return 1 !== e.elem.nodeType || null != e.elem[e.prop] && null == e.elem.style[e.prop] ? e.elem[e.prop] : (t = ge.css(e.elem, e.prop, "")) && "auto" !== t ? t : 0
            },
            set: function(e) {
                ge.fx.step[e.prop] ? ge.fx.step[e.prop](e) : 1 !== e.elem.nodeType || null == e.elem.style[ge.cssProps[e.prop]] && !ge.cssHooks[e.prop] ? e.elem[e.prop] = e.now : ge.style(e.elem, e.prop, e.now + e.unit)
            }
        }
    }, N.propHooks.scrollTop = N.propHooks.scrollLeft = {
        set: function(e) {
            e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
        }
    }, ge.easing = {
        linear: function(e) {
            return e
        },
        swing: function(e) {
            return .5 - Math.cos(e * Math.PI) / 2
        },
        _default: "swing"
    }, ge.fx = N.prototype.init, ge.fx.step = {};
    var mt,
        yt,
        vt,
        xt,
        bt = /^(?:toggle|show|hide)$/,
        wt = /queueHooks$/;
    ge.Animation = ge.extend(V, {
        tweeners: {
            "*": [function(e, t) {
                var n = this.createTween(e, t);
                return d(n.elem, e, Fe.exec(t), n), n
            }]
        },
        tweener: function(e, t) {
            ge.isFunction(e) ? (t = e, e = ["*"]) : e = e.match(_e);
            for (var n, i = 0, r = e.length; i < r; i++)
                n = e[i], V.tweeners[n] = V.tweeners[n] || [], V.tweeners[n].unshift(t)
        },
        prefilters: [B],
        prefilter: function(e, t) {
            t ? V.prefilters.unshift(e) : V.prefilters.push(e)
        }
    }), ge.speed = function(e, t, n) {
        var i = e && "object" == typeof e ? ge.extend({}, e) : {
            complete: n || !n && t || ge.isFunction(e) && e,
            duration: e,
            easing: n && t || t && !ge.isFunction(t) && t
        };
        return ge.fx.off ? i.duration = 0 : "number" != typeof i.duration && (i.duration in ge.fx.speeds ? i.duration = ge.fx.speeds[i.duration] : i.duration = ge.fx.speeds._default), null != i.queue && !0 !== i.queue || (i.queue = "fx"), i.old = i.complete, i.complete = function() {
            ge.isFunction(i.old) && i.old.call(this), i.queue && ge.dequeue(this, i.queue)
        }, i
    }, ge.fn.extend({
        fadeTo: function(e, t, n, i) {
            return this.filter($e).css("opacity", 0).show().end().animate({
                opacity: t
            }, e, n, i)
        },
        animate: function(t, e, n, i) {
            var r = ge.isEmptyObject(t),
                o = ge.speed(e, n, i),
                s = function() {
                    var e = V(this, ge.extend({}, t), o);
                    (r || We.get(this, "finish")) && e.stop(!0)
                };
            return s.finish = s, r || !1 === o.queue ? this.each(s) : this.queue(o.queue, s)
        },
        stop: function(r, e, o) {
            var s = function(e) {
                var t = e.stop;
                delete e.stop, t(o)
            };
            return "string" != typeof r && (o = e, e = r, r = undefined), e && !1 !== r && this.queue(r || "fx", []), this.each(function() {
                var e = !0,
                    t = null != r && r + "queueHooks",
                    n = ge.timers,
                    i = We.get(this);
                if (t)
                    i[t] && i[t].stop && s(i[t]);
                else
                    for (t in i)
                        i[t] && i[t].stop && wt.test(t) && s(i[t]);
                for (t = n.length; t--;)
                    n[t].elem !== this || null != r && n[t].queue !== r || (n[t].anim.stop(o), e = !1, n.splice(t, 1));
                !e && o || ge.dequeue(this, r)
            })
        },
        finish: function(s) {
            return !1 !== s && (s = s || "fx"), this.each(function() {
                var e,
                    t = We.get(this),
                    n = t[s + "queue"],
                    i = t[s + "queueHooks"],
                    r = ge.timers,
                    o = n ? n.length : 0;
                for (t.finish = !0, ge.queue(this, s, []), i && i.stop && i.stop.call(this, !0), e = r.length; e--;)
                    r[e].elem === this && r[e].queue === s && (r[e].anim.stop(!0), r.splice(e, 1));
                for (e = 0; e < o; e++)
                    n[e] && n[e].finish && n[e].finish.call(this);
                delete t.finish
            })
        }
    }), ge.each(["toggle", "show", "hide"], function(e, i) {
        var r = ge.fn[i];
        ge.fn[i] = function(e, t, n) {
            return null == e || "boolean" == typeof e ? r.apply(this, arguments) : this.animate($(i, !0), e, t, n)
        }
    }), ge.each({
        slideDown: $("show"),
        slideUp: $("hide"),
        slideToggle: $("toggle"),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function(e, i) {
        ge.fn[e] = function(e, t, n) {
            return this.animate(i, e, t, n)
        }
    }), ge.timers = [], ge.fx.tick = function() {
        var e,
            t = 0,
            n = ge.timers;
        for (mt = ge.now(); t < n.length; t++)
            (e = n[t])() || n[t] !== e || n.splice(t--, 1);
        n.length || ge.fx.stop(), mt = undefined
    }, ge.fx.timer = function(e) {
        ge.timers.push(e), ge.fx.start()
    }, ge.fx.interval = 13, ge.fx.start = function() {
        yt || (yt = !0, F())
    }, ge.fx.stop = function() {
        yt = null
    }, ge.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
    }, ge.fn.delay = function(i, e) {
        return i = ge.fx && ge.fx.speeds[i] || i, e = e || "fx", this.queue(e, function(e, t) {
            var n = E.setTimeout(e, i);
            t.stop = function() {
                E.clearTimeout(n)
            }
        })
    }, vt = ne.createElement("input"), xt = ne.createElement("select").appendChild(ne.createElement("option")), vt.type = "checkbox", pe.checkOn = "" !== vt.value, pe.optSelected = xt.selected, (vt = ne.createElement("input")).value = "t", vt.type = "radio", pe.radioValue = "t" === vt.value;
    var zt,
        Ct = ge.expr.attrHandle;
    ge.fn.extend({
        attr: function(e, t) {
            return qe(this, ge.attr, e, t, 1 < arguments.length)
        },
        removeAttr: function(e) {
            return this.each(function() {
                ge.removeAttr(this, e)
            })
        }
    }), ge.extend({
        attr: function(e, t, n) {
            var i,
                r,
                o = e.nodeType;
            if (3 !== o && 8 !== o && 2 !== o)
                return "undefined" == typeof e.getAttribute ? ge.prop(e, t, n) : (1 === o && ge.isXMLDoc(e) || (r = ge.attrHooks[t.toLowerCase()] || (ge.expr.match.bool.test(t) ? zt : undefined)), n !== undefined ? null === n ? void ge.removeAttr(e, t) : r && "set" in r && (i = r.set(e, n, t)) !== undefined ? i : (e.setAttribute(t, n + ""), n) : r && "get" in r && null !== (i = r.get(e, t)) ? i : null == (i = ge.find.attr(e, t)) ? undefined : i)
        },
        attrHooks: {
            type: {
                set: function(e, t) {
                    if (!pe.radioValue && "radio" === t && l(e, "input")) {
                        var n = e.value;
                        return e.setAttribute("type", t), n && (e.value = n), t
                    }
                }
            }
        },
        removeAttr: function(e, t) {
            var n,
                i = 0,
                r = t && t.match(_e);
            if (r && 1 === e.nodeType)
                for (; n = r[i++];)
                    e.removeAttribute(n)
        }
    }), zt = {
        set: function(e, t, n) {
            return !1 === t ? ge.removeAttr(e, n) : e.setAttribute(n, n), n
        }
    }, ge.each(ge.expr.match.bool.source.match(/\w+/g), function(e, t) {
        var s = Ct[t] || ge.find.attr;
        Ct[t] = function(e, t, n) {
            var i,
                r,
                o = t.toLowerCase();
            return n || (r = Ct[o], Ct[o] = i, i = null != s(e, t, n) ? o : null, Ct[o] = r), i
        }
    });
    var Et = /^(?:input|select|textarea|button)$/i,
        Lt = /^(?:a|area)$/i;
    ge.fn.extend({
        prop: function(e, t) {
            return qe(this, ge.prop, e, t, 1 < arguments.length)
        },
        removeProp: function(e) {
            return this.each(function() {
                delete this[ge.propFix[e] || e]
            })
        }
    }), ge.extend({
        prop: function(e, t, n) {
            var i,
                r,
                o = e.nodeType;
            if (3 !== o && 8 !== o && 2 !== o)
                return 1 === o && ge.isXMLDoc(e) || (t = ge.propFix[t] || t, r = ge.propHooks[t]), n !== undefined ? r && "set" in r && (i = r.set(e, n, t)) !== undefined ? i : e[t] = n : r && "get" in r && null !== (i = r.get(e, t)) ? i : e[t]
        },
        propHooks: {
            tabIndex: {
                get: function(e) {
                    var t = ge.find.attr(e, "tabindex");
                    return t ? parseInt(t, 10) : Et.test(e.nodeName) || Lt.test(e.nodeName) && e.href ? 0 : -1
                }
            }
        },
        propFix: {
            "for": "htmlFor",
            "class": "className"
        }
    }), pe.optSelected || (ge.propHooks.selected = {
        get: function(e) {
            var t = e.parentNode;
            return t && t.parentNode && t.parentNode.selectedIndex, null
        },
        set: function(e) {
            var t = e.parentNode;
            t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex)
        }
    }), ge.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
        ge.propFix[this.toLowerCase()] = this
    }), ge.fn.extend({
        addClass: function(t) {
            var e,
                n,
                i,
                r,
                o,
                s,
                a,
                u = 0;
            if (ge.isFunction(t))
                return this.each(function(e) {
                    ge(this).addClass(t.call(this, e, Y(this)))
                });
            if ("string" == typeof t && t)
                for (e = t.match(_e) || []; n = this[u++];)
                    if (r = Y(n), i = 1 === n.nodeType && " " + G(r) + " ") {
                        for (s = 0; o = e[s++];)
                            i.indexOf(" " + o + " ") < 0 && (i += o + " ");
                        r !== (a = G(i)) && n.setAttribute("class", a)
                    }
            return this
        },
        removeClass: function(t) {
            var e,
                n,
                i,
                r,
                o,
                s,
                a,
                u = 0;
            if (ge.isFunction(t))
                return this.each(function(e) {
                    ge(this).removeClass(t.call(this, e, Y(this)))
                });
            if (!arguments.length)
                return this.attr("class", "");
            if ("string" == typeof t && t)
                for (e = t.match(_e) || []; n = this[u++];)
                    if (r = Y(n), i = 1 === n.nodeType && " " + G(r) + " ") {
                        for (s = 0; o = e[s++];)
                            for (; -1 < i.indexOf(" " + o + " ");)
                                i = i.replace(" " + o + " ", " ");
                        r !== (a = G(i)) && n.setAttribute("class", a)
                    }
            return this
        },
        toggleClass: function(r, t) {
            var o = typeof r;
            return "boolean" == typeof t && "string" === o ? t ? this.addClass(r) : this.removeClass(r) : ge.isFunction(r) ? this.each(function(e) {
                ge(this).toggleClass(r.call(this, e, Y(this), t), t)
            }) : this.each(function() {
                var e,
                    t,
                    n,
                    i;
                if ("string" === o)
                    for (t = 0, n = ge(this), i = r.match(_e) || []; e = i[t++];)
                        n.hasClass(e) ? n.removeClass(e) : n.addClass(e);
                else
                    r !== undefined && "boolean" !== o || ((e = Y(this)) && We.set(this, "__className__", e), this.setAttribute && this.setAttribute("class", e || !1 === r ? "" : We.get(this, "__className__") || ""))
            })
        },
        hasClass: function(e) {
            var t,
                n,
                i = 0;
            for (t = " " + e + " "; n = this[i++];)
                if (1 === n.nodeType && -1 < (" " + G(Y(n)) + " ").indexOf(t))
                    return !0;
            return !1
        }
    });
    var St = /\r/g;
    ge.fn.extend({
        val: function(n) {
            var i,
                e,
                r,
                t = this[0];
            return arguments.length ? (r = ge.isFunction(n), this.each(function(e) {
                var t;
                1 === this.nodeType && (null == (t = r ? n.call(this, e, ge(this).val()) : n) ? t = "" : "number" == typeof t ? t += "" : Array.isArray(t) && (t = ge.map(t, function(e) {
                    return null == e ? "" : e + ""
                })), (i = ge.valHooks[this.type] || ge.valHooks[this.nodeName.toLowerCase()]) && "set" in i && i.set(this, t, "value") !== undefined || (this.value = t))
            })) : t ? (i = ge.valHooks[t.type] || ge.valHooks[t.nodeName.toLowerCase()]) && "get" in i && (e = i.get(t, "value")) !== undefined ? e : "string" == typeof (e = t.value) ? e.replace(St, "") : null == e ? "" : e : void 0
        }
    }), ge.extend({
        valHooks: {
            option: {
                get: function(e) {
                    var t = ge.find.attr(e, "value");
                    return null != t ? t : G(ge.text(e))
                }
            },
            select: {
                get: function(e) {
                    var t,
                        n,
                        i,
                        r = e.options,
                        o = e.selectedIndex,
                        s = "select-one" === e.type,
                        a = s ? null : [],
                        u = s ? o + 1 : r.length;
                    for (i = o < 0 ? u : s ? o : 0; i < u; i++)
                        if (((n = r[i]).selected || i === o) && !n.disabled && (!n.parentNode.disabled || !l(n.parentNode, "optgroup"))) {
                            if (t = ge(n).val(), s)
                                return t;
                            a.push(t)
                        }
                    return a
                },
                set: function(e, t) {
                    for (var n, i, r = e.options, o = ge.makeArray(t), s = r.length; s--;)
                        ((i = r[s]).selected = -1 < ge.inArray(ge.valHooks.option.get(i), o)) && (n = !0);
                    return n || (e.selectedIndex = -1), o
                }
            }
        }
    }), ge.each(["radio", "checkbox"], function() {
        ge.valHooks[this] = {
            set: function(e, t) {
                if (Array.isArray(t))
                    return e.checked = -1 < ge.inArray(ge(e).val(), t)
            }
        }, pe.checkOn || (ge.valHooks[this].get = function(e) {
            return null === e.getAttribute("value") ? "on" : e.value
        })
    });
    var Tt = /^(?:focusinfocus|focusoutblur)$/;
    ge.extend(ge.event, {
        trigger: function(e, t, n, i) {
            var r,
                o,
                s,
                a,
                u,
                l,
                c,
                h = [n || ne],
                f = ce.call(e, "type") ? e.type : e,
                p = ce.call(e, "namespace") ? e.namespace.split(".") : [];
            if (o = s = n = n || ne, 3 !== n.nodeType && 8 !== n.nodeType && !Tt.test(f + ge.event.triggered) && (-1 < f.indexOf(".") && (f = (p = f.split(".")).shift(), p.sort()), u = f.indexOf(":") < 0 && "on" + f, (e = e[ge.expando] ? e : new ge.Event(f, "object" == typeof e && e)).isTrigger = i ? 2 : 3, e.namespace = p.join("."), e.rnamespace = e.namespace ? new RegExp("(^|\\.)" + p.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, e.result = undefined, e.target || (e.target = n), t = null == t ? [e] : ge.makeArray(t, [e]), c = ge.event.special[f] || {}, i || !c.trigger || !1 !== c.trigger.apply(n, t))) {
                if (!i && !c.noBubble && !ge.isWindow(n)) {
                    for (a = c.delegateType || f, Tt.test(a + f) || (o = o.parentNode); o; o = o.parentNode)
                        h.push(o), s = o;
                    s === (n.ownerDocument || ne) && h.push(s.defaultView || s.parentWindow || E)
                }
                for (r = 0; (o = h[r++]) && !e.isPropagationStopped();)
                    e.type = 1 < r ? a : c.bindType || f, (l = (We.get(o, "events") || {})[e.type] && We.get(o, "handle")) && l.apply(o, t), (l = u && o[u]) && l.apply && Ae(o) && (e.result = l.apply(o, t), !1 === e.result && e.preventDefault());
                return e.type = f, i || e.isDefaultPrevented() || c._default && !1 !== c._default.apply(h.pop(), t) || !Ae(n) || u && ge.isFunction(n[f]) && !ge.isWindow(n) && ((s = n[u]) && (n[u] = null), n[ge.event.triggered = f](), ge.event.triggered = undefined, s && (n[u] = s)), e.result
            }
        },
        simulate: function(e, t, n) {
            var i = ge.extend(new ge.Event, n, {
                type: e,
                isSimulated: !0
            });
            ge.event.trigger(i, null, t)
        }
    }), ge.fn.extend({
        trigger: function(e, t) {
            return this.each(function() {
                ge.event.trigger(e, t, this)
            })
        },
        triggerHandler: function(e, t) {
            var n = this[0];
            if (n)
                return ge.event.trigger(e, t, n, !0)
        }
    }), ge.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function(e, n) {
        ge.fn[n] = function(e, t) {
            return 0 < arguments.length ? this.on(n, null, e, t) : this.trigger(n)
        }
    }), ge.fn.extend({
        hover: function(e, t) {
            return this.mouseenter(e).mouseleave(t || e)
        }
    }), pe.focusin = "onfocusin" in E, pe.focusin || ge.each({
        focus: "focusin",
        blur: "focusout"
    }, function(n, i) {
        var r = function(e) {
            ge.event.simulate(i, e.target, ge.event.fix(e))
        };
        ge.event.special[i] = {
            setup: function() {
                var e = this.ownerDocument || this,
                    t = We.access(e, i);
                t || e.addEventListener(n, r, !0), We.access(e, i, (t || 0) + 1)
            },
            teardown: function() {
                var e = this.ownerDocument || this,
                    t = We.access(e, i) - 1;
                t ? We.access(e, i, t) : (e.removeEventListener(n, r, !0), We.remove(e, i))
            }
        }
    });
    var It = E.location,
        Ot = ge.now(),
        _t = /\?/;
    ge.parseXML = function(e) {
        var t;
        if (!e || "string" != typeof e)
            return null;
        try {
            t = (new E.DOMParser).parseFromString(e, "text/xml")
        } catch (n) {
            t = undefined
        }
        return t && !t.getElementsByTagName("parsererror").length || ge.error("Invalid XML: " + e), t
    };
    var kt = /\[\]$/,
        Ht = /\r?\n/g,
        qt = /^(?:submit|button|image|reset|file)$/i,
        At = /^(?:input|select|textarea|keygen)/i;
    ge.param = function(e, t) {
        var n,
            i = [],
            r = function(e, t) {
                var n = ge.isFunction(t) ? t() : t;
                i[i.length] = encodeURIComponent(e) + "=" + encodeURIComponent(null == n ? "" : n)
            };
        if (Array.isArray(e) || e.jquery && !ge.isPlainObject(e))
            ge.each(e, function() {
                r(this.name, this.value)
            });
        else
            for (n in e)
                X(n, e[n], t, r);
        return i.join("&")
    }, ge.fn.extend({
        serialize: function() {
            return ge.param(this.serializeArray())
        },
        serializeArray: function() {
            return this.map(function() {
                var e = ge.prop(this, "elements");
                return e ? ge.makeArray(e) : this
            }).filter(function() {
                var e = this.type;
                return this.name && !ge(this).is(":disabled") && At.test(this.nodeName) && !qt.test(e) && (this.checked || !Ue.test(e))
            }).map(function(e, t) {
                var n = ge(this).val();
                return null == n ? null : Array.isArray(n) ? ge.map(n, function(e) {
                    return {
                        name: t.name,
                        value: e.replace(Ht, "\r\n")
                    }
                }) : {
                    name: t.name,
                    value: n.replace(Ht, "\r\n")
                }
            }).get()
        }
    });
    var Wt = /%20/g,
        jt = /#.*$/,
        Rt = /([?&])_=[^&]*/,
        Dt = /^(.*?):[ \t]*([^\r\n]*)$/gm,
        Nt = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
        Ft = /^(?:GET|HEAD)$/,
        Pt = /^\/\//,
        $t = {},
        Mt = {},
        Bt = "*/".concat("*"),
        Ut = ne.createElement("a");
    Ut.href = It.href, ge.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: It.href,
            type: "GET",
            isLocal: Nt.test(It.protocol),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": Bt,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /\bxml\b/,
                html: /\bhtml/,
                json: /\bjson\b/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": JSON.parse,
                "text xml": ge.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(e, t) {
            return t ? K(K(e, ge.ajaxSettings), t) : K(ge.ajaxSettings, e)
        },
        ajaxPrefilter: Q($t),
        ajaxTransport: Q(Mt),
        ajax: function(e, t) {
            function n(e, t, n, i) {
                var r,
                    o,
                    s,
                    a,
                    u,
                    l = t;
                d || (d = !0, p && E.clearTimeout(p), c = undefined, f = i || "", z.readyState = 0 < e ? 4 : 0, r = 200 <= e && e < 300 || 304 === e, n && (a = Z(m, z, n)), a = ee(m, a, z, r), r ? (m.ifModified && ((u = z.getResponseHeader("Last-Modified")) && (ge.lastModified[h] = u), (u = z.getResponseHeader("etag")) && (ge.etag[h] = u)), 204 === e || "HEAD" === m.type ? l = "nocontent" : 304 === e ? l = "notmodified" : (l = a.state, o = a.data, r = !(s = a.error))) : (s = l, !e && l || (l = "error", e < 0 && (e = 0))), z.status = e, z.statusText = (t || l) + "", r ? x.resolveWith(y, [o, l, z]) : x.rejectWith(y, [z, l, s]), z.statusCode(w), w = undefined, g && v.trigger(r ? "ajaxSuccess" : "ajaxError", [z, m, r ? o : s]), b.fireWith(y, [z, l]), g && (v.trigger("ajaxComplete", [z, m]), --ge.active || ge.event.trigger("ajaxStop")))
            }
            "object" == typeof e && (t = e, e = undefined), t = t || {};
            var c,
                h,
                f,
                i,
                p,
                r,
                d,
                g,
                o,
                s,
                m = ge.ajaxSetup({}, t),
                y = m.context || m,
                v = m.context && (y.nodeType || y.jquery) ? ge(y) : ge.event,
                x = ge.Deferred(),
                b = ge.Callbacks("once memory"),
                w = m.statusCode || {},
                a = {},
                u = {},
                l = "canceled",
                z = {
                    readyState: 0,
                    getResponseHeader: function(e) {
                        var t;
                        if (d) {
                            if (!i)
                                for (i = {}; t = Dt.exec(f);)
                                    i[t[1].toLowerCase()] = t[2];
                            t = i[e.toLowerCase()]
                        }
                        return null == t ? null : t
                    },
                    getAllResponseHeaders: function() {
                        return d ? f : null
                    },
                    setRequestHeader: function(e, t) {
                        return null == d && (e = u[e.toLowerCase()] = u[e.toLowerCase()] || e, a[e] = t), this
                    },
                    overrideMimeType: function(e) {
                        return null == d && (m.mimeType = e), this
                    },
                    statusCode: function(e) {
                        var t;
                        if (e)
                            if (d)
                                z.always(e[z.status]);
                            else
                                for (t in e)
                                    w[t] = [w[t], e[t]];
                        return this
                    },
                    abort: function(e) {
                        var t = e || l;
                        return c && c.abort(t), n(0, t), this
                    }
                };
            if (x.promise(z), m.url = ((e || m.url || It.href) + "").replace(Pt, It.protocol + "//"), m.type = t.method || t.type || m.method || m.type, m.dataTypes = (m.dataType || "*").toLowerCase().match(_e) || [""], null == m.crossDomain) {
                r = ne.createElement("a");
                try {
                    r.href = m.url, r.href = r.href, m.crossDomain = Ut.protocol + "//" + Ut.host != r.protocol + "//" + r.host
                } catch (C) {
                    m.crossDomain = !0
                }
            }
            if (m.data && m.processData && "string" != typeof m.data && (m.data = ge.param(m.data, m.traditional)), J($t, m, t, z), d)
                return z;
            for (o in (g = ge.event && m.global) && 0 == ge.active++ && ge.event.trigger("ajaxStart"), m.type = m.type.toUpperCase(), m.hasContent = !Ft.test(m.type), h = m.url.replace(jt, ""), m.hasContent ? m.data && m.processData && 0 === (m.contentType || "").indexOf("application/x-www-form-urlencoded") && (m.data = m.data.replace(Wt, "+")) : (s = m.url.slice(h.length), m.data && (h += (_t.test(h) ? "&" : "?") + m.data, delete m.data), !1 === m.cache && (h = h.replace(Rt, "$1"), s = (_t.test(h) ? "&" : "?") + "_=" + Ot++ + s), m.url = h + s), m.ifModified && (ge.lastModified[h] && z.setRequestHeader("If-Modified-Since", ge.lastModified[h]), ge.etag[h] && z.setRequestHeader("If-None-Match", ge.etag[h])), (m.data && m.hasContent && !1 !== m.contentType || t.contentType) && z.setRequestHeader("Content-Type", m.contentType), z.setRequestHeader("Accept", m.dataTypes[0] && m.accepts[m.dataTypes[0]] ? m.accepts[m.dataTypes[0]] + ("*" !== m.dataTypes[0] ? ", " + Bt + "; q=0.01" : "") : m.accepts["*"]), m.headers)
                z.setRequestHeader(o, m.headers[o]);
            if (m.beforeSend && (!1 === m.beforeSend.call(y, z, m) || d))
                return z.abort();
            if (l = "abort", b.add(m.complete), z.done(m.success), z.fail(m.error), c = J(Mt, m, t, z)) {
                if (z.readyState = 1, g && v.trigger("ajaxSend", [z, m]), d)
                    return z;
                m.async && 0 < m.timeout && (p = E.setTimeout(function() {
                    z.abort("timeout")
                }, m.timeout));
                try {
                    d = !1, c.send(a, n)
                } catch (C) {
                    if (d)
                        throw C;
                    n(-1, C)
                }
            } else
                n(-1, "No Transport");
            return z
        },
        getJSON: function(e, t, n) {
            return ge.get(e, t, n, "json")
        },
        getScript: function(e, t) {
            return ge.get(e, undefined, t, "script")
        }
    }), ge.each(["get", "post"], function(e, r) {
        ge[r] = function(e, t, n, i) {
            return ge.isFunction(t) && (i = i || n, n = t, t = undefined), ge.ajax(ge.extend({
                url: e,
                type: r,
                dataType: i,
                data: t,
                success: n
            }, ge.isPlainObject(e) && e))
        }
    }), ge._evalUrl = function(e) {
        return ge.ajax({
            url: e,
            type: "GET",
            dataType: "script",
            cache: !0,
            async: !1,
            global: !1,
            "throws": !0
        })
    }, ge.fn.extend({
        wrapAll: function(e) {
            var t;
            return this[0] && (ge.isFunction(e) && (e = e.call(this[0])), t = ge(e, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && t.insertBefore(this[0]), t.map(function() {
                for (var e = this; e.firstElementChild;)
                    e = e.firstElementChild;
                return e
            }).append(this)), this
        },
        wrapInner: function(n) {
            return ge.isFunction(n) ? this.each(function(e) {
                ge(this).wrapInner(n.call(this, e))
            }) : this.each(function() {
                var e = ge(this),
                    t = e.contents();
                t.length ? t.wrapAll(n) : e.append(n)
            })
        },
        wrap: function(t) {
            var n = ge.isFunction(t);
            return this.each(function(e) {
                ge(this).wrapAll(n ? t.call(this, e) : t)
            })
        },
        unwrap: function(e) {
            return this.parent(e).not("body").each(function() {
                ge(this).replaceWith(this.childNodes)
            }), this
        }
    }), ge.expr.pseudos.hidden = function(e) {
        return !ge.expr.pseudos.visible(e)
    }, ge.expr.pseudos.visible = function(e) {
        return !!(e.offsetWidth || e.offsetHeight || e.getClientRects().length)
    }, ge.ajaxSettings.xhr = function() {
        try {
            return new E.XMLHttpRequest
        } catch (e) {}
    };
    var Vt = {
            0: 200,
            1223: 204
        },
        Gt = ge.ajaxSettings.xhr();
    pe.cors = !!Gt && "withCredentials" in Gt, pe.ajax = Gt = !!Gt, ge.ajaxTransport(function(o) {
        var s,
            a;
        if (pe.cors || Gt && !o.crossDomain)
            return {
                send: function(e, t) {
                    var n,
                        i = o.xhr();
                    if (i.open(o.type, o.url, o.async, o.username, o.password), o.xhrFields)
                        for (n in o.xhrFields)
                            i[n] = o.xhrFields[n];
                    for (n in o.mimeType && i.overrideMimeType && i.overrideMimeType(o.mimeType), o.crossDomain || e["X-Requested-With"] || (e["X-Requested-With"] = "XMLHttpRequest"), e)
                        i.setRequestHeader(n, e[n]);
                    s = function(e) {
                        return function() {
                            s && (s = a = i.onload = i.onerror = i.onabort = i.onreadystatechange = null, "abort" === e ? i.abort() : "error" === e ? "number" != typeof i.status ? t(0, "error") : t(i.status, i.statusText) : t(Vt[i.status] || i.status, i.statusText, "text" !== (i.responseType || "text") || "string" != typeof i.responseText ? {
                                binary: i.response
                            } : {
                                text: i.responseText
                            }, i.getAllResponseHeaders()))
                        }
                    }, i.onload = s(), a = i.onerror = s("error"), i.onabort !== undefined ? i.onabort = a : i.onreadystatechange = function() {
                        4 === i.readyState && E.setTimeout(function() {
                            s && a()
                        })
                    }, s = s("abort");
                    try {
                        i.send(o.hasContent && o.data || null)
                    } catch (r) {
                        if (s)
                            throw r
                    }
                },
                abort: function() {
                    s && s()
                }
            }
    }), ge.ajaxPrefilter(function(e) {
        e.crossDomain && (e.contents.script = !1)
    }), ge.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /\b(?:java|ecma)script\b/
        },
        converters: {
            "text script": function(e) {
                return ge.globalEval(e), e
            }
        }
    }), ge.ajaxPrefilter("script", function(e) {
        e.cache === undefined && (e.cache = !1), e.crossDomain && (e.type = "GET")
    }), ge.ajaxTransport("script", function(n) {
        var i,
            r;
        if (n.crossDomain)
            return {
                send: function(e, t) {
                    i = ge("<script>").prop({
                        charset: n.scriptCharset,
                        src: n.url
                    }).on("load error", r = function(e) {
                        i.remove(), r = null, e && t("error" === e.type ? 404 : 200, e.type)
                    }), ne.head.appendChild(i[0])
                },
                abort: function() {
                    r && r()
                }
            }
    });
    var Yt,
        Xt = [],
        Qt = /(=)\?(?=&|$)|\?\?/;
    ge.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var e = Xt.pop() || ge.expando + "_" + Ot++;
            return this[e] = !0, e
        }
    }), ge.ajaxPrefilter("json jsonp", function(e, t, n) {
        var i,
            r,
            o,
            s = !1 !== e.jsonp && (Qt.test(e.url) ? "url" : "string" == typeof e.data && 0 === (e.contentType || "").indexOf("application/x-www-form-urlencoded") && Qt.test(e.data) && "data");
        if (s || "jsonp" === e.dataTypes[0])
            return i = e.jsonpCallback = ge.isFunction(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, s ? e[s] = e[s].replace(Qt, "$1" + i) : !1 !== e.jsonp && (e.url += (_t.test(e.url) ? "&" : "?") + e.jsonp + "=" + i), e.converters["script json"] = function() {
                return o || ge.error(i + " was not called"), o[0]
            }, e.dataTypes[0] = "json", r = E[i], E[i] = function() {
                o = arguments
            }, n.always(function() {
                r === undefined ? ge(E).removeProp(i) : E[i] = r, e[i] && (e.jsonpCallback = t.jsonpCallback, Xt.push(i)), o && ge.isFunction(r) && r(o[0]), o = r = undefined
            }), "script"
    }), pe.createHTMLDocument = ((Yt = ne.implementation.createHTMLDocument("").body).innerHTML = "<form></form><form></form>", 2 === Yt.childNodes.length), ge.parseHTML = function(e, t, n) {
        return "string" != typeof e ? [] : ("boolean" == typeof t && (n = t, t = !1), t || (pe.createHTMLDocument ? ((i = (t = ne.implementation.createHTMLDocument("")).createElement("base")).href = ne.location.href, t.head.appendChild(i)) : t = ne), o = !n && [], (r = Ee.exec(e)) ? [t.createElement(r[1])] : (r = b([e], t, o), o && o.length && ge(o).remove(), ge.merge([], r.childNodes)));
        var i,
            r,
            o
    }, ge.fn.load = function(e, t, n) {
        var i,
            r,
            o,
            s = this,
            a = e.indexOf(" ");
        return -1 < a && (i = G(e.slice(a)), e = e.slice(0, a)), ge.isFunction(t) ? (n = t, t = undefined) : t && "object" == typeof t && (r = "POST"), 0 < s.length && ge.ajax({
            url: e,
            type: r || "GET",
            dataType: "html",
            data: t
        }).done(function(e) {
            o = arguments, s.html(i ? ge("<div>").append(ge.parseHTML(e)).find(i) : e)
        }).always(n && function(e, t) {
            s.each(function() {
                n.apply(this, o || [e.responseText, t, e])
            })
        }), this
    }, ge.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(e, t) {
        ge.fn[t] = function(e) {
            return this.on(t, e)
        }
    }), ge.expr.pseudos.animated = function(t) {
        return ge.grep(ge.timers, function(e) {
            return t === e.elem
        }).length
    }, ge.offset = {
        setOffset: function(e, t, n) {
            var i,
                r,
                o,
                s,
                a,
                u,
                l = ge.css(e, "position"),
                c = ge(e),
                h = {};
            "static" === l && (e.style.position = "relative"), a = c.offset(), o = ge.css(e, "top"), u = ge.css(e, "left"), ("absolute" === l || "fixed" === l) && -1 < (o + u).indexOf("auto") ? (s = (i = c.position()).top, r = i.left) : (s = parseFloat(o) || 0, r = parseFloat(u) || 0), ge.isFunction(t) && (t = t.call(e, n, ge.extend({}, a))), null != t.top && (h.top = t.top - a.top + s), null != t.left && (h.left = t.left - a.left + r), "using" in t ? t.using.call(e, h) : c.css(h)
        }
    }, ge.fn.extend({
        offset: function(t) {
            if (arguments.length)
                return t === undefined ? this : this.each(function(e) {
                    ge.offset.setOffset(this, t, e)
                });
            var e,
                n,
                i,
                r,
                o = this[0];
            return o ? o.getClientRects().length ? (i = o.getBoundingClientRect(), n = (e = o.ownerDocument).documentElement, r = e.defaultView, {
                top: i.top + r.pageYOffset - n.clientTop,
                left: i.left + r.pageXOffset - n.clientLeft
            }) : {
                top: 0,
                left: 0
            } : void 0
        },
        position: function() {
            if (this[0]) {
                var e,
                    t,
                    n = this[0],
                    i = {
                        top: 0,
                        left: 0
                    };
                return "fixed" === ge.css(n, "position") ? t = n.getBoundingClientRect() : (e = this.offsetParent(), t = this.offset(), l(e[0], "html") || (i = e.offset()), i = {
                    top: i.top + ge.css(e[0], "borderTopWidth", !0),
                    left: i.left + ge.css(e[0], "borderLeftWidth", !0)
                }), {
                    top: t.top - i.top - ge.css(n, "marginTop", !0),
                    left: t.left - i.left - ge.css(n, "marginLeft", !0)
                }
            }
        },
        offsetParent: function() {
            return this.map(function() {
                for (var e = this.offsetParent; e && "static" === ge.css(e, "position");)
                    e = e.offsetParent;
                return e || Ke
            })
        }
    }), ge.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(t, r) {
        var o = "pageYOffset" === r;
        ge.fn[t] = function(e) {
            return qe(this, function(e, t, n) {
                var i;
                if (ge.isWindow(e) ? i = e : 9 === e.nodeType && (i = e.defaultView), n === undefined)
                    return i ? i[r] : e[t];
                i ? i.scrollTo(o ? i.pageXOffset : n, o ? n : i.pageYOffset) : e[t] = n
            }, t, e, arguments.length)
        }
    }), ge.each(["top", "left"], function(e, n) {
        ge.cssHooks[n] = q(pe.pixelPosition, function(e, t) {
            if (t)
                return t = H(e, n), ut.test(t) ? ge(e).position()[n] + "px" : t
        })
    }), ge.each({
        Height: "height",
        Width: "width"
    }, function(s, a) {
        ge.each({
            padding: "inner" + s,
            content: a,
            "": "outer" + s
        }, function(i, o) {
            ge.fn[o] = function(e, t) {
                var n = arguments.length && (i || "boolean" != typeof e),
                    r = i || (!0 === e || !0 === t ? "margin" : "border");
                return qe(this, function(e, t, n) {
                    var i;
                    return ge.isWindow(e) ? 0 === o.indexOf("outer") ? e["inner" + s] : e.document.documentElement["client" + s] : 9 === e.nodeType ? (i = e.documentElement, Math.max(e.body["scroll" + s], i["scroll" + s], e.body["offset" + s], i["offset" + s], i["client" + s])) : n === undefined ? ge.css(e, t, r) : ge.style(e, t, n, r)
                }, a, n ? e : undefined, n)
            }
        })
    }), ge.fn.extend({
        bind: function(e, t, n) {
            return this.on(e, null, t, n)
        },
        unbind: function(e, t) {
            return this.off(e, null, t)
        },
        delegate: function(e, t, n, i) {
            return this.on(t, e, n, i)
        },
        undelegate: function(e, t, n) {
            return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
        }
    }), ge.holdReady = function(e) {
        e ? ge.readyWait++ : ge.ready(!0)
    }, ge.isArray = Array.isArray, ge.parseJSON = JSON.parse, ge.nodeName = l, "function" == typeof define && define.amd && define("jquery", [], function() {
        return ge
    });
    var Jt = E.jQuery,
        Kt = E.$;
    return ge.noConflict = function(e) {
        return E.$ === ge && (E.$ = Kt), e && E.jQuery === ge && (E.jQuery = Jt), ge
    }, e || (E.jQuery = E.$ = ge), ge
}), function() {
    function e() {}
    function o(e, t) {
        for (var n = e.length; n--;)
            if (e[n].listener === t)
                return n;
        return -1
    }
    function t(e) {
        return function t() {
            return this[e].apply(this, arguments)
        }
    }
    var n = e.prototype,
        i = this,
        r = i.EventEmitter;
    n.getListeners = function s(e) {
        var t,
            n,
            i = this._getEvents();
        if ("object" == typeof e)
            for (n in t = {}, i)
                i.hasOwnProperty(n) && e.test(n) && (t[n] = i[n]);
        else
            t = i[e] || (i[e] = []);
        return t
    }, n.flattenListeners = function a(e) {
        var t,
            n = [];
        for (t = 0; t < e.length; t += 1)
            n.push(e[t].listener);
        return n
    }, n.getListenersAsObject = function u(e) {
        var t,
            n = this.getListeners(e);
        return n instanceof Array && ((t = {})[e] = n), t || n
    }, n.addListener = function l(e, t) {
        var n,
            i = this.getListenersAsObject(e),
            r = "object" == typeof t;
        for (n in i)
            i.hasOwnProperty(n) && -1 === o(i[n], t) && i[n].push(r ? t : {
                listener: t,
                once: !1
            });
        return this
    }, n.on = t("addListener"), n.addOnceListener = function c(e, t) {
        return this.addListener(e, {
            listener: t,
            once: !0
        })
    }, n.once = t("addOnceListener"), n.defineEvent = function h(e) {
        return this.getListeners(e), this
    }, n.defineEvents = function f(e) {
        for (var t = 0; t < e.length; t += 1)
            this.defineEvent(e[t]);
        return this
    }, n.removeListener = function p(e, t) {
        var n,
            i,
            r = this.getListenersAsObject(e);
        for (i in r)
            r.hasOwnProperty(i) && -1 !== (n = o(r[i], t)) && r[i].splice(n, 1);
        return this
    }, n.off = t("removeListener"), n.addListeners = function d(e, t) {
        return this.manipulateListeners(!1, e, t)
    }, n.removeListeners = function g(e, t) {
        return this.manipulateListeners(!0, e, t)
    }, n.manipulateListeners = function m(e, t, n) {
        var i,
            r,
            o = e ? this.removeListener : this.addListener,
            s = e ? this.removeListeners : this.addListeners;
        if ("object" != typeof t || t instanceof RegExp)
            for (i = n.length; i--;)
                o.call(this, t, n[i]);
        else
            for (i in t)
                t.hasOwnProperty(i) && (r = t[i]) && ("function" == typeof r ? o.call(this, i, r) : s.call(this, i, r));
        return this
    }, n.removeEvent = function y(e) {
        var t,
            n = typeof e,
            i = this._getEvents();
        if ("string" === n)
            delete i[e];
        else if ("object" === n)
            for (t in i)
                i.hasOwnProperty(t) && e.test(t) && delete i[t];
        else
            delete this._events;
        return this
    }, n.removeAllListeners = t("removeEvent"), n.emitEvent = function v(e, t) {
        var n,
            i,
            r,
            o = this.getListenersAsObject(e);
        for (r in o)
            if (o.hasOwnProperty(r))
                for (i = o[r].length; i--;)
                    !0 === (n = o[r][i]).once && this.removeListener(e, n.listener), n.listener.apply(this, t || []) === this._getOnceReturnValue() && this.removeListener(e, n.listener);
        return this
    }, n.trigger = t("emitEvent"), n.emit = function x(e) {
        var t = Array.prototype.slice.call(arguments, 1);
        return this.emitEvent(e, t)
    }, n.setOnceReturnValue = function b(e) {
        return this._onceReturnValue = e, this
    }, n._getOnceReturnValue = function w() {
        return !this.hasOwnProperty("_onceReturnValue") || this._onceReturnValue
    }, n._getEvents = function z() {
        return this._events || (this._events = {})
    }, e.noConflict = function C() {
        return i.EventEmitter = r, e
    }, "function" == typeof define && define.amd ? define("eventEmitter/EventEmitter", [], function() {
        return e
    }) : "object" == typeof module && module.exports ? module.exports = e : this.EventEmitter = e
}.call(this), function(n) {
    function i(e) {
        var t = n.event;
        return t.target = t.target || t.srcElement || e, t
    }
    var e = document.documentElement,
        t = function() {};
    e.addEventListener ? t = function(e, t, n) {
        e.addEventListener(t, n, !1)
    } : e.attachEvent && (t = function(t, e, n) {
        t[e + n] = n.handleEvent ? function() {
            var e = i(t);
            n.handleEvent.call(n, e)
        } : function() {
            var e = i(t);
            n.call(t, e)
        }, t.attachEvent("on" + e, t[e + n])
    });
    var r = function() {};
    e.removeEventListener ? r = function(e, t, n) {
        e.removeEventListener(t, n, !1)
    } : e.detachEvent && (r = function(e, t, n) {
        e.detachEvent("on" + t, e[t + n]);
        try {
            delete e[t + n]
        } catch (i) {
            e[t + n] = undefined
        }
    });
    var o = {
        bind: t,
        unbind: r
    };
    "function" == typeof define && define.amd ? define("eventie/eventie", o) : n.eventie = o
}(this), function(e) {
    function s(e, t) {
        for (var n in t)
            e[n] = t[n];
        return e
    }
    function r(e) {
        return "[object Array]" === n.call(e)
    }
    function a(e) {
        var t = [];
        if (r(e))
            t = e;
        else if ("number" == typeof e.length)
            for (var n = 0, i = e.length; n < i; n++)
                t.push(e[n]);
        else
            t.push(e);
        return t
    }
    function t(e, t) {
        function r(e, t, n) {
            if (!(this instanceof r))
                return new r(e, t);
            "string" == typeof e && (e = document.querySelectorAll(e)), this.elements = a(e), this.options = s({}, this.options), "function" == typeof t ? n = t : s(this.options, t), n && this.on("always", n), this.getImages(), u && (this.jqDeferred = new u.Deferred);
            var i = this;
            setTimeout(function() {
                i.check()
            })
        }
        function n(e) {
            this.img = e
        }
        function i(e) {
            this.src = e, o[e] = this
        }
        r.prototype = new e, r.prototype.options = {}, r.prototype.getImages = function() {
            this.images = [];
            for (var e = 0, t = this.elements.length; e < t; e++) {
                var n = this.elements[e];
                "IMG" === n.nodeName && this.addImage(n);
                for (var i = n.querySelectorAll("img"), r = 0, o = i.length; r < o; r++) {
                    var s = i[r];
                    this.addImage(s)
                }
            }
        }, r.prototype.addImage = function(e) {
            var t = new n(e);
            this.images.push(t)
        }, r.prototype.check = function() {
            function e(e, t) {
                return n.options.debug && c && l.log("confirm", e, t), n.progress(e), ++i === r && n.complete(), !0
            }
            var n = this,
                i = 0,
                r = this.images.length;
            if (this.hasAnyBroken = !1, r)
                for (var t = 0; t < r; t++) {
                    var o = this.images[t];
                    o.on("confirm", e), o.check()
                }
            else
                this.complete()
        }, r.prototype.progress = function(e) {
            this.hasAnyBroken = this.hasAnyBroken || !e.isLoaded;
            var t = this;
            setTimeout(function() {
                t.emit("progress", t, e), t.jqDeferred && t.jqDeferred.notify(t, e)
            })
        }, r.prototype.complete = function() {
            var t = this.hasAnyBroken ? "fail" : "done";
            this.isComplete = !0;
            var n = this;
            setTimeout(function() {
                if (n.emit(t, n), n.emit("always", n), n.jqDeferred) {
                    var e = n.hasAnyBroken ? "reject" : "resolve";
                    n.jqDeferred[e](n)
                }
            })
        }, u && (u.fn.imagesLoaded = function(e, t) {
            return new r(this, e, t).jqDeferred.promise(u(this))
        }), n.prototype = new e, n.prototype.check = function() {
            var e = o[this.img.src] || new i(this.img.src);
            if (e.isConfirmed)
                this.confirm(e.isLoaded, "cached was confirmed");
            else if (this.img.complete && this.img.naturalWidth !== undefined)
                this.confirm(0 !== this.img.naturalWidth, "naturalWidth");
            else {
                var n = this;
                e.on("confirm", function(e, t) {
                    return n.confirm(e.isLoaded, t), !0
                }), e.check()
            }
        }, n.prototype.confirm = function(e, t) {
            this.isLoaded = e, this.emit("confirm", this, t)
        };
        var o = {};
        return i.prototype = new e, i.prototype.check = function() {
            if (!this.isChecked) {
                var e = new Image;
                t.bind(e, "load", this), t.bind(e, "error", this), e.src = this.src, this.isChecked = !0
            }
        }, i.prototype.handleEvent = function(e) {
            var t = "on" + e.type;
            this[t] && this[t](e)
        }, i.prototype.onload = function(e) {
            this.confirm(!0, "onload"), this.unbindProxyEvents(e)
        }, i.prototype.onerror = function(e) {
            this.confirm(!1, "onerror"), this.unbindProxyEvents(e)
        }, i.prototype.confirm = function(e, t) {
            this.isConfirmed = !0, this.isLoaded = e, this.emit("confirm", this, t)
        }, i.prototype.unbindProxyEvents = function(e) {
            t.unbind(e.target, "load", this), t.unbind(e.target, "error", this)
        }, r
    }
    var u = e.jQuery,
        l = e.console,
        c = void 0 !== l,
        n = Object.prototype.toString;
    "function" == typeof define && define.amd ? define(["eventEmitter/EventEmitter", "eventie/eventie"], t) : e.imagesLoaded = t(e.EventEmitter, e.eventie)
}(window), function() {
    function o(e, t) {
        if ("function" == typeof e)
            return o("*", e);
        if ("function" == typeof t)
            for (var n = new a(e), i = 1; i < arguments.length; ++i)
                o.callbacks.push(n.middleware(arguments[i]));
        else
            "string" == typeof e ? o.show(e, t) : o.start(e)
    }
    function r(e) {
        window.location.pathname + window.location.search != e.canonicalPath && (o.stop(), e.unhandled = !0, window.location = e.canonicalPath)
    }
    function s(e, t) {
        "/" == e[0] && 0 != e.indexOf(p) && (e = p + e);
        var n = e.indexOf("?");
        if (
        this.canonicalPath = e, this.path = e.replace(p, "") || "/", this.title = document.title, this.state = t || {}, this.state.path = e, this.querystring = ~n ? e.slice(n + 1) : "", this.pathname = ~n ? e.slice(0, n) : e, this.params = [], this.hash = "", ~this.path.indexOf("#")) {
            var i = this.path.split("#");
            this.path = i[0], this.hash = i[1] || "", this.querystring = this.querystring.split("#")[0]
        }
    }
    function a(e, t) {
        t = t || {}, this.path = e, this.method = "GET", this.regexp = n(e, this.keys = [], t.sensitive, t.strict)
    }
    function n(e, s, t, n) {
        return e instanceof RegExp ? e : (e instanceof Array && (e = "(" + e.join("|") + ")"), e = e.concat(n ? "" : "/?").replace(/\/\(/g, "(?:/").replace(/(\/)?(\.)?:(\w+)(?:(\(.*?\)))?(\?)?/g, function(e, t, n, i, r, o) {
            return s.push({
                name: i,
                optional: !!o
            }), t = t || "", (o ? "" : t) + "(?:" + (o ? t : "") + (n || "") + (r || (n ? "([^/.]+?)" : "([^/]+?)")) + ")" + (o || "")
        }).replace(/([\/.])/g, "\\$1").replace(/\*/g, "(.*)"), new RegExp("^" + e + "$", t ? "" : "i"))
    }
    function i(e) {
        if (e.state) {
            var t = e.state.path;
            o.replace(t, e.state)
        }
    }
    function u(e) {
        if (1 == l(e) && !(e.metaKey || e.ctrlKey || e.shiftKey || e.defaultPrevented)) {
            for (var t = e.target; t && "A" != t.nodeName;)
                t = t.parentNode;
            if (t && "A" == t.nodeName) {
                var n = t.getAttribute("href");
                if ((t.pathname != location.pathname || !t.hash && "#" != n) && !t.target && c(t.href)) {
                    var i = t.pathname + t.search + (t.hash || ""),
                        r = i + t.hash;
                    i = i.replace(p, ""), p && r == i || (e.preventDefault(), o.show(r))
                }
            }
        }
    }
    function l(e) {
        return null == (e = e || window.event).which ? e.button : e.which
    }
    function c(e) {
        var t = location.protocol + "//" + location.hostname;
        return location.port && (t += ":" + location.port), 0 == e.indexOf(t)
    }
    var h,
        f = !0,
        p = "";
    o.callbacks = [], o.base = function(e) {
        if (0 == arguments.length)
            return p;
        p = e
    }, o.start = function(e) {
        if (e = e || {}, !h && (!(h = !0) === e.dispatch && (f = !1), !1 !== e.popstate && window.addEventListener("popstate", i, !1), !1 !== e.click && window.addEventListener("click", u, !1), f)) {
            var t = location.pathname + location.search + location.hash;
            o.replace(t, null, !0, f)
        }
    }, o.stop = function() {
        h = !1, removeEventListener("click", u, !1), removeEventListener("popstate", i, !1)
    }, o.show = function(e, t, n) {
        var i = new s(e, t);
        return !1 !== n && o.dispatch(i), i.unhandled || i.pushState(), i
    }, o.replace = function(e, t, n, i) {
        var r = new s(e, t);
        return r.init = n, null == i && (i = !0), i && o.dispatch(r), r.save(), r
    }, o.dispatch = function(t) {
        function n() {
            var e = o.callbacks[i++];
            if (!e)
                return r(t);
            e(t, n)
        }
        var i = 0;
        n()
    }, (o.Context = s).prototype.pushState = function() {
        history.pushState(this.state, this.title, this.canonicalPath)
    }, s.prototype.save = function() {
        history.replaceState(this.state, this.title, this.canonicalPath)
    }, (o.Route = a).prototype.middleware = function(n) {
        var i = this;
        return function(e, t) {
            if (i.match(e.path, e.params))
                return n(e, t);
            t()
        }
    }, a.prototype.match = function(e, t) {
        var n = this.keys,
            i = e.indexOf("?"),
            r = ~i ? e.slice(0, i) : e,
            o = this.regexp.exec(decodeURIComponent(r));
        if (!o)
            return !1;
        for (var s = 1, a = o.length; s < a; ++s) {
            var u = n[s - 1],
                l = "string" == typeof o[s] ? decodeURIComponent(o[s]) : o[s];
            u ? t[u.name] = undefined !== t[u.name] ? t[u.name] : l : t.push(l)
        }
        return !0
    }, "undefined" == typeof module ? window.page = o : module.exports = o
}(), function() {
    var e = this,
        t = e._,
        s = {},
        r = Array.prototype,
        n = Object.prototype,
        i = Function.prototype,
        o = r.push,
        u = r.slice,
        a = r.concat,
        h = n.toString,
        l = n.hasOwnProperty,
        c = r.forEach,
        f = r.map,
        p = r.reduce,
        d = r.reduceRight,
        g = r.filter,
        m = r.every,
        y = r.some,
        v = r.indexOf,
        x = r.lastIndexOf,
        b = Array.isArray,
        w = Object.keys,
        z = i.bind,
        C = function(e) {
            return e instanceof C ? e : this instanceof C ? void (this._wrapped = e) : new C(e)
        };
    "undefined" != typeof exports ? ("undefined" != typeof module && module.exports && (exports = module.exports = C), exports._ = C) : e._ = C, C.VERSION = "1.5.2";
    var E = C.each = C.forEach = function(e, t, n) {
        if (null != e)
            if (c && e.forEach === c)
                e.forEach(t, n);
            else if (e.length === +e.length) {
                for (var i = 0, r = e.length; i < r; i++)
                    if (t.call(n, e[i], i, e) === s)
                        return
            } else {
                var o = C.keys(e);
                for (i = 0, r = o.length; i < r; i++)
                    if (t.call(n, e[o[i]], o[i], e) === s)
                        return
            }
    };
    C.map = C.collect = function(e, i, r) {
        var o = [];
        return null == e ? o : f && e.map === f ? e.map(i, r) : (E(e, function(e, t, n) {
            o.push(i.call(r, e, t, n))
        }), o)
    };
    var L = "Reduce of empty array with no initial value";
    C.reduce = C.foldl = C.inject = function(e, i, r, o) {
        var s = 2 < arguments.length;
        if (null == e && (e = []), p && e.reduce === p)
            return o && (i = C.bind(i, o)), s ? e.reduce(i, r) : e.reduce(i);
        if (E(e, function(e, t, n) {
            s ? r = i.call(o, r, e, t, n) : (r = e, s = !0)
        }), !s)
            throw new TypeError(L);
        return r
    }, C.reduceRight = C.foldr = function(i, r, o, s) {
        var a = 2 < arguments.length;
        if (null == i && (i = []), d && i.reduceRight === d)
            return s && (r = C.bind(r, s)), a ? i.reduceRight(r, o) : i.reduceRight(r);
        var u = i.length;
        if (u !== +u) {
            var l = C.keys(i);
            u = l.length
        }
        if (E(i, function(e, t, n) {
            t = l ? l[--u] : --u, a ? o = r.call(s, o, i[t], t, n) : (o = i[t], a = !0)
        }), !a)
            throw new TypeError(L);
        return o
    }, C.find = C.detect = function(e, i, r) {
        var o;
        return S(e, function(e, t, n) {
            return i.call(r, e, t, n) ? (o = e, !0) : void 0
        }), o
    }, C.filter = C.select = function(e, i, r) {
        var o = [];
        return null == e ? o : g && e.filter === g ? e.filter(i, r) : (E(e, function(e, t, n) {
            i.call(r, e, t, n) && o.push(e)
        }), o)
    }, C.reject = function(e, i, r) {
        return C.filter(e, function(e, t, n) {
            return !i.call(r, e, t, n)
        }, r)
    }, C.every = C.all = function(e, i, r) {
        i || (i = C.identity);
        var o = !0;
        return null == e ? o : m && e.every === m ? e.every(i, r) : (E(e, function(e, t, n) {
            return (o = o && i.call(r, e, t, n)) ? void 0 : s
        }), !!o)
    };
    var S = C.some = C.any = function(e, i, r) {
        i || (i = C.identity);
        var o = !1;
        return null == e ? o : y && e.some === y ? e.some(i, r) : (E(e, function(e, t, n) {
            return o || (o = i.call(r, e, t, n)) ? s : void 0
        }), !!o)
    };
    C.contains = C.include = function(e, t) {
        return null != e && (v && e.indexOf === v ? -1 != e.indexOf(t) : S(e, function(e) {
                return e === t
            }))
    }, C.invoke = function(e, t) {
        var n = u.call(arguments, 2),
            i = C.isFunction(t);
        return C.map(e, function(e) {
            return (i ? t : e[t]).apply(e, n)
        })
    }, C.pluck = function(e, t) {
        return C.map(e, function(e) {
            return e[t]
        })
    }, C.where = function(e, n, t) {
        return C.isEmpty(n) ? t ? void 0 : [] : C[t ? "find" : "filter"](e, function(e) {
            for (var t in n)
                if (n[t] !== e[t])
                    return !1;
            return !0
        })
    }, C.findWhere = function(e, t) {
        return C.where(e, t, !0)
    }, C.max = function(e, r, o) {
        if (!r && C.isArray(e) && e[0] === +e[0] && e.length < 65535)
            return Math.max.apply(Math, e);
        if (!r && C.isEmpty(e))
            return -1 / 0;
        var s = {
            computed: -1 / 0,
            value: -1 / 0
        };
        return E(e, function(e, t, n) {
            var i = r ? r.call(o, e, t, n) : e;
            i > s.computed && (s = {
                value: e,
                computed: i
            })
        }), s.value
    }, C.min = function(e, r, o) {
        if (!r && C.isArray(e) && e[0] === +e[0] && e.length < 65535)
            return Math.min.apply(Math, e);
        if (!r && C.isEmpty(e))
            return 1 / 0;
        var s = {
            computed: 1 / 0,
            value: 1 / 0
        };
        return E(e, function(e, t, n) {
            var i = r ? r.call(o, e, t, n) : e;
            i < s.computed && (s = {
                value: e,
                computed: i
            })
        }), s.value
    }, C.shuffle = function(e) {
        var t,
            n = 0,
            i = [];
        return E(e, function(e) {
            t = C.random(n++), i[n - 1] = i[t], i[t] = e
        }), i
    }, C.sample = function(e, t, n) {
        return arguments.length < 2 || n ? e[C.random(e.length - 1)] : C.shuffle(e).slice(0, Math.max(0, t))
    };
    var T = function(t) {
        return C.isFunction(t) ? t : function(e) {
            return e[t]
        }
    };
    C.sortBy = function(e, t, i) {
        var r = T(t);
        return C.pluck(C.map(e, function(e, t, n) {
            return {
                value: e,
                index: t,
                criteria: r.call(i, e, t, n)
            }
        }).sort(function(e, t) {
            var n = e.criteria,
                i = t.criteria;
            if (n !== i) {
                if (i < n || void 0 === n)
                    return 1;
                if (n < i || void 0 === i)
                    return -1
            }
            return e.index - t.index
        }), "value")
    };
    var I = function(a) {
        return function(i, e, r) {
            var o = {},
                s = null == e ? C.identity : T(e);
            return E(i, function(e, t) {
                var n = s.call(r, e, t, i);
                a(o, n, e)
            }), o
        }
    };
    C.groupBy = I(function(e, t, n) {
        (C.has(e, t) ? e[t] : e[t] = []).push(n)
    }), C.indexBy = I(function(e, t, n) {
        e[t] = n
    }), C.countBy = I(function(e, t) {
        C.has(e, t) ? e[t]++ : e[t] = 1
    }), C.sortedIndex = function(e, t, n, i) {
        for (var r = (n = null == n ? C.identity : T(n)).call(i, t), o = 0, s = e.length; o < s;) {
            var a = o + s >>> 1;
            n.call(i, e[a]) < r ? o = a + 1 : s = a
        }
        return o
    }, C.toArray = function(e) {
        return e ? C.isArray(e) ? u.call(e) : e.length === +e.length ? C.map(e, C.identity) : C.values(e) : []
    }, C.size = function(e) {
        return null == e ? 0 : e.length === +e.length ? e.length : C.keys(e).length
    }, C.first = C.head = C.take = function(e, t, n) {
        return null == e ? void 0 : null == t || n ? e[0] : u.call(e, 0, t)
    }, C.initial = function(e, t, n) {
        return u.call(e, 0, e.length - (null == t || n ? 1 : t))
    }, C.last = function(e, t, n) {
        return null == e ? void 0 : null == t || n ? e[e.length - 1] : u.call(e, Math.max(e.length - t, 0))
    }, C.rest = C.tail = C.drop = function(e, t, n) {
        return u.call(e, null == t || n ? 1 : t)
    }, C.compact = function(e) {
        return C.filter(e, C.identity)
    };
    var O = function(e, t, n) {
        return t && C.every(e, C.isArray) ? a.apply(n, e) : (E(e, function(e) {
            C.isArray(e) || C.isArguments(e) ? t ? o.apply(n, e) : O(e, t, n) : n.push(e)
        }), n)
    };
    C.flatten = function(e, t) {
        return O(e, t, [])
    }, C.without = function(e) {
        return C.difference(e, u.call(arguments, 1))
    }, C.uniq = C.unique = function(n, i, e, t) {
        C.isFunction(i) && (t = e, e = i, i = !1);
        var r = e ? C.map(n, e, t) : n,
            o = [],
            s = [];
        return E(r, function(e, t) {
            (i ? t && s[s.length - 1] === e : C.contains(s, e)) || (s.push(e), o.push(n[t]))
        }), o
    }, C.union = function() {
        return C.uniq(C.flatten(arguments, !0))
    }, C.intersection = function(e) {
        var n = u.call(arguments, 1);
        return C.filter(C.uniq(e), function(t) {
            return C.every(n, function(e) {
                return 0 <= C.indexOf(e, t)
            })
        })
    }, C.difference = function(e) {
        var t = a.apply(r, u.call(arguments, 1));
        return C.filter(e, function(e) {
            return !C.contains(t, e)
        })
    }, C.zip = function() {
        for (var e = C.max(C.pluck(arguments, "length").concat(0)), t = new Array(e), n = 0; n < e; n++)
            t[n] = C.pluck(arguments, "" + n);
        return t
    }, C.object = function(e, t) {
        if (null == e)
            return {};
        for (var n = {}, i = 0, r = e.length; i < r; i++)
            t ? n[e[i]] = t[i] : n[e[i][0]] = e[i][1];
        return n
    }, C.indexOf = function(e, t, n) {
        if (null == e)
            return -1;
        var i = 0,
            r = e.length;
        if (n) {
            if ("number" != typeof n)
                return e[i = C.sortedIndex(e, t)] === t ? i : -1;
            i = n < 0 ? Math.max(0, r + n) : n
        }
        if (v && e.indexOf === v)
            return e.indexOf(t, n);
        for (; i < r; i++)
            if (e[i] === t)
                return i;
        return -1
    }, C.lastIndexOf = function(e, t, n) {
        if (null == e)
            return -1;
        var i = null != n;
        if (x && e.lastIndexOf === x)
            return i ? e.lastIndexOf(t, n) : e.lastIndexOf(t);
        for (var r = i ? n : e.length; r--;)
            if (e[r] === t)
                return r;
        return -1
    }, C.range = function(e, t, n) {
        arguments.length <= 1 && (t = e || 0, e = 0), n = n || 1;
        for (var i = Math.max(Math.ceil((t - e) / n), 0), r = 0, o = new Array(i); r < i;)
            o[r++] = e, e += n;
        return o
    };
    var _ = function() {};
    C.bind = function(n, i) {
        var r,
            o;
        if (z && n.bind === z)
            return z.apply(n, u.call(arguments, 1));
        if (!C.isFunction(n))
            throw new TypeError;
        return r = u.call(arguments, 2), o = function() {
            if (!(this instanceof o))
                return n.apply(i, r.concat(u.call(arguments)));
            _.prototype = n.prototype;
            var e = new _;
            _.prototype = null;
            var t = n.apply(e, r.concat(u.call(arguments)));
            return Object(t) === t ? t : e
        }
    }, C.partial = function(e) {
        var t = u.call(arguments, 1);
        return function() {
            return e.apply(this, t.concat(u.call(arguments)))
        }
    }, C.bindAll = function(t) {
        var e = u.call(arguments, 1);
        if (0 === e.length)
            throw new Error("bindAll must be passed function names");
        return E(e, function(e) {
            t[e] = C.bind(t[e], t)
        }), t
    }, C.memoize = function(t, n) {
        var i = {};
        return n || (n = C.identity), function() {
            var e = n.apply(this, arguments);
            return C.has(i, e) ? i[e] : i[e] = t.apply(this, arguments)
        }
    }, C.delay = function(e, t) {
        var n = u.call(arguments, 2);
        return setTimeout(function() {
            return e.apply(null, n)
        }, t)
    }, C.defer = function(e) {
        return C.delay.apply(C, [e, 1].concat(u.call(arguments, 1)))
    }, C.throttle = function(n, i, r) {
        var o,
            s,
            a,
            u = null,
            l = 0;
        r || (r = {});
        var c = function() {
            l = !1 === r.leading ? 0 : new Date, u = null, a = n.apply(o, s)
        };
        return function() {
            var e = new Date;
            l || !1 !== r.leading || (l = e);
            var t = i - (e - l);
            return o = this, s = arguments, t <= 0 ? (clearTimeout(u), u = null, l = e, a = n.apply(o, s)) : u || !1 === r.trailing || (u = setTimeout(c, t)), a
        }
    }, C.debounce = function(n, i, r) {
        var o,
            s,
            a,
            u,
            l;
        return function() {
            a = this, s = arguments, u = new Date;
            var t = function() {
                    var e = new Date - u;
                    e < i ? o = setTimeout(t, i - e) : (o = null, r || (l = n.apply(a, s)))
                },
                e = r && !o;
            return o || (o = setTimeout(t, i)), e && (l = n.apply(a, s)), l
        }
    }, C.once = function(e) {
        var t,
            n = !1;
        return function() {
            return n || (n = !0, t = e.apply(this, arguments), e = null), t
        }
    }, C.wrap = function(t, n) {
        return function() {
            var e = [t];
            return o.apply(e, arguments), n.apply(this, e)
        }
    }, C.compose = function() {
        var n = arguments;
        return function() {
            for (var e = arguments, t = n.length - 1; 0 <= t; t--)
                e = [n[t].apply(this, e)];
            return e[0]
        }
    }, C.after = function(e, t) {
        return function() {
            return --e < 1 ? t.apply(this, arguments) : void 0
        }
    }, C.keys = w || function(e) {
        if (e !== Object(e))
            throw new TypeError("Invalid object");
        var t = [];
        for (var n in e)
            C.has(e, n) && t.push(n);
        return t
    }, C.values = function(e) {
        for (var t = C.keys(e), n = t.length, i = new Array(n), r = 0; r < n; r++)
            i[r] = e[t[r]];
        return i
    }, C.pairs = function(e) {
        for (var t = C.keys(e), n = t.length, i = new Array(n), r = 0; r < n; r++)
            i[r] = [t[r], e[t[r]]];
        return i
    }, C.invert = function(e) {
        for (var t = {}, n = C.keys(e), i = 0, r = n.length; i < r; i++)
            t[e[n[i]]] = n[i];
        return t
    }, C.functions = C.methods = function(e) {
        var t = [];
        for (var n in e)
            C.isFunction(e[n]) && t.push(n);
        return t.sort()
    }, C.extend = function(n) {
        return E(u.call(arguments, 1), function(e) {
            if (e)
                for (var t in e)
                    n[t] = e[t]
        }), n
    }, C.pick = function(t) {
        var n = {},
            e = a.apply(r, u.call(arguments, 1));
        return E(e, function(e) {
            e in t && (n[e] = t[e])
        }), n
    }, C.omit = function(e) {
        var t = {},
            n = a.apply(r, u.call(arguments, 1));
        for (var i in e)
            C.contains(n, i) || (t[i] = e[i]);
        return t
    }, C.defaults = function(n) {
        return E(u.call(arguments, 1), function(e) {
            if (e)
                for (var t in e)
                    void 0 === n[t] && (n[t] = e[t])
        }), n
    }, C.clone = function(e) {
        return C.isObject(e) ? C.isArray(e) ? e.slice() : C.extend({}, e) : e
    }, C.tap = function(e, t) {
        return t(e), e
    };
    var k = function(e, t, n, i) {
        if (e === t)
            return 0 !== e || 1 / e == 1 / t;
        if (null == e || null == t)
            return e === t;
        e instanceof C && (e = e._wrapped), t instanceof C && (t = t._wrapped);
        var r = h.call(e);
        if (r != h.call(t))
            return !1;
        switch (r) {
        case "[object String]":
            return e == String(t);
        case "[object Number]":
            return e != +e ? t != +t : 0 == e ? 1 / e == 1 / t : e == +t;
        case "[object Date]":
        case "[object Boolean]":
            return +e == +t;
        case "[object RegExp]":
            return e.source == t.source && e.global == t.global && e.multiline == t.multiline && e.ignoreCase == t.ignoreCase
        }
        if ("object" != typeof e || "object" != typeof t)
            return !1;
        for (var o = n.length; o--;)
            if (n[o] == e)
                return i[o] == t;
        var s = e.constructor,
            a = t.constructor;
        if (s !== a && !(C.isFunction(s) && s instanceof s && C.isFunction(a) && a instanceof a))
            return !1;
        n.push(e), i.push(t);
        var u = 0,
            l = !0;
        if ("[object Array]" == r) {
            if (l = (u = e.length) == t.length)
                for (; u-- && (l = k(e[u], t[u], n, i));)
                    ;
        } else {
            for (var c in e)
                if (C.has(e, c) && (u++, !(l = C.has(t, c) && k(e[c], t[c], n, i))))
                    break;
            if (l) {
                for (c in t)
                    if (C.has(t, c) && !u--)
                        break;
                l = !u
            }
        }
        return n.pop(), i.pop(), l
    };
    C.isEqual = function(e, t) {
        return k(e, t, [], [])
    }, C.isEmpty = function(e) {
        if (null == e)
            return !0;
        if (C.isArray(e) || C.isString(e))
            return 0 === e.length;
        for (var t in e)
            if (C.has(e, t))
                return !1;
        return !0
    }, C.isElement = function(e) {
        return !(!e || 1 !== e.nodeType)
    }, C.isArray = b || function(e) {
        return "[object Array]" == h.call(e)
    }, C.isObject = function(e) {
        return e === Object(e)
    }, E(["Arguments", "Function", "String", "Number", "Date", "RegExp"], function(t) {
        C["is" + t] = function(e) {
            return h.call(e) == "[object " + t + "]"
        }
    }), C.isArguments(arguments) || (C.isArguments = function(e) {
        return !(!e || !C.has(e, "callee"))
    }), "function" != typeof /./ && (C.isFunction = function(e) {
        return "function" == typeof e
    }), C.isFinite = function(e) {
        return isFinite(e) && !isNaN(parseFloat(e))
    }, C.isNaN = function(e) {
        return C.isNumber(e) && e != +e
    }, C.isBoolean = function(e) {
        return !0 === e || !1 === e || "[object Boolean]" == h.call(e)
    }, C.isNull = function(e) {
        return null === e
    }, C.isUndefined = function(e) {
        return void 0 === e
    }, C.has = function(e, t) {
        return l.call(e, t)
    }, C.noConflict = function() {
        return e._ = t, this
    }, C.identity = function(e) {
        return e
    }, C.times = function(e, t, n) {
        for (var i = Array(Math.max(0, e)), r = 0; r < e; r++)
            i[r] = t.call(n, r);
        return i
    }, C.random = function(e, t) {
        return null == t && (t = e, e = 0), e + Math.floor(Math.random() * (t - e + 1))
    };
    var H = {
        escape: {
            "&": "&amp;",
            "<": "&lt;",
            ">": "&gt;",
            '"': "&quot;",
            "'": "&#x27;"
        }
    };
    H.unescape = C.invert(H.escape);
    var q = {
        escape: new RegExp("[" + C.keys(H.escape).join("") + "]", "g"),
        unescape: new RegExp("(" + C.keys(H.unescape).join("|") + ")", "g")
    };
    C.each(["escape", "unescape"], function(t) {
        C[t] = function(e) {
            return null == e ? "" : ("" + e).replace(q[t], function(e) {
                return H[t][e]
            })
        }
    }), C.result = function(e, t) {
        if (null != e) {
            var n = e[t];
            return C.isFunction(n) ? n.call(e) : n
        }
    }, C.mixin = function(n) {
        E(C.functions(n), function(e) {
            var t = C[e] = n[e];
            C.prototype[e] = function() {
                var e = [this._wrapped];
                return o.apply(e, arguments), D.call(this, t.apply(C, e))
            }
        })
    };
    var A = 0;
    C.uniqueId = function(e) {
        var t = ++A + "";
        return e ? e + t : t
    }, C.templateSettings = {
        evaluate: /<%([\s\S]+?)%>/g,
        interpolate: /<%=([\s\S]+?)%>/g,
        escape: /<%-([\s\S]+?)%>/g
    };
    var W = /(.)^/,
        j = {
            "'": "'",
            "\\": "\\",
            "\r": "r",
            "\n": "n",
            "\t": "t",
            "\u2028": "u2028",
            "\u2029": "u2029"
        },
        R = /\\|'|\r|\n|\t|\u2028|\u2029/g;
    C.template = function(o, e, t) {
        var n;
        t = C.defaults({}, t, C.templateSettings);
        var i = new RegExp([(t.escape || W).source, (t.interpolate || W).source, (t.evaluate || W).source].join("|") + "|$", "g"),
            s = 0,
            a = "__p+='";
        o.replace(i, function(e, t, n, i, r) {
            return a += o.slice(s, r).replace(R, function(e) {
                return "\\" + j[e]
            }), t && (a += "'+\n((__t=(" + t + "))==null?'':_.escape(__t))+\n'"), n && (a += "'+\n((__t=(" + n + "))==null?'':__t)+\n'"), i && (a += "';\n" + i + "\n__p+='"), s = r + e.length, e
        }), a += "';\n", t.variable || (a = "with(obj||{}){\n" + a + "}\n"), a = "var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};\n" + a + "return __p;\n";
        try {
            n = new Function(t.variable || "obj", "_", a)
        } catch (u) {
            throw u.source = a, u
        }
        if (e)
            return n(e, C);
        var r = function(e) {
            return n.call(this, e, C)
        };
        return r.source = "function(" + (t.variable || "obj") + "){\n" + a + "}", r
    }, C.chain = function(e) {
        return C(e).chain()
    };
    var D = function(e) {
        return this._chain ? C(e).chain() : e
    };
    C.mixin(C), E(["pop", "push", "reverse", "shift", "sort", "splice", "unshift"], function(t) {
        var n = r[t];
        C.prototype[t] = function() {
            var e = this._wrapped;
            return n.apply(e, arguments), "shift" != t && "splice" != t || 0 !== e.length || delete e[0], D.call(this, e)
        }
    }), E(["concat", "join", "slice"], function(e) {
        var t = r[e];
        C.prototype[e] = function() {
            return D.call(this, t.apply(this._wrapped, arguments))
        }
    }), C.extend(C.prototype, {
        chain: function() {
            return this._chain = !0, this
        },
        value: function() {
            return this._wrapped
        }
    })
}.call(this), function(e) {
    "use strict";
    function r(e) {
        this.initialize(e)
    }
    function t(e) {
        this.initialize(e)
    }
    var n = {
        bind: function(e, t) {
            this._callbacks || (this._callbacks = {});
            return (this._callbacks[e] || (this._callbacks[e] = [])).push(t), this
        },
        trigger: function(e) {
            var t,
                n,
                i,
                r;
            if (!(n = this._callbacks))
                return this;
            if (t = n[e])
                for (i = 0, r = t.length; i < r; i++)
                    t[i].apply(this, Array.prototype.slice.call(arguments, 1));
            return this
        },
        addEvents: function(e) {
            e.prototype.trigger = this.trigger, e.prototype.bind = this.bind
        }
    };
    n.addEvents(r), r.prototype.initialize = function(e) {
        this.state = "initial", this.data = e.data, e.url ? this.url = e.url : this.url = e.data.url, this.hoister = e.hoister, this.bind("load", this.handleLoad), this.bind("loadError", this.handleLoadError)
    }, r.prototype.handleLoad = function() {
        this.hoister.trigger("virtElLoaded", this)
    }, r.prototype.handleLoadError = function() {
        this.hoister.trigger("virtElImgNotFound", this)
    }, n.addEvents(t), t.prototype.initialize = function(e) {
        if (this.state = "initial", e.urls)
            this.urls = e.urls;
        else {
            if (!e.url_path)
                throw "Must initialize with either urls or url_path";
            this.url_path = e.url_path;
            var t = new XMLHttpRequest;
            t.open("GET", this.url_path, !1), t.send(), console.log(t.response), this.urlResp = JSON.parse(t.response), "string" === this.urlResp[0] ? this.urls = this.urlResp : this.srcObjs = this.urlResp
        }
        this.standardChunkSize = e.chunkSize || 4, this.container = e.container || document.getElementById("container"), this.append = e.append || this.defaultAppend, this.virtualElements = [], this.virtElIndex = 0, this.bind("virtElLoaded", this.elementLoaded), this.bind("virtElImgNotFound", this.elementLoaded);
        for (var n = 0; n < this.urlResp.length; n++) {
            var i;
            i = "string" == typeof this.urlResp[n] ? new r({
                url: this.urlResp[n],
                hoister: this
            }) : new r({
                data: this.urlResp[n],
                hoister: this
            }), this.virtualElements.push(i)
        }
        this.state = "ready"
    }, t.prototype.elementLoaded = function() {
        this.loadedCount += 1, this.maybeAppend()
    }, t.prototype.maybeAppend = function() {
        if (this.loadedCount === this.chunkSize) {
            for (var e = 0; e < this.chunkSize; e++) {
                var t = this.virtualElements[this.virtElIndex + e];
                this.append(t)
            }
            this.virtElIndex = this.virtElIndex + this.chunkSize, this.state = "ready", this.next()
        }
    }, t.prototype.defaultAppend = function(e) {
        this.container.appendChild(e.image)
    }, t.prototype.next = function() {
        if ("ready" !== this.state)
            return console.log("Not ready for next chunk; exiting"), !1;
        if (this.virtElIndex === this.virtualElements.length)
            return console.log("Out of elements; exiting"), !1;
        this.virtElIndex + this.standardChunkSize > this.virtualElements.length ? this.chunkSize = this.virtualElements.length - this.virtElIndex : this.chunkSize = this.standardChunkSize, this.state = "loading chunk";
        for (var e = this.loadedCount = 0; e < this.chunkSize; e++) {
            var t = this.virtualElements[this.virtElIndex + e];
            t.url ? (t.image = new Image, t.image.onload = function() {
                t.trigger("load")
            }, t.image.onerror = function() {
                t.trigger("loadError")
            }, t.image.src = t.url) : t.trigger("load")
        }
    }, t.prototype.launch = function() {
        return console.log("Launch!"), this.next(), !0
    }, e.Hoist = t
}(window), function(n) {
    "use strict";
    function i(e) {
        var t = n.event;
        return t.target = t.target || t.srcElement || e, t
    }
    var e = document.documentElement,
        t = function() {};
    e.addEventListener ? t = function(e, t, n) {
        e.addEventListener(t, n, !1)
    } : e.attachEvent && (t = function(t, e, n) {
        t[e + n] = n.handleEvent ? function() {
            var e = i(t);
            n.handleEvent.call(n, e)
        } : function() {
            var e = i(t);
            n.call(t, e)
        }, t.attachEvent("on" + e, t[e + n])
    });
    var r = function() {};
    e.removeEventListener ? r = function(e, t, n) {
        e.removeEventListener(t, n, !1)
    } : e.detachEvent && (r = function(e, t, n) {
        e.detachEvent("on" + t, e[t + n]);
        try {
            delete e[t + n]
        } catch (i) {
            e[t + n] = undefined
        }
    });
    var o = {
        bind: t,
        unbind: r
    };
    "function" == typeof define && define.amd ? define(o) : "object" == typeof exports ? module.exports = o : n.eventie = o
}(window), function() {
    "use strict";
    function e() {}
    function o(e, t) {
        for (var n = e.length; n--;)
            if (e[n].listener === t)
                return n;
        return -1
    }
    function t(e) {
        return function t() {
            return this[e].apply(this, arguments)
        }
    }
    var n = e.prototype,
        i = this,
        r = i.EventEmitter;
    n.getListeners = function s(e) {
        var t,
            n,
            i = this._getEvents();
        if (e instanceof RegExp)
            for (n in t = {}, i)
                i.hasOwnProperty(n) && e.test(n) && (t[n] = i[n]);
        else
            t = i[e] || (i[e] = []);
        return t
    }, n.flattenListeners = function a(e) {
        var t,
            n = [];
        for (t = 0; t < e.length; t += 1)
            n.push(e[t].listener);
        return n
    }, n.getListenersAsObject = function u(e) {
        var t,
            n = this.getListeners(e);
        return n instanceof Array && ((t = {})[e] = n), t || n
    }, n.addListener = function l(e, t) {
        var n,
            i = this.getListenersAsObject(e),
            r = "object" == typeof t;
        for (n in i)
            i.hasOwnProperty(n) && -1 === o(i[n], t) && i[n].push(r ? t : {
                listener: t,
                once: !1
            });
        return this
    }, n.on = t("addListener"), n.addOnceListener = function c(e, t) {
        return this.addListener(e, {
            listener: t,
            once: !0
        })
    }, n.once = t("addOnceListener"), n.defineEvent = function h(e) {
        return this.getListeners(e), this
    }, n.defineEvents = function f(e) {
        for (var t = 0; t < e.length; t += 1)
            this.defineEvent(e[t]);
        return this
    }, n.removeListener = function p(e, t) {
        var n,
            i,
            r = this.getListenersAsObject(e);
        for (i in r)
            r.hasOwnProperty(i) && -1 !== (n = o(r[i], t)) && r[i].splice(n, 1);
        return this
    }, n.off = t("removeListener"), n.addListeners = function d(e, t) {
        return this.manipulateListeners(!1, e, t)
    }, n.removeListeners = function g(e, t) {
        return this.manipulateListeners(!0, e, t)
    }, n.manipulateListeners = function m(e, t, n) {
        var i,
            r,
            o = e ? this.removeListener : this.addListener,
            s = e ? this.removeListeners : this.addListeners;
        if ("object" != typeof t || t instanceof RegExp)
            for (i = n.length; i--;)
                o.call(this, t, n[i]);
        else
            for (i in t)
                t.hasOwnProperty(i) && (r = t[i]) && ("function" == typeof r ? o.call(this, i, r) : s.call(this, i, r));
        return this
    }, n.removeEvent = function y(e) {
        var t,
            n = typeof e,
            i = this._getEvents();
        if ("string" === n)
            delete i[e];
        else if (e instanceof RegExp)
            for (t in i)
                i.hasOwnProperty(t) && e.test(t) && delete i[t];
        else
            delete this._events;
        return this
    }, n.removeAllListeners = t("removeEvent"), n.emitEvent = function v(e, t) {
        var n,
            i,
            r,
            o = this.getListenersAsObject(e);
        for (r in o)
            if (o.hasOwnProperty(r))
                for (i = o[r].length; i--;)
                    !0 === (n = o[r][i]).once && this.removeListener(e, n.listener), n.listener.apply(this, t || []) === this._getOnceReturnValue() && this.removeListener(e, n.listener);
        return this
    }, n.trigger = t("emitEvent"), n.emit = function x(e) {
        var t = Array.prototype.slice.call(arguments, 1);
        return this.emitEvent(e, t)
    }, n.setOnceReturnValue = function b(e) {
        return this._onceReturnValue = e, this
    }, n._getOnceReturnValue = function w() {
        return !this.hasOwnProperty("_onceReturnValue") || this._onceReturnValue
    }, n._getEvents = function z() {
        return this._events || (this._events = {})
    }, e.noConflict = function C() {
        return i.EventEmitter = r, e
    }, "function" == typeof define && define.amd ? define(function() {
        return e
    }) : "object" == typeof module && module.exports ? module.exports = e : i.EventEmitter = e
}.call(this), function(t) {
    "use strict";
    function n(e) {
        "function" == typeof e && (n.isReady ? e() : s.push(e))
    }
    function i(e) {
        var t = "readystatechange" === e.type && "complete" !== o.readyState;
        n.isReady || t || r()
    }
    function r() {
        n.isReady = !0;
        for (var e = 0, t = s.length; e < t; e++) {
            (0, s[e])()
        }
    }
    function e(e) {
        return "complete" === o.readyState ? r() : (e.bind(o, "DOMContentLoaded", i), e.bind(o, "readystatechange", i), e.bind(t, "load", i)), n
    }
    var o = t.document,
        s = [];
    n.isReady = !1, "function" == typeof define && define.amd ? define(["eventie/eventie"], e) : "object" == typeof exports ? module.exports = e(require("eventie")) : t.docReady = e(t.eventie)
}(window), function(r) {
    "use strict";
    function n(e, t) {
        return e[s](t)
    }
    function o(e) {
        e.parentNode || document.createDocumentFragment().appendChild(e)
    }
    function e(e, t) {
        o(e);
        for (var n = e.parentNode.querySelectorAll(t), i = 0, r = n.length; i < r; i++)
            if (n[i] === e)
                return !0;
        return !1
    }
    function t(e, t) {
        return o(e), n(e, t)
    }
    var i,
        s = function() {
            if (r.matches)
                return "matches";
            if (r.matchesSelector)
                return "matchesSelector";
            for (var e = ["webkit", "moz", "ms", "o"], t = 0, n = e.length; t < n; t++) {
                var i = e[t] + "MatchesSelector";
                if (r[i])
                    return i
            }
        }();
    if (s) {
        var a = n(document.createElement("div"), "div");
        i = a ? n : t
    } else
        i = e;
    "function" == typeof define && define.amd ? define(function() {
        return i
    }) : "object" == typeof exports ? module.exports = i : window.matchesSelector = i
}(Element.prototype), function(e) {
    "use strict";
    function t(e) {
        if (e) {
            if ("string" == typeof o[e])
                return e;
            var t;
            e = e.charAt(0).toUpperCase() + e.slice(1);
            for (var n = 0, i = r.length; n < i; n++)
                if (t = r[n] + e, "string" == typeof o[t])
                    return t
        }
    }
    var r = "Webkit Moz ms Ms O".split(" "),
        o = document.documentElement.style;
    "function" == typeof define && define.amd ? define(function() {
        return t
    }) : "object" == typeof exports ? module.exports = t : e.getStyleProperty = t
}(window), function(u) {
    "use strict";
    function C(e) {
        var t = parseFloat(e);
        return -1 === e.indexOf("%") && !isNaN(t) && t
    }
    function e() {}
    function E() {
        for (var e = {
                width: 0,
                height: 0,
                innerWidth: 0,
                innerHeight: 0,
                outerWidth: 0,
                outerHeight: 0
            }, t = 0, n = L.length; t < n; t++) {
            e[L[t]] = 0
        }
        return e
    }
    function t(s) {
        function v() {
            if (!a) {
                a = !0;
                var n,
                    t = u.getComputedStyle;
                if (n = t ? function(e) {
                    return t(e, null)
                } : function(e) {
                    return e.currentStyle
                }, b = function o(e) {
                    var t = n(e);
                    return t || l("Style returned " + t + ". Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1"), t
                }, w = s("boxSizing")) {
                    var e = document.createElement("div");
                    e.style.width = "200px", e.style.padding = "1px 2px 3px 4px", e.style.borderStyle = "solid", e.style.borderWidth = "1px 2px 3px 4px", e.style[w] = "border-box";
                    var i = document.body || document.documentElement;
                    i.appendChild(e);
                    var r = b(e);
                    z = 200 === C(r.width), i.removeChild(e)
                }
            }
        }
        function e(e) {
            if (v(), "string" == typeof e && (e = document.querySelector(e)), e && "object" == typeof e && e.nodeType) {
                var t = b(e);
                if ("none" === t.display)
                    return E();
                var n = {};
                n.width = e.offsetWidth, n.height = e.offsetHeight;
                for (var i = n.isBorderBox = !(!w || !t[w] || "border-box" !== t[w]), r = 0, o = L.length; r < o; r++) {
                    var s = L[r],
                        a = t[s];
                    a = x(e, a);
                    var u = parseFloat(a);
                    n[s] = isNaN(u) ? 0 : u
                }
                var l = n.paddingLeft + n.paddingRight,
                    c = n.paddingTop + n.paddingBottom,
                    h = n.marginLeft + n.marginRight,
                    f = n.marginTop + n.marginBottom,
                    p = n.borderLeftWidth + n.borderRightWidth,
                    d = n.borderTopWidth + n.borderBottomWidth,
                    g = i && z,
                    m = C(t.width);
                !1 !== m && (n.width = m + (g ? 0 : l + p));
                var y = C(t.height);
                return !1 !== y && (n.height = y + (g ? 0 : c + d)), n.innerWidth = n.width - (l + p), n.innerHeight = n.height - (c + d), n.outerWidth = n.width + h, n.outerHeight = n.height + f, n
            }
        }
        function x(e, t) {
            if (u.getComputedStyle || -1 === t.indexOf("%"))
                return t;
            var n = e.style,
                i = n.left,
                r = e.runtimeStyle,
                o = r && r.left;
            return o && (r.left = e.currentStyle.left), n.left = t, t = n.pixelLeft, n.left = i, o && (r.left = o), t
        }
        var b,
            w,
            z,
            a = !1;
        return e
    }
    var l = "undefined" == typeof console ? e : function(e) {
            console.error(e)
        },
        L = ["paddingLeft", "paddingRight", "paddingTop", "paddingBottom", "marginLeft", "marginRight", "marginTop", "marginBottom", "borderLeftWidth", "borderRightWidth", "borderTopWidth", "borderBottomWidth"];
    "function" == typeof define && define.amd ? define(["get-style-property/get-style-property"], t) : "object" == typeof exports ? module.exports = t(require("desandro-get-style-property")) : u.getSize = t(u.getStyleProperty)
}(window), function(e, t) {
    "use strict";
    e.fizzyUIUtils = t(e, e.docReady, e.matchesSelector)
}(window, function factory(p, e, l) {
    "use strict";
    var d = {
            extend: function(e, t) {
                for (var n in t)
                    e[n] = t[n];
                return e
            },
            modulo: function(e, t) {
                return (e % t + t) % t
            }
        },
        t = Object.prototype.toString;
    d.isArray = function(e) {
        return "[object Array]" == t.call(e)
    }, d.makeArray = function(e) {
        var t = [];
        if (d.isArray(e))
            t = e;
        else if (e && "number" == typeof e.length)
            for (var n = 0, i = e.length; n < i; n++)
                t.push(e[n]);
        else
            t.push(e);
        return t
    }, d.indexOf = Array.prototype.indexOf ? function(e, t) {
        return e.indexOf(t)
    } : function(e, t) {
        for (var n = 0, i = e.length; n < i; n++)
            if (e[n] === t)
                return n;
        return -1
    }, d.removeFrom = function(e, t) {
        var n = d.indexOf(e, t);
        -1 != n && e.splice(n, 1)
    }, d.isElement = "function" == typeof HTMLElement || "object" == typeof HTMLElement ? function n(e) {
        return e instanceof HTMLElement
    } : function i(e) {
        return e && "object" == typeof e && 1 == e.nodeType && "string" == typeof e.nodeName
    }, d.setText = function() {
        function e(e, t) {
            e[n = n || (document.documentElement.textContent !== undefined ? "textContent" : "innerText")] = t
        }
        var n;
        return e
    }(), d.getParent = function(e, t) {
        for (; e != document.body;)
            if (e = e.parentNode, l(e, t))
                return e
    }, d.getQueryElement = function(e) {
        return "string" == typeof e ? document.querySelector(e) : e
    }, d.handleEvent = function(e) {
        var t = "on" + e.type;
        this[t] && this[t](e)
    }, d.filterFindElements = function(e, t) {
        for (var n = [], i = 0, r = (e = d.makeArray(e)).length; i < r; i++) {
            var o = e[i];
            if (d.isElement(o))
                if (t) {
                    l(o, t) && n.push(o);
                    for (var s = o.querySelectorAll(t), a = 0, u = s.length; a < u; a++)
                        n.push(s[a])
                } else
                    n.push(o)
        }
        return n
    }, d.debounceMethod = function(e, t, i) {
        var r = e.prototype[t],
            o = t + "Timeout";
        e.prototype[t] = function() {
            var e = this[o];
            e && clearTimeout(e);
            var t = arguments,
                n = this;
            this[o] = setTimeout(function() {
                r.apply(n, t), delete n[o]
            }, i || 100)
        }
    }, d.toDashed = function(e) {
        return e.replace(/(.)([A-Z])/g, function(e, t, n) {
            return t + "-" + n
        }).toLowerCase()
    };
    var g = p.console;
    return d.htmlInit = function(h, f) {
        e(function() {
            for (var e = d.toDashed(f), t = document.querySelectorAll(".js-" + e), n = "data-" + e + "-options", i = 0, r = t.length; i < r; i++) {
                var o,
                    s = t[i],
                    a = s.getAttribute(n);
                try {
                    o = a && JSON.parse(a)
                } catch (c) {
                    g && g.error("Error parsing " + n + " on " + s.nodeName.toLowerCase() + (s.id ? "#" + s.id : "") + ": " + c);
                    continue
                }
                var u = new h(s, o),
                    l = p.jQuery;
                l && l.data(s, f, u)
            }
        })
    }, d
}), function(e) {
    "use strict";
    function t() {}
    function n(l) {
        function n(e) {
            e.prototype.option || (e.prototype.option = function(e) {
                l.isPlainObject(e) && (this.options = l.extend(!0, this.options, e))
            })
        }
        function i(a, u) {
            l.fn[a] = function(t) {
                if ("string" != typeof t)
                    return this.each(function() {
                        var e = l.data(this, a);
                        e ? (e.option(t), e._init()) : (e = new u(this, t), l.data(this, a, e))
                    });
                for (var e = h.call(arguments, 1), n = 0, i = this.length; n < i; n++) {
                    var r = this[n],
                        o = l.data(r, a);
                    if (o)
                        if (l.isFunction(o[t]) && "_" !== t.charAt(0)) {
                            var s = o[t].apply(o, e);
                            if (s !== undefined)
                                return s
                        } else
                            c("no such method '" + t + "' for " + a + " instance");
                    else
                        c("cannot call methods on " + a + " prior to initialization; attempted to call '" + t + "'")
                }
                return this
            }
        }
        if (l) {
            var c = "undefined" == typeof console ? t : function(e) {
                console.error(e)
            };
            return l.bridget = function(e, t) {
                n(t), i(e, t)
            }, l.bridget
        }
    }
    var h = Array.prototype.slice;
    "function" == typeof define && define.amd ? define(["jquery"], n) : "object" == typeof exports ? n(require("jquery")) : n(e.jQuery)
}(window), function(r, o) {
    "use strict";
    "function" == typeof define && define.amd ? define(["eventEmitter/EventEmitter", "get-size/get-size", "get-style-property/get-style-property", "fizzy-ui-utils/utils"], function(e, t, n, i) {
        return o(r, e, t, n, i)
    }) : "object" == typeof exports ? module.exports = o(r, require("wolfy87-eventemitter"), require("get-size"), require("desandro-get-style-property"), require("fizzy-ui-utils")) : (r.Outlayer = {}, r.Outlayer.Item = o(r, r.EventEmitter, r.getSize, r.getStyleProperty, r.fizzyUIUtils))
}(window, function factory(e, t, n, o, i) {
    "use strict";
    function r(e) {
        for (var t in e)
            return !1;
        return !null
    }
    function s(e, t) {
        e && (this.element = e, this.layout = t, this.position = {
            x: 0,
            y: 0
        }, this._create())
    }
    function a(e) {
        return e.replace(/([A-Z])/g, function(e) {
            return "-" + e.toLowerCase()
        })
    }
    var u = e.getComputedStyle,
        l = u ? function(e) {
            return u(e, null)
        } : function(e) {
            return e.currentStyle
        },
        c = o("transition"),
        h = o("transform"),
        f = c && h,
        p = !!o("perspective"),
        d = {
            WebkitTransition: "webkitTransitionEnd",
            MozTransition: "transitionend",
            OTransition: "otransitionend",
            transition: "transitionend"
        }[c],
        g = ["transform", "transition", "transitionDuration", "transitionProperty"],
        m = function() {
            for (var e = {}, t = 0, n = g.length; t < n; t++) {
                var i = g[t],
                    r = o(i);
                r && r !== i && (e[i] = r)
            }
            return e
        }();
    i.extend(s.prototype, t.prototype), s.prototype._create = function() {
        this._transn = {
            ingProperties: {},
            clean: {},
            onEnd: {}
        }
    }, s.prototype.handleEvent = function(e) {
        var t = "on" + e.type;
        this[t] && this[t](e)
    }, s.prototype.getSize = function() {
        this.size = n(this.element)
    }, s.prototype.css = function(e) {
        var t = this.element.style;
        for (var n in e) {
            t[m[n] || n] = e[n]
        }
    }, s.prototype.getPosition = function() {
        var e = l(this.element),
            t = this.layout.options,
            n = t.isOriginLeft,
            i = t.isOriginTop,
            r = e[n ? "left" : "right"],
            o = e[i ? "top" : "bottom"],
            s = this.layout.size,
            a = -1 != r.indexOf("%") ? parseFloat(r) / 100 * s.width : parseInt(r, 10),
            u = -1 != o.indexOf("%") ? parseFloat(o) / 100 * s.height : parseInt(o, 10);
        a = isNaN(a) ? 0 : a, u = isNaN(u) ? 0 : u, a -= n ? s.paddingLeft : s.paddingRight, u -= i ? s.paddingTop : s.paddingBottom, this.position.x = a, this.position.y = u
    }, s.prototype.layoutPosition = function() {
        var e = this.layout.size,
            t = this.layout.options,
            n = {},
            i = t.isOriginLeft ? "paddingLeft" : "paddingRight",
            r = t.isOriginLeft ? "left" : "right",
            o = t.isOriginLeft ? "right" : "left",
            s = this.position.x + e[i];
        n[r] = this.getXValue(s), n[o] = "";
        var a = t.isOriginTop ? "paddingTop" : "paddingBottom",
            u = t.isOriginTop ? "top" : "bottom",
            l = t.isOriginTop ? "bottom" : "top",
            c = this.position.y + e[a];
        n[u] = this.getYValue(c), n[l] = "", this.css(n), this.emitEvent("layout", [this])
    }, s.prototype.getXValue = function(e) {
        var t = this.layout.options;
        return t.percentPosition && !t.isHorizontal ? e / this.layout.size.width * 100 + "%" : e + "px"
    }, s.prototype.getYValue = function(e) {
        var t = this.layout.options;
        return t.percentPosition && t.isHorizontal ? e / this.layout.size.height * 100 + "%" : e + "px"
    }, s.prototype._transitionTo = function(e, t) {
        this.getPosition();
        var n = this.position.x,
            i = this.position.y,
            r = parseInt(e, 10),
            o = parseInt(t, 10),
            s = r === this.position.x && o === this.position.y;
        if (this.setPosition(e, t), !s || this.isTransitioning) {
            var a = e - n,
                u = t - i,
                l = {};
            l.transform = this.getTranslate(a, u), this.transition({
                to: l,
                onTransitionEnd: {
                    transform: this.layoutPosition
                },
                isCleaning: !0
            })
        } else
            this.layoutPosition()
    }, s.prototype.getTranslate = function(e, t) {
        var n = this.layout.options;
        return e = n.isOriginLeft ? e : -e, t = n.isOriginTop ? t : -t, p ? "translate3d(" + e + "px, " + t + "px, 0)" : "translate(" + e + "px, " + t + "px)"
    }, s.prototype.goTo = function(e, t) {
        this.setPosition(e, t), this.layoutPosition()
    }, s.prototype.moveTo = f ? s.prototype._transitionTo : s.prototype.goTo, s.prototype.setPosition = function(e, t) {
        this.position.x = parseFloat(e), this.position.y = parseFloat(t)
    }, s.prototype._nonTransition = function(e) {
        for (var t in this.css(e.to), e.isCleaning && this._removeStyles(e.to), e.onTransitionEnd)
            e.onTransitionEnd[t].call(this)
    }, s.prototype._transition = function(e) {
        if (parseFloat(this.layout.options.transitionDuration)) {
            var t = this._transn;
            for (var n in e.onTransitionEnd)
                t.onEnd[n] = e.onTransitionEnd[n];
            for (n in e.to)
                t.ingProperties[n] = !0, e.isCleaning && (t.clean[n] = !0);
            if (e.from) {
                this.css(e.from);
                this.element.offsetHeight;
                null
            }
            this.enableTransition(e.to), this.css(e.to), this.isTransitioning = !0
        } else
            this._nonTransition(e)
    };
    var y = "opacity," + a(m.transform || "transform");
    s.prototype.enableTransition = function() {
        this.isTransitioning || (this.css({
            transitionProperty: y,
            transitionDuration: this.layout.options.transitionDuration
        }), this.element.addEventListener(d, this, !1))
    }, s.prototype.transition = s.prototype[c ? "_transition" : "_nonTransition"], s.prototype.onwebkitTransitionEnd = function(e) {
        this.ontransitionend(e)
    }, s.prototype.onotransitionend = function(e) {
        this.ontransitionend(e)
    };
    var v = {
        "-webkit-transform": "transform",
        "-moz-transform": "transform",
        "-o-transform": "transform"
    };
    s.prototype.ontransitionend = function(e) {
        if (e.target === this.element) {
            var t = this._transn,
                n = v[e.propertyName] || e.propertyName;
            if (delete t.ingProperties[n], r(t.ingProperties) && this.disableTransition(), n in t.clean && (this.element.style[e.propertyName] = "", delete t.clean[n]), n in t.onEnd)
                t.onEnd[n].call(this), delete t.onEnd[n];
            this.emitEvent("transitionEnd", [this])
        }
    }, s.prototype.disableTransition = function() {
        this.removeTransitionStyles(), this.element.removeEventListener(d, this, !1), this.isTransitioning = !1
    }, s.prototype._removeStyles = function(e) {
        var t = {};
        for (var n in e)
            t[n] = "";
        this.css(t)
    };
    var x = {
        transitionProperty: "",
        transitionDuration: ""
    };
    return s.prototype.removeTransitionStyles = function() {
        this.css(x)
    }, s.prototype.removeElem = function() {
        this.element.parentNode.removeChild(this.element), this.css({
            display: ""
        }), this.emitEvent("remove", [this])
    }, s.prototype.remove = function() {
        if (c && parseFloat(this.layout.options.transitionDuration)) {
            var e = this;
            this.once("transitionEnd", function() {
                e.removeElem()
            }), this.hide()
        } else
            this.removeElem()
    }, s.prototype.reveal = function() {
        delete this.isHidden, this.css({
            display: ""
        });
        var e = this.layout.options,
            t = {};
        t[this.getHideRevealTransitionEndProperty("visibleStyle")] = this.onRevealTransitionEnd, this.transition({
            from: e.hiddenStyle,
            to: e.visibleStyle,
            isCleaning: !0,
            onTransitionEnd: t
        })
    }, s.prototype.onRevealTransitionEnd = function() {
        this.isHidden || this.emitEvent("reveal")
    }, s.prototype.getHideRevealTransitionEndProperty = function(e) {
        var t = this.layout.options[e];
        if (t.opacity)
            return "opacity";
        for (var n in t)
            return n
    }, s.prototype.hide = function() {
        this.isHidden = !0, this.css({
            display: ""
        });
        var e = this.layout.options,
            t = {};
        t[this.getHideRevealTransitionEndProperty("hiddenStyle")] = this.onHideTransitionEnd, this.transition({
            from: e.visibleStyle,
            to: e.hiddenStyle,
            isCleaning: !0,
            onTransitionEnd: t
        })
    }, s.prototype.onHideTransitionEnd = function() {
        this.isHidden && (this.css({
            display: "none"
        }), this.emitEvent("hide"))
    }, s.prototype.destroy = function() {
        this.css({
            position: "",
            left: "",
            right: "",
            top: "",
            bottom: "",
            transition: "",
            transform: ""
        })
    }, s
}), function(o, s) {
    "use strict";
    "function" == typeof define && define.amd ? define(["eventie/eventie", "eventEmitter/EventEmitter", "get-size/get-size", "fizzy-ui-utils/utils", "./item"], function(e, t, n, i, r) {
        return s(o, e, t, n, i, r)
    }) : "object" == typeof exports ? module.exports = s(o, require("eventie"), require("wolfy87-eventemitter"), require("get-size"), require("fizzy-ui-utils"), require("./item")) : o.Outlayer = s(o, o.eventie, o.EventEmitter, o.getSize, o.fizzyUIUtils, o.Outlayer.Item)
}(window, function factory(e, t, n, r, s, o) {
    "use strict";
    function a(e, t) {
        var n = s.getQueryElement(e);
        if (n) {
            this.element = n, l && (this.$element = l(this.element)), this.options = s.extend({}, this.constructor.defaults), this.option(t);
            var i = ++c;
            this.element.outlayerGUID = i, (h[i] = this)._create(), this.options.isInitLayout && this.layout()
        } else
            u && u.error("Bad element for " + this.constructor.namespace + ": " + (n || e))
    }
    var u = e.console,
        l = e.jQuery,
        i = function() {},
        c = 0,
        h = {};
    return a.namespace = "outlayer", a.Item = o, a.defaults = {
        containerStyle: {
            position: "relative"
        },
        isInitLayout: !0,
        isOriginLeft: !0,
        isOriginTop: !0,
        isResizeBound: !0,
        isResizingContainer: !0,
        transitionDuration: "0.4s",
        hiddenStyle: {
            opacity: 0,
            transform: "scale(0.001)"
        },
        visibleStyle: {
            opacity: 1,
            transform: "scale(1)"
        }
    }, s.extend(a.prototype, n.prototype), a.prototype.option = function(e) {
        s.extend(this.options, e)
    }, a.prototype._create = function() {
        this.reloadItems(), this.stamps = [], this.stamp(this.options.stamp), s.extend(this.element.style, this.options.containerStyle), this.options.isResizeBound && this.bindResize()
    }, a.prototype.reloadItems = function() {
        this.items = this._itemize(this.element.children)
    }, a.prototype._itemize = function(e) {
        for (var t = this._filterFindItemElements(e), n = this.constructor.Item, i = [], r = 0, o = t.length; r < o; r++) {
            var s = new n(t[r], this);
            i.push(s)
        }
        return i
    }, a.prototype._filterFindItemElements = function(e) {
        return s.filterFindElements(e, this.options.itemSelector)
    }, a.prototype.getItemElements = function() {
        for (var e = [], t = 0, n = this.items.length; t < n; t++)
            e.push(this.items[t].element);
        return e
    }, a.prototype.layout = function() {
        this._resetLayout(), this._manageStamps();
        var e = this.options.isLayoutInstant !== undefined ? this.options.isLayoutInstant : !this._isLayoutInited;
        this.layoutItems(this.items, e), this._isLayoutInited = !0
    }, a.prototype._init = a.prototype.layout, a.prototype._resetLayout = function() {
        this.getSize()
    }, a.prototype.getSize = function() {
        this.size = r(this.element)
    }, a.prototype._getMeasurement = function(e, t) {
        var n,
            i = this.options[e];
        i ? ("string" == typeof i ? n = this.element.querySelector(i) : s.isElement(i) && (n = i), this[e] = n ? r(n)[t] : i) : this[e] = 0
    }, a.prototype.layoutItems = function(e, t) {
        e = this._getItemsForLayout(e), this._layoutItems(e, t), this._postLayout()
    }, a.prototype._getItemsForLayout = function(e) {
        for (var t = [], n = 0, i = e.length; n < i; n++) {
            var r = e[n];
            r.isIgnored || t.push(r)
        }
        return t
    }, a.prototype._layoutItems = function(e, t) {
        if (this._emitCompleteOnItems("layout", e), e && e.length) {
            for (var n = [], i = 0, r = e.length; i < r; i++) {
                var o = e[i],
                    s = this._getItemLayoutPosition(o);
                s.item = o, s.isInstant = t || o.isLayoutInstant, n.push(s)
            }
            this._processLayoutQueue(n)
        }
    }, a.prototype._getItemLayoutPosition = function() {
        return {
            x: 0,
            y: 0
        }
    }, a.prototype._processLayoutQueue = function(e) {
        for (var t = 0, n = e.length; t < n; t++) {
            var i = e[t];
            this._positionItem(i.item, i.x, i.y, i.isInstant)
        }
    }, a.prototype._positionItem = function(e, t, n, i) {
        i ? e.goTo(t, n) : e.moveTo(t, n)
    }, a.prototype._postLayout = function() {
        this.resizeContainer()
    }, a.prototype.resizeContainer = function() {
        if (this.options.isResizingContainer) {
            var e = this._getContainerSize();
            e && (this._setContainerMeasure(e.width, !0), this._setContainerMeasure(e.height, !1))
        }
    }, a.prototype._getContainerSize = i, a.prototype._setContainerMeasure = function(e, t) {
        if (e !== undefined) {
            var n = this.size;
            n.isBorderBox && (e += t ? n.paddingLeft + n.paddingRight + n.borderLeftWidth + n.borderRightWidth : n.paddingBottom + n.paddingTop + n.borderTopWidth + n.borderBottomWidth), e = Math.max(e, 0), this.element.style[t ? "width" : "height"] = e + "px"
        }
    }, a.prototype._emitCompleteOnItems = function(e, t) {
        function n() {
            r.dispatchEvent(e + "Complete", null, [t])
        }
        function i() {
            ++s === o && n()
        }
        var r = this,
            o = t.length;
        if (t && o)
            for (var s = 0, a = 0, u = t.length; a < u; a++) {
                t[a].once(e, i)
            }
        else
            n()
    }, a.prototype.dispatchEvent = function(e, t, n) {
        var i = t ? [t].concat(n) : n;
        if (this.emitEvent(e, i), l)
            if (this.$element = this.$element || l(this.element), t) {
                var r = l.Event(t);
                r.type = e, this.$element.trigger(r, n)
            } else
                this.$element.trigger(e, n)
    }, a.prototype.ignore = function(e) {
        var t = this.getItem(e);
        t && (t.isIgnored = !0)
    }, a.prototype.unignore = function(e) {
        var t = this.getItem(e);
        t && delete t.isIgnored
    }, a.prototype.stamp = function(e) {
        if (e = this._find(e)) {
            this.stamps = this.stamps.concat(e);
            for (var t = 0, n = e.length; t < n; t++) {
                var i = e[t];
                this.ignore(i)
            }
        }
    }, a.prototype.unstamp = function(e) {
        if (e = this._find(e))
            for (var t = 0, n = e.length; t < n; t++) {
                var i = e[t];
                s.removeFrom(this.stamps, i), this.unignore(i)
            }
    }, a.prototype._find = function(e) {
        if (e)
            return "string" == typeof e && (e = this.element.querySelectorAll(e)), e = s.makeArray(e)
    }, a.prototype._manageStamps = function() {
        if (this.stamps && this.stamps.length) {
            this._getBoundingRect();
            for (var e = 0, t = this.stamps.length; e < t; e++) {
                var n = this.stamps[e];
                this._manageStamp(n)
            }
        }
    }, a.prototype._getBoundingRect = function() {
        var e = this.element.getBoundingClientRect(),
            t = this.size;
        this._boundingRect = {
            left: e.left + t.paddingLeft + t.borderLeftWidth,
            top: e.top + t.paddingTop + t.borderTopWidth,
            right: e.right - (t.paddingRight + t.borderRightWidth),
            bottom: e.bottom - (t.paddingBottom + t.borderBottomWidth)
        }
    }, a.prototype._manageStamp = i, a.prototype._getElementOffset = function(e) {
        var t = e.getBoundingClientRect(),
            n = this._boundingRect,
            i = r(e);
        return {
            left: t.left - n.left - i.marginLeft,
            top: t.top - n.top - i.marginTop,
            right: n.right - t.right - i.marginRight,
            bottom: n.bottom - t.bottom - i.marginBottom
        }
    }, a.prototype.handleEvent = function(e) {
        var t = "on" + e.type;
        this[t] && this[t](e)
    }, a.prototype.bindResize = function() {
        this.isResizeBound || (t.bind(e, "resize", this), this.isResizeBound = !0)
    }, a.prototype.unbindResize = function() {
        this.isResizeBound && t.unbind(e, "resize", this), this.isResizeBound = !1
    }, a.prototype.onresize = function() {
        function e() {
            t.resize(), delete t.resizeTimeout
        }
        this.resizeTimeout && clearTimeout(this.resizeTimeout);
        var t = this;
        this.resizeTimeout = setTimeout(e, 100)
    }, a.prototype.resize = function() {
        this.isResizeBound && this.needsResizeLayout() && this.layout()
    }, a.prototype.needsResizeLayout = function() {
        var e = r(this.element);
        return this.size && e && e.innerWidth !== this.size.innerWidth
    }, a.prototype.addItems = function(e) {
        var t = this._itemize(e);
        return t.length && (this.items = this.items.concat(t)), t
    }, a.prototype.appended = function(e) {
        var t = this.addItems(e);
        t.length && (this.layoutItems(t, !0), this.reveal(t))
    }, a.prototype.prepended = function(e) {
        var t = this._itemize(e);
        if (t.length) {
            var n = this.items.slice(0);
            this.items = t.concat(n), this._resetLayout(), this._manageStamps(), this.layoutItems(t, !0), this.reveal(t), this.layoutItems(n)
        }
    }, a.prototype.reveal = function(e) {
        this._emitCompleteOnItems("reveal", e);
        for (var t = e && e.length, n = 0; t && n < t; n++) {
            e[n].reveal()
        }
    }, a.prototype.hide = function(e) {
        this._emitCompleteOnItems("hide", e);
        for (var t = e && e.length, n = 0; t && n < t; n++) {
            e[n].hide()
        }
    }, a.prototype.revealItemElements = function(e) {
        var t = this.getItems(e);
        this.reveal(t)
    }, a.prototype.hideItemElements = function(e) {
        var t = this.getItems(e);
        this.hide(t)
    }, a.prototype.getItem = function(e) {
        for (var t = 0, n = this.items.length; t < n; t++) {
            var i = this.items[t];
            if (i.element === e)
                return i
        }
    }, a.prototype.getItems = function(e) {
        for (var t = [], n = 0, i = (e = s.makeArray(e)).length; n < i; n++) {
            var r = e[n],
                o = this.getItem(r);
            o && t.push(o)
        }
        return t
    }, a.prototype.remove = function(e) {
        var t = this.getItems(e);
        if (this._emitCompleteOnItems("remove", t), t && t.length)
            for (var n = 0, i = t.length; n < i; n++) {
                var r = t[n];
                r.remove(), s.removeFrom(this.items, r)
            }
    }, a.prototype.destroy = function() {
        var e = this.element.style;
        e.height = "", e.position = "", e.width = "";
        for (var t = 0, n = this.items.length; t < n; t++) {
            this.items[t].destroy()
        }
        this.unbindResize();
        var i = this.element.outlayerGUID;
        delete h[i], delete this.element.outlayerGUID, l && l.removeData(this.element, this.constructor.namespace)
    }, a.data = function(e) {
        var t = (e = s.getQueryElement(e)) && e.outlayerGUID;
        return t && h[t]
    }, a.create = function(e, t) {
        function n() {
            a.apply(this, arguments)
        }
        return Object.create ? n.prototype = Object.create(a.prototype) : s.extend(n.prototype, a.prototype), (n.prototype.constructor = n).defaults = s.extend({}, a.defaults), s.extend(n.defaults, t), n.prototype.settings = {}, n.namespace = e, n.data = a.data, n.Item = function i() {
            o.apply(this, arguments)
        }, n.Item.prototype = new o, s.htmlInit(n, e), l && l.bridget && l.bridget(e, n), n
    }, a.Item = o, a.prototype.imageWidths = [200, 400, 600, 800, 1e3, 1200, 1400, 1600], a
}), function() {
    var e,
        t,
        n;
    if (n = {
        env: "production"
    }, void 0 === i)
        var i = {};
    i.noop = function() {}, e = function(t, e) {
        var n;
        return n = t[e], function() {
            var e = Array.prototype.slice.call(arguments);
            n.apply(t, e)
        }
    }, i.loadAuthToken = function() {
        var e;
        (e = $('meta[name="auth-token"]')) && (i.authToken = e.attr("content"))
    }, i.authArgs = function() {
        return {
            Authorization: i.authToken
        }
    }, t = function(e) {
        "production" != n.env && console.log(e)
    }, window.Util = i, window.Environment = n, window.route = e, "undefined" == typeof window.maybeSay && (window.maybeSay = t)
}();
var Canvas = Outlayer.create("canvas");
Canvas.prototype.imageWidths = [200, 400, 600, 800, 1e3, 1200, 1400, 1600], Canvas.prototype.needsResizeLayout = function() {
    var e = getSize(this.element),
        t = this.size && e,
        n = this.options.isHorizontal ? "innerHeight" : "innerWidth";
    return t && e[n] !== this.size[n]
}, Canvas.prototype._resetLayout = function() {
    this.getSize(), this.options.isHorizontal ? this.getContainerHeight() : this.getContainerWidth()
}, Canvas.prototype._getItemLayoutPosition = function(e) {
    var t,
        n,
        i,
        r,
        o,
        s,
        a,
        u,
        l,
        c,
        h,
        f,
        p,
        d,
        g,
        m;
    for (e.getSize(), t = e.element.dataset, n = this.containerWidth, i = this.containerHeight, r = (o = this.options.isHorizontal) ? i : n, s = o ? t.x : t.y, a = o ? t.y : t.x, u = t.size, l = t.aspectratio, c = r * s / 100, h = r * a / 100, f = o ? i * u * l / 100 : n * u / 100, p = 0, d = null; p < this.imageWidths.length && !(f <= (d = this.imageWidths[p]));)
        p += 1;
    return Layout.retina && (d *= 2), g = t.prefix, m = t.suffix, {
        x: c,
        y: h,
        width: f,
        imgUrl: d ? g + "_w" + parseInt(d) + "." + m : g + "." + m
    }
}, Canvas.prototype.getContainerWidth = function() {
    this.containerWidth = this._getContainerDimension("innerWidth")
}, Canvas.prototype.getContainerHeight = function() {
    this.containerHeight = this._getContainerDimension("innerHeight")
}, Canvas.prototype._getContainerDimension = function(e) {
    var t = this.options.isFitWidth ? this.element.parentNode : this.element,
        n = getSize(t);
    return n && n[e]
}, Canvas.prototype._processLayoutQueue = function(e) {
    for (var t = 0, n = e.length; t < n; t++) {
        var i = e[t];
        this._positionItem(i.item, i.x, i.y, i.width, i.imgUrl, i.isInstant)
    }
}, Canvas.prototype._positionItem = function(e, t, n, i, r, o) {
    var s,
        a;
    s = {
        width: i + "px"
    }, a = e.element.getElementsByTagName("IMG")[0], o ? e.goTo(t, n) : e.moveTo(t, n), e.css(s), a && (a.src = r), e.css(s)
}, Canvas.defaults = {
    isHorizontal: !1,
    containerStyle: {},
    isInitLayout: !0,
    isOriginLeft: !0,
    isOriginTop: !0,
    isResizeBound: !0,
    isResizingContainer: !0,
    transitionDuration: "0.4s",
    hiddenStyle: {
        opacity: 0,
        transform: "scale(0.001)"
    },
    visibleStyle: {
        opacity: 1,
        transform: "scale(1)"
    }
};
var Columns = Outlayer.create("columns");
Columns.prototype.imageWidths = [200, 400, 600, 800, 1e3, 1200, 1400, 1600], Columns.prototype.needsResizeLayout = function() {
    var e = getSize(this.element),
        t = this.size && e,
        n = this.options.isHorizontal ? "innerHeight" : "innerWidth";
    return t && e[n] !== this.size[n]
}, Columns.prototype._resetLayout = function() {
    this.getSize(), this.options.isHorizontal ? this.getContainerHeight() : this.getContainerWidth()
}, Columns.prototype._getItemLayoutPosition = function(e) {
    var t,
        n,
        i,
        r,
        o;
    for (console.log("_getItemLayoutPosition"), e.getSize(), t = e.element.dataset, width = this.options.columnWidth, n = 0, i = null; n < this.imageWidths.length && !((i = this.imageWidths[n]) >= width);)
        n += 1;
    return Layout.retina && (i *= 2), r = t.prefix, o = t.suffix, {
        imgUrl: null !== i ? r + "_w" + parseInt(i) + "." + o : r + "." + o
    }
}, Columns.prototype.getContainerWidth = function() {
    this.containerWidth = this._getContainerDimension("innerWidth")
}, Columns.prototype.getContainerHeight = function() {
    this.containerHeight = this._getContainerDimension("innerHeight")
}, Columns.prototype._getContainerDimension = function(e) {
    var t = this.options.isFitWidth ? this.element.parentNode : this.element,
        n = getSize(t);
    return n && n[e]
}, Columns.prototype._processLayoutQueue = function(e) {
    for (var t = 0, n = e.length; t < n; t++) {
        var i = e[t];
        this._positionItem(i.item, i.imgUrl, i.isInstant)
    }
}, Columns.prototype._positionItem = function(e, t) {
    var n;
    (n = e.element.getElementsByTagName("IMG")[0]) && (n.src = t)
}, Columns.defaults = {
    isHorizontal: !1,
    containerStyle: {},
    isInitLayout: !0,
    isOriginLeft: !0,
    isOriginTop: !0,
    isResizeBound: !0,
    isResizingContainer: !0,
    transitionDuration: "0.4s",
    hiddenStyle: {
        opacity: 0,
        transform: "scale(0.001)"
    },
    visibleStyle: {
        opacity: 1,
        transform: "scale(1)"
    }
};
var Grid = Outlayer.create("grid");
Grid.prototype.imageWidths = [200, 400, 600, 800, 1e3, 1200, 1400, 1600], Grid.prototype.needsResizeLayout = function() {
    var e = getSize(this.element),
        t = this.size && e,
        n = this.options.isHorizontal ? "innerHeight" : "innerWidth";
    return t && e[n] !== this.size[n]
}, Grid.prototype._resetLayout = function() {
    this.getSize(), this.options.isHorizontal ? this.getContainerHeight() : this.getContainerWidth()
}, Grid.prototype._getItemLayoutPosition = function(e) {
    var t,
        n,
        i,
        r,
        o;
    for (e.getSize(), t = e.element.dataset, width = this.options.columnWidth, n = 0, i = null; n < this.imageWidths.length && !((i = this.imageWidths[n]) >= width);)
        n += 1;
    return Layout.retina && (i *= 2), r = t.prefix, o = t.suffix, {
        imgUrl: null !== i ? r + "_w" + parseInt(i) + "." + o : r + "." + o
    }
}, Grid.prototype.getContainerWidth = function() {
    this.containerWidth = this._getContainerDimension("innerWidth")
}, Grid.prototype.getContainerHeight = function() {
    this.containerHeight = this._getContainerDimension("innerHeight")
}, Grid.prototype._getContainerDimension = function(e) {
    var t = this.options.isFitWidth ? this.element.parentNode : this.element,
        n = getSize(t);
    return n && n[e]
}, Grid.prototype._processLayoutQueue = function(e) {
    for (var t = 0, n = e.length; t < n; t++) {
        var i = e[t];
        this._positionItem(i.item, i.imgUrl, i.isInstant)
    }
}, Grid.prototype._positionItem = function(e, t) {
    e.element.getElementsByTagName("IMG")[0].src = t
};
var Lightbox = {
    open: !(Grid.defaults = {
        isHorizontal: !1,
        containerStyle: {},
        isInitLayout: !0,
        isOriginLeft: !0,
        isOriginTop: !0,
        isResizeBound: !0,
        isResizingContainer: !0,
        transitionDuration: "0.4s",
        hiddenStyle: {
            opacity: 0,
            transform: "scale(0.001)"
        },
        visibleStyle: {
            opacity: 1,
            transform: "scale(1)"
        }
    }),
    locked: !1,
    initialized: !1,
    liveEl: null,
    pageElIdMatcher: /^page-el/,
    maxProportionOfVerticalHeight: .8,
    initialize: function() {
        var t = this;
        this.initialized || ($(document).click(function(e) {
            t.clickHandler.call(t, e)
        }), $(document).keydown(function(e) {
            t.keydownHandler.call(t, e)
        })), this._setUp(), this.initialized = !0
    },
    _setUp: function() {
        this.body = $("body"), this.standardContainer = $("#container"), this.lightboxContainer = $(".lightbox"), this.navIsStatic = !!this.body.attr("class").match(/nav-static/), this.navIsFixed = !!this.body.attr("class").match(/nav-fixed/), this.scrollsHorizontally = !!this.standardContainer.attr("class").match(/([^-]|^)horizontal([^-]|$)/), this.opensAndCloses = !!this.standardContainer.attr("class").match(/rows|columns|grid/), this.permanentlyOpen = !!this.standardContainer.attr("class").match(/single(?!-element)/), this.fitsInContainer = this.navIsStatic && this.permanentlyOpen, this.fillsViewport = !this.fitsInContainer, this.fillsViewport ? this.container = $(".lightbox") : this.container = $("#container"), this.contentOuterWrapper = $("#outer-wrapper"), this.enableOrDisable(), 0 !== this.container.length && (this.getImgUrls(), this.findLiveEl(), this.resize(), this.permanentlyOpen && (this.open = !0))
    },
    enableOrDisable: function() {
        var e;
        e = Math.max(document.documentElement.clientWidth, window.innerWidth || 0), this.enabled = !(e < 601)
    },
    findLiveEl: function() {
        this.liveEl = $(this.container.find(".page-element.live")[0])
    },
    clickHandler: function(e) {
        return $(e.target).is("a") ? null : parseInt($(window).width()) <= 600 ? null : this.locked ? null : (this.locked = !0, this.opensAndCloses ? this.opensAndClosesClickHandler.call(this, e) : this.permanentlyOpen && this.permanentlyOpenClickHandler.call(this, e), void (this.locked = !1))
    },
    keydownHandler: function(e) {
        if (this.enabled && !this.locked && this.open) {
            switch (this.locked = !0, e.which) {
            case 37:
                this.recede();
                break;
            case 38:
                break;
            case 39:
                this.advance();
                break;
            case 40:
            case 27:
                this.opensAndCloses && this.close();
                break;
            default:
                this.locked = !1
            }
            e.preventDefault(), this.locked = !1
        }
    },
    permanentlyOpenClickHandler: function() {
        this.advance()
    },
    opensAndClosesClickHandler: function(e) {
        var t,
            n,
            i,
            r;
        i = (t = $(e.target).closest(".page-element")).attr("id"), this.enabled && (t && i ? i.match(this.pageElIdMatcher) ? (r = i.replace(this.pageElIdMatcher, "lb-el"), n = $("#" + r), this.show(n), this.open = !0) : this.advance() : this.close())
    },
    advance: function() {
        var e,
            t,
            n,
            i;
        e = this.container.find(".page-element"), i = this.liveEl || this.container.find(".page-element.live"), n = (t = e.index(i)) + 1 >= e.length ? 0 : t + 1, _.each(e, function(e) {
            $(e).removeClass("live")
        }), this.liveEl = $(e[n]), this.resize(), this.liveEl.addClass("live"), Layout.fadeLiveImageIn()
    },
    recede: function() {
        var e,
            t,
            n,
            i;
        e = this.container.find(".page-element"), i = this.liveEl || this.container.find(".page-element.live"), n = (t = e.index(i)) <= 0 ? e.length - 1 : t - 1, _.each(e, function(e) {
            $(e).removeClass("live")
        }), this.liveEl = $(e[n]), this.resize(), this.liveEl.addClass("live"), Layout.fadeLiveImageIn()
    },
    close: function() {
        var e;
        e = this.container.find(".page-element"), _.each(e, function(e) {
            $(e).removeClass("live")
        }), this.container.hide(), Layout.fadeImagesIn(), this.open = !1
    },
    show: function(e) {
        this.container.show(), this.liveEl = $(e), this.liveEl.addClass("live"), this.resize(), Layout.fadeLiveImageIn()
    },
    resize: function() {
        var e,
            t,
            n,
            i,
            r,
            o;
        $(window).height(), e = this.container.height(), t = this.container.width(), this.fitsInContainer ? o = r = i = n = 0 : this.fillsViewport && this.navIsStatic && this.scrollsHorizontally ? (n = parseInt(this.standardContainer.css("padding-top")), i = parseInt(this.standardContainer.css("padding-bottom")), r = parseInt(this.standardContainer.css("padding-left")), o = parseInt(this.standardContainer.css("padding-right"))) : (n = parseInt(this.contentOuterWrapper.css("padding-top")), i = parseInt(this.contentOuterWrapper.css("padding-bottom")), r = parseInt(this.contentOuterWrapper.css("padding-left")), o = parseInt(this.contentOuterWrapper.css("padding-right")))
    },
    getImgUrls: function() {
        var e,
            t,
            f,
            p,
            d,
            n,
            i,
            g;
        e = (t = this.container).find(".page-element"), f = t.height(), p = t.width(), n = parseInt(this.contentOuterWrapper.css("padding-left")), i = parseInt(this.contentOuterWrapper.css("padding-right")), d = p / f, g = (p - (n + i)) / p, _.each(e, function(e) {
            var t,
                n,
                i,
                r,
                o,
                s,
                a,
                u,
                l,
                c,
                h;
            if (n = (t = $(e)).find("img")) {
                for (r = (i = t.data()).aspectratio, o = "height" === (d < r ? "width" : "height") ? f * r : p, o *= g, u = Outlayer.prototype.imageWidths, s = 0, a = null; s < u.length && !(o <= (a = u[s]));)
                    s += 1;
                Layout.retina && (a *= 2), c = i.prefix, h = i.suffix, l = a ? c + "_w" + parseInt(a) + "." + h : c + "." + h, n.attr("src", l)
            }
        })
    }
};
!function(e, t) {
    "use strict";
    "function" == typeof define && define.amd ? define(["outlayer/outlayer", "get-size/get-size", "fizzy-ui-utils/utils"], t) : "object" == typeof exports ? module.exports = t(require("outlayer"), require("get-size"), require("fizzy-ui-utils")) : e.Masonry = t(e.Outlayer, e.getSize, e.fizzyUIUtils)
}(window, function factory(e, l, c) {
    "use strict";
    var t = e.create("masonry");
    return t.prototype._resetLayout = function() {
        this.getSize(), this._getMeasurement("columnWidth", "outerWidth"), this._getMeasurement("gutter", "outerWidth"), this.measureColumns();
        var e = this.cols;
        for (this.colYs = []; e--;)
            this.colYs.push(0);
        this.maxY = 0
    }, t.prototype.measureColumns = function() {
        if (this.getContainerWidth(), !this.columnWidth) {
            var e = this.items[0],
                t = e && e.element;
            this.columnWidth = t && l(t).outerWidth || this.containerWidth
        }
        var n = this.columnWidth += this.gutter,
            i = this.containerWidth + this.gutter,
            r = i / n,
            o = n - i % n;
        r = Math[o && o < 1 ? "round" : "floor"](r), this.cols = Math.max(r, 1)
    }, t.prototype.getContainerWidth = function() {
        var e = this.options.isFitWidth ? this.element.parentNode : this.element,
            t = l(e);
        this.containerWidth = t && t.innerWidth
    }, t.prototype._getItemLayoutPosition = function(e) {
        e.getSize();
        var t = e.size.outerWidth % this.columnWidth,
            n = Math[t && t < 1 ? "round" : "ceil"](e.size.outerWidth / this.columnWidth);
        n = Math.min(n, this.cols);
        for (var i = this._getColGroup(n), r = Math.min.apply(Math, i), o = c.indexOf(i, r), s = {
                x: this.columnWidth * o,
                y: r
            }, a = r + e.size.outerHeight, u = this.cols + 1 - i.length, l = 0; l < u; l++)
            this.colYs[o + l] = a;
        return s
    }, t.prototype._getColGroup = function(e) {
        if (e < 2)
            return this.colYs;
        for (var t = [], n = this.cols + 1 - e, i = 0; i < n; i++) {
            var r = this.colYs.slice(i, i + e);
            t[i] = Math.max.apply(Math, r)
        }
        return t
    }, t.prototype._manageStamp = function(e) {
        var t = l(e),
            n = this._getElementOffset(e),
            i = this.options.isOriginLeft ? n.left : n.right,
            r = i + t.outerWidth,
            o = Math.floor(i / this.columnWidth);
        o = Math.max(0, o);
        var s = Math.floor(r / this.columnWidth);
        s -= r % this.columnWidth ? 0 : 1, s = Math.min(this.cols - 1, s);
        for (var a = (this.options.isOriginTop ? n.top : n.bottom) + t.outerHeight, u = o; u <= s; u++)
            this.colYs[u] = Math.max(a, this.colYs[u])
    }, t.prototype._getContainerSize = function() {
        this.maxY = Math.max.apply(Math, this.colYs);
        var e = {
            height: this.maxY
        };
        return this.options.isFitWidth && (e.width = this._getContainerFitWidth()), e
    }, t.prototype._getContainerFitWidth = function() {
        for (var e = 0, t = this.cols; --t && 0 === this.colYs[t];)
            e++;
        return (this.cols - e) * this.columnWidth - this.gutter
    }, t.prototype.needsResizeLayout = function() {
        var e = this.containerWidth;
        return this.getContainerWidth(), e !== this.containerWidth
    }, t.prototype._positionItem = function(e, t, n, i) {
        e.css({
            position: "absolute"
        }), i ? e.goTo(t, n) : e.moveTo(t, n)
    }, t
});
var HorizontalMasonry = Outlayer.create("horizontalMasonry");
HorizontalMasonry.prototype._resetLayout = function() {
    this.getSize(), this._getMeasurement("columnHeight", "outerHeight"), this._getMeasurement("gutter", "outerHeight"), this.measureColumns();
    var e = this.cols;
    for (this.colYs = []; e--;)
        this.colYs.push(0);
    this.maxY = 0
}, HorizontalMasonry.prototype.measureColumns = function() {
    if (this.getContainerHeight(), !this.columnHeight) {
        var e = this.items[0],
            t = e && e.element;
        this.columnHeight = t && getSize(t).outerHeight || this.containerHeight
    }
    var n = this.columnHeight += this.gutter,
        i = this.containerHeight + this.gutter,
        r = i / n,
        o = n - i % n;
    r = Math[o && o < 1 ? "round" : "floor"](r), this.cols = Math.max(r, 1)
}, HorizontalMasonry.prototype.getContainerHeight = function() {
    var e = this.options.isFitHeight ? this.element.parentNode : this.element,
        t = getSize(e);
    this.containerHeight = t && t.innerHeight
}, HorizontalMasonry.prototype._getItemLayoutPosition = function(e) {
    e.getSize();
    var t = e.size.outerHeight % this.columnHeight,
        n = Math[t && t < 1 ? "round" : "ceil"](e.size.outerHeight / this.columnHeight);
    n = Math.min(n, this.cols);
    for (var i = this._getColGroup(n), r = Math.min.apply(Math, i), o = utils.indexOf(i, r), s = {
            x: this.columnHeight * o,
            y: r
        }, a = r + e.size.outerWidth, u = this.cols + 1 - i.length, l = 0; l < u; l++)
        this.colYs[o + l] = a;
    return s
}, HorizontalMasonry.prototype._getColGroup = function(e) {
    if (e < 2)
        return this.colYs;
    for (var t = [], n = this.cols + 1 - e, i = 0; i < n; i++) {
        var r = this.colYs.slice(i, i + e);
        t[i] = Math.max.apply(Math, r)
    }
    return t
}, HorizontalMasonry.prototype._manageStamp = function(e) {
    var t = getSize(e),
        n = this._getElementOffset(e),
        i = this.options.isOriginLeft ? n.left : n.right,
        r = i + t.outerHeight,
        o = Math.floor(i / this.columnHeight);
    o = Math.max(0, o);
    var s = Math.floor(r / this.columnHeight);
    s -= r % this.columnHeight ? 0 : 1, s = Math.min(this.cols - 1, s);
    for (var a = (this.options.isOriginTop ? n.top : n.bottom) + t.outerWidth, u = o; u <= s; u++)
        this.colYs[u] = Math.max(a, this.colYs[u])
}, HorizontalMasonry.prototype._getContainerSize = function() {
    this.maxY = Math.max.apply(Math, this.colYs);
    var e = {
        width: this.maxY
    };
    return this.options.isFitHeight && (e.height = this._getContainerFitHeight()), e
}, HorizontalMasonry.prototype._getContainerFitHeight = function() {
    for (var e = 0, t = this.cols; --t && 0 === this.colYs[t];)
        e++;
    return (this.cols - e) * this.columnHeight - this.gutter
}, HorizontalMasonry.prototype.needsResizeLayout = function() {
    var e = this.containerHeight;
    return this.getContainerHeight(), e !== this.containerHeight
};
var Pile = Outlayer.create("pile");
Pile.prototype.imageWidths = [200, 400, 600, 800, 1e3, 1200, 1400, 1600], Pile.prototype.needsResizeLayout = function() {
    var e = getSize(this.element),
        t = this.size && e,
        n = this.options.isHorizontal ? "innerHeight" : "innerWidth";
    return t && e[n] !== this.size[n]
}, Pile.prototype._resetLayout = function() {
    this.getSize(), this.options.isHorizontal ? this.getContainerHeight() : this.getContainerWidth()
}, Pile.prototype._getItemLayoutPosition = function(e) {
    return data = e.element.dataset, prefix = data.prefix, suffix = data.suffix, {
        imgUrl: prefix + "." + suffix
    }
}, Pile.prototype.getContainerWidth = function() {
    this.containerWidth = this._getContainerDimension("innerWidth")
}, Pile.prototype.getContainerHeight = function() {
    this.containerHeight = this._getContainerDimension("innerHeight")
}, Pile.prototype._getContainerDimension = function(e) {
    var t = this.options.isFitWidth ? this.element.parentNode : this.element,
        n = getSize(t);
    return n && n[e]
}, Pile.prototype._processLayoutQueue = function(e) {
    for (var t = 0, n = e.length; t < n; t++) {
        var i = e[t];
        this._positionItem(i.item, i.imgUrl, i.isInstant)
    }
}, Pile.prototype._positionItem = function(e, t) {
    e.element.getElementsByTagName("IMG")[0].src = t
}, Pile.defaults = {
    isHorizontal: !1,
    containerStyle: {},
    isInitLayout: !0,
    isOriginLeft: !0,
    isOriginTop: !0,
    isResizeBound: !0,
    isResizingContainer: !0,
    transitionDuration: "0.4s",
    hiddenStyle: {
        opacity: 0,
        transform: "scale(0.001)"
    },
    visibleStyle: {
        opacity: 1,
        transform: "scale(1)"
    }
};
var Rows = Outlayer.create("rows");
Rows.prototype.imageWidths = [200, 400, 600, 800, 1e3, 1200, 1400, 1600], Rows.prototype.needsResizeLayout = function() {
    var e = getSize(this.element),
        t = this.size && e,
        n = this.options.isHorizontal ? "innerHeight" : "innerWidth";
    return t && e[n] !== this.size[n]
}, Rows.prototype._resetLayout = function() {
    this.getSize(), this.options.isHorizontal ? this.getContainerHeight() : this.getContainerWidth()
}, Rows.prototype._getItemLayoutPosition = function(e) {
    var t,
        n,
        i,
        r,
        o,
        s;
    for (e.getSize(), t = e.element.dataset, height = this.options.rowHeight, n = t.aspectratio, width = height * n, i = 0, r = null; i < this.imageWidths.length && !((r = this.imageWidths[i]) >= width);)
        i += 1;
    return Layout.retina && (r *= 2), o = t.prefix, s = t.suffix, {
        imgUrl: null !== r ? o + "_w" + parseInt(r) + "." + s : o + "." + s
    }
}, Rows.prototype.getContainerWidth = function() {
    this.containerWidth = this._getContainerDimension("innerWidth")
}, Rows.prototype.getContainerHeight = function() {
    this.containerHeight = this._getContainerDimension("innerHeight")
}, Rows.prototype._getContainerDimension = function(e) {
    var t = this.options.isFitWidth ? this.element.parentNode : this.element,
        n = getSize(t);
    return n && n[e]
}, Rows.prototype._processLayoutQueue = function(e) {
    for (var t = 0, n = e.length; t < n; t++) {
        var i = e[t];
        this._positionItem(i.item, i.imgUrl, i.isInstant)
    }
}, Rows.prototype._positionItem = function(e, t) {
    var n;
    (n = e.element.getElementsByTagName("IMG")[0]) && (n.src = t)
}, Rows.defaults = {
    isHorizontal: !1,
    containerStyle: {},
    isInitLayout: !0,
    isOriginLeft: !0,
    isOriginTop: !0,
    isResizeBound: !0,
    isResizingContainer: !0,
    transitionDuration: "0.4s",
    hiddenStyle: {
        opacity: 0,
        transform: "scale(0.001)"
    },
    visibleStyle: {
        opacity: 1,
        transform: "scale(1)"
    }
};
var SequenceOriginal = Outlayer.create("sequenceoriginal");
SequenceOriginal.prototype.needsResizeLayout = function() {
    var e = getSize(this.element),
        t = this.size && e,
        n = this.options.isHorizontal ? "innerHeight" : "innerWidth";
    return t && e[n] !== this.size[n]
}, SequenceOriginal.prototype._resetLayout = function() {
    this.getSize(), this.options.isHorizontal ? this.getContainerHeight() : this.getContainerWidth()
}, SequenceOriginal.prototype._getItemLayoutPosition = function(e) {
    var t,
        n,
        i,
        r,
        o,
        s,
        a,
        u,
        l;
    if (e.getSize(), t = e.element.dataset, n = this.containerWidth, i = this.containerHeight, r = this.options.isHorizontal ? i : n, o = t.aspectratio, this.options.isHorizontal ? width = r * o : width = r, this.options.optimizeFileSize) {
        for (s = 0, a = null; s < this.imageWidths.length && !((a = this.imageWidths[s]) >= width);)
            s += 1;
        Layout.retina && (a *= 2)
    }
    return u = t.prefix, l = t.suffix, {
        imgUrl: a ? u + "_w" + parseInt(a) + "." + l : u + "." + l
    }
}, SequenceOriginal.prototype.getContainerWidth = function() {
    this.containerWidth = this._getContainerDimension("innerWidth")
}, SequenceOriginal.prototype.getContainerHeight = function() {
    this.containerHeight = this._getContainerDimension("innerHeight")
}, SequenceOriginal.prototype._getContainerDimension = function(e) {
    var t = this.options.isFitWidth ? this.element.parentNode : this.element,
        n = getSize(t);
    return n && n[e]
}, SequenceOriginal.prototype._processLayoutQueue = function(e) {
    for (var t = 0, n = e.length; t < n; t++) {
        var i = e[t];
        this._positionItem(i.item, i.imgUrl, i.isInstant)
    }
}, SequenceOriginal.prototype._positionItem = function(e, t) {
    var n;
    (n = e.element.getElementsByTagName("IMG")[0]) && (n.src = t)
}, SequenceOriginal.defaults = {
    isHorizontal: !1,
    containerStyle: {},
    isInitLayout: !0,
    isOriginLeft: !0,
    isOriginTop: !0,
    isResizeBound: !0,
    isResizingContainer: !0,
    transitionDuration: "0.4s",
    hiddenStyle: {
        opacity: 0,
        transform: "scale(0.001)"
    },
    visibleStyle: {
        opacity: 1,
        transform: "scale(1)"
    }
};
var Sequence = Outlayer.create("sequence");
Sequence.prototype.needsResizeLayout = function() {
    var e = getSize(this.element),
        t = this.size && e,
        n = this.options.isHorizontal ? "innerHeight" : "innerWidth";
    return t && e[n] !== this.size[n]
}, Sequence.prototype._resetLayout = function() {
    this.getSize(), this.options.isHorizontal ? this.getContainerHeight() : this.getContainerWidth()
}, Sequence.prototype._getItemLayoutPosition = function(e) {
    var t,
        n,
        i,
        r,
        o,
        s,
        a,
        u,
        l,
        c,
        h;
    if (e.getSize(), t = e.element.dataset, n = this.containerWidth, i = this.containerHeight, r = this.options.isHorizontal ? i : n, o = t.aspectratio, a = (s = t.originalwidth) / o, this.options.isHorizontal ? width = r * o : width = r, this.options.isHorizontal)
        if (a <= this.containerHeight)
            l = null;
        else {
            for (u = 0, l = null; u < this.imageWidths.length && !((l = this.imageWidths[u]) >= width);)
                u += 1;
            Layout.retina && (l *= 2)
        }
    else if (s <= this.containerWidth)
        l = null;
    else {
        for (u = 0, l = null; u < this.imageWidths.length && !((l = this.imageWidths[u]) >= width);)
            u += 1;
        Layout.retina && (l *= 2)
    }
    return c = t.prefix, h = t.suffix, {
        imgUrl: l ? c + "_w" + parseInt(l) + "." + h : c + "." + h
    }
}, Sequence.prototype.getContainerWidth = function() {
    this.containerWidth = this._getContainerDimension("innerWidth")
}, Sequence.prototype.getContainerHeight = function() {
    this.containerHeight = this._getContainerDimension("innerHeight")
}, Sequence.prototype._getContainerDimension = function(e) {
    var t = this.options.isFitWidth ? this.element.parentNode : this.element,
        n = getSize(t);
    return n && n[e]
}, Sequence.prototype._processLayoutQueue = function(e) {
    for (var t = 0, n = e.length; t < n; t++) {
        var i = e[t];
        this._positionItem(i.item, i.imgUrl, i.isInstant)
    }
}, Sequence.prototype._positionItem = function(e, t) {
    var n;
    (n = e.element.getElementsByTagName("IMG")[0]) && (n.src = t)
}, Sequence.defaults = {
    isHorizontal: !1,
    containerStyle: {},
    isInitLayout: !0,
    isOriginLeft: !0,
    isOriginTop: !0,
    isResizeBound: !0,
    isResizingContainer: !0,
    transitionDuration: "0.4s",
    hiddenStyle: {
        opacity: 0,
        transform: "scale(0.001)"
    },
    visibleStyle: {
        opacity: 1,
        transform: "scale(1)"
    }
};
var ImageLoading = {
        attachEventListeners: function() {
            var n = this;
            _.each($("img"), function(e) {
                var t;
                (t = $(e)).on("load", function(e) {
                    n.loadHandler.call(n, e)
                }), t.on("error", function(e) {
                    n.errorHandler.call(n, e)
                })
            })
        },
        loadHandler: function(e) {
            var t;
            (t = $(e.target)).removeClass("srcNotLoaded"), t.addClass("srcLoaded")
        },
        errorHandler: function(e) {
            var t,
                n,
                i,
                r;
            (r = (n = (t = $(e.target)).attr("src")).match(/_w\d\d\d\d?\./)) ? (i = n.split(r).join("."), t.attr("src", i)) : (t.removeClass("srcLoaded"), t.addClass("srcError"))
        }
    },
    Layout = {
        resizing: !1,
        containerSelector: ".container",
        lightboxContainerSelector: ".lightbox-inner-wrapper",
        noop: function() {},
        detectFeatures: function() {
            var e;
            this.supportsDataSet = Modernizr.dataset, this.supportsFlexBox = Modernizr.flexbox, e = window.devicePixelRatio || window.screen.deviceXDPI / window.screen.logicalXDPI || 1, this.retina = !!(1 < e)
        },
        lockResizing: function() {
            this.resizing = !0
        },
        unLockResizing: function() {
            this.resizing = !1
        },
        findContainer: function() {
            this.container = $(this.containerSelector), this.lightboxContainer = $(this.lightboxContainerSelector), this.constrainingAxis = this.findConstrainingAxis()
        },
        findLayoutEngine: function() {
            var e,
                t,
                n,
                i,
                r;
            t = "noflexbox " + (e = this.container.attr("class").replace(/container /, "").trim()), this.selector = e, this.findWindowWidth() < 601 ? $("body").attr("class").match(/mobile-horizontal/) ? (this.initialize = this.sequenceInit, this.resizeImages = this.resizeImagesForHorizontalPortrait, this.resizeContainer = this.resizeContainerForHorizontalPortrait) : (this.initialize = this.sequenceInit, this.resizeImages = this.returnSizesToDefaults, this.resizeContainer = this.noop) : (Modernizr.flexbox ? (n = this.initializers[e], this.initialize = this[n] || this.noop, i = this.imageResizers[e], this.resizeImages = this[i] || this.noop, r = this.containerResizers[e]) : (n = this.initializers[t] || this.initializers[e], this.initialize = this[n] || this.noop, i = this.imageResizers[t] || this.imageResizers[e], this.resizeImages = this[i] || this.noop, r = this.containerResizers[t] || this.containerResizers[e]), this.resizeContainer = this[r] || this.noop), maybeSay("initialize" + this.initialize.name), maybeSay("resizeImages" + this.resizeImages.name), maybeSay("resizeContainer" + this.resizeContainer.name), maybeSay("")
        },
        findWindowWidth: function() {
            return Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
        },
        initializers: {
            "horizontal sequence near": "sequenceInit",
            "horizontal sequence center": "sequenceInit",
            "horizontal sequence far": "sequenceInit",
            "horizontal sequence full": "sequenceInit",
            "horizontal pile near": "pileInit",
            "horizontal pile center": "pileInit",
            "horizontal pile far": "pileInit",
            "horizontal pile full": "pileInit",
            "horizontal rows near": "rowsInit",
            "horizontal rows center": "rowsInit",
            "horizontal rows far": "rowsInit",
            "horizontal rows full": "rowsInit",
            "horizontal columns near": "columnsInit",
            "horizontal columns center": "columnsInit",
            "horizontal columns far": "columnsInit",
            "horizontal columns full": "columnsInit",
            "horizontal grid near": "gridInit",
            "horizontal grid center": "gridInit",
            "horizontal grid far": "gridInit",
            "horizontal grid full": "gridInit",
            "vertical sequence near": "sequenceInit",
            "vertical sequence center": "sequenceInit",
            "vertical sequence far": "sequenceInit",
            "vertical sequence full": "sequenceInit",
            "vertical pile near": "pileInit",
            "vertical pile center": "pileInit",
            "vertical pile far": "pileInit",
            "vertical pile full": "pileInit",
            "vertical rows near": "rowsInit",
            "vertical rows center": "rowsInit",
            "vertical rows far": "rowsInit",
            "vertical rows full": "rowsInit",
            "vertical columns near": "columnsInit",
            "vertical columns center": "columnsInit",
            "vertical columns far": "columnsInit",
            "vertical columns full": "columnsInit",
            "vertical grid near": "gridInit",
            "vertical grid center": "gridInit",
            "vertical grid far": "gridInit",
            "vertical grid full": "gridInit",
            "single sequence near": "slideshowOnly",
            "single sequence center": "slideshowOnly",
            "single sequence far": "slideshowOnly",
            "single sequence full": "slideshowOnly",
            "single pile near": "slideshowOnly",
            "single pile center": "slideshowOnly",
            "single pile far": "slideshowOnly",
            "single pile full": "slideshowOnly",
            "single rows near": "slideshowOnly",
            "single rows center": "slideshowOnly",
            "single rows far": "slideshowOnly",
            "single rows full": "slideshowOnly",
            "single columns near": "slideshowOnly",
            "single columns center": "slideshowOnly",
            "single columns far": "slideshowOnly",
            "single columns full": "slideshowOnly",
            "single grid near": "slideshowOnly",
            "single grid center": "slideshowOnly",
            "single grid far": "slideshowOnly",
            "single grid full": "slideshowOnly",
            "horizontal freeform": "canvasInit",
            "vertical freeform": "canvasInit"
        },
        imageResizers: {
            "horizontal sequence near": "resizeImagesForHorizontalSequence",
            "horizontal sequence center": "resizeImagesForHorizontalSequence",
            "horizontal sequence far": "resizeImagesForHorizontalSequence",
            "horizontal sequence full": "resizeImagesForHorizontalSequence",
            "horizontal pile near": "noop",
            "horizontal pile center": "noop",
            "horizontal pile far": "noop",
            "horizontal pile full": "noop",
            "horizontal rows near": "noop",
            "horizontal rows center": "noop",
            "horizontal rows far": "noop",
            "horizontal rows full": "noop",
            "horizontal columns near": "noop",
            "horizontal columns center": "noop",
            "horizontal columns far": "noop",
            "horizontal columns full": "noop",
            "horizontal grid near": "noop",
            "horizontal grid center": "noop",
            "horizontal grid far": "noop",
            "horizontal grid full": "noop",
            "vertical sequence near": "noop",
            "vertical sequence center": "noop",
            "vertical sequence far": "noop",
            "vertical sequence full": "noop",
            "vertical pile near": "noop",
            "vertical pile center": "noop",
            "vertical pile far": "noop",
            "vertical pile full": "noop",
            "vertical rows near": "noop",
            "vertical rows center": "noop",
            "vertical rows far": "noop",
            "vertical rows full": "noop",
            "vertical columns near": "noop",
            "vertical columns center": "noop",
            "vertical columns far": "noop",
            "vertical columns full": "noop",
            "vertical grid near": "noop",
            "vertical grid center": "noop",
            "vertical grid far": "noop",
            "vertical grid full": "noop",
            "horizontal freeform": "canvasResize",
            "vertical freeform": "canvasResize"
        },
        containerResizers: {
            "noflexbox horizontal sequence near": "resizeContainerForHorizontalSequence",
            "noflexbox horizontal sequence center": "resizeContainerForHorizontalSequence",
            "noflexbox horizontal sequence far": "resizeContainerForHorizontalSequence",
            "noflexbox horizontal sequence full": "resizeContainerForHorizontalSequence",
            "horizontal sequence near": "noop",
            "horizontal sequence center": "noop",
            "horizontal sequence far": "noop",
            "horizontal sequence full": "noop",
            "horizontal pile near": "noop",
            "horizontal pile center": "noop",
            "horizontal pile far": "noop",
            "horizontal pile full": "noop",
            "horizontal rows near": "resizeLightboxOnly",
            "horizontal rows center": "resizeLightboxOnly",
            "horizontal rows far": "resizeLightboxOnly",
            "horizontal rows full": "resizeLightboxOnly",
            "horizontal columns near": "resizeLightboxOnly",
            "horizontal columns center": "resizeLightboxOnly",
            "horizontal columns far": "resizeLightboxOnly",
            "horizontal columns full": "resizeLightboxOnly",
            "horizontal grid near": "resizeLightboxOnly",
            "horizontal grid center": "resizeLightboxOnly",
            "horizontal grid far": "resizeLightboxOnly",
            "horizontal grid full": "resizeLightboxOnly",
            "vertical sequence near": "noop",
            "vertical sequence center": "noop",
            "vertical sequence far": "noop",
            "vertical sequence full": "noop",
            "vertical pile near": "noop",
            "vertical pile center": "noop",
            "vertical pile far": "noop",
            "vertical pile full": "noop",
            "vertical rows near": "resizeLightboxOnly",
            "vertical rows center": "resizeLightboxOnly",
            "vertical rows far": "resizeLightboxOnly",
            "vertical rows full": "resizeLightboxOnly",
            "vertical columns near": "resizeLightboxOnly",
            "vertical columns center": "resizeLightboxOnly",
            "vertical columns far": "resizeLightboxOnly",
            "vertical columns full": "resizeLightboxOnly",
            "vertical grid near": "resizeLightboxOnly",
            "vertical grid center": "resizeLightboxOnly",
            "vertical grid far": "resizeLightboxOnly",
            "vertical grid full": "resizeLightboxOnly",
            "single sequence near": "resizeLightboxOnly",
            "single sequence center": "resizeLightboxOnly",
            "single sequence far": "resizeLightboxOnly",
            "single sequence full": "resizeLightboxOnly",
            "single pile near": "resizeLightboxOnly",
            "single pile center": "resizeLightboxOnly",
            "single pile far": "resizeLightboxOnly",
            "single pile full": "resizeLightboxOnly",
            "single rows near": "resizeLightboxOnly",
            "single rows center": "resizeLightboxOnly",
            "single rows far": "resizeLightboxOnly",
            "single rows full": "resizeLightboxOnly",
            "single columns near": "resizeLightboxOnly",
            "single columns center": "resizeLightboxOnly",
            "single columns far": "resizeLightboxOnly",
            "single columns full": "resizeLightboxOnly",
            "single grid near": "resizeLightboxOnly",
            "single grid center": "resizeLightboxOnly",
            "single grid far": "resizeLightboxOnly",
            "single grid full": "resizeLightboxOnly"
        },
        enqueueResize: function() {
            this.clearTimeout(this.timeoutHandle), this.timeoutHandle = this.setTimeout(this.resizeImages, 100)
        },
        fadeImagesIn: function() {
            $(".page-element").each(function(e, t) {
                var n = $(t);
                1 !== n.css("opacity") && n.animate({
                    opacity: 1
                })
            })
        },
        fadeLiveImageIn: function() {
            var e;
            e = $(".page-element.live"), $(".page-element").not(".live").css({
                opacity: 0
            }), e && 1 !== e.css("opacity") && e.animate({
                opacity: 1
            })
        },
        sequenceInit: function() {
            var e,
                t,
                n,
                i;
            e = this.container.attr("class").replace(/container /, ""), (t = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)) < 601 ? n = !1 : "horizontal" === this.constrainingAxis ? n = !1 : (n = !0, i = !!e.match(/horizontal sequence full/)), this.container.sequence({
                isHorizontal: n,
                itemSelector: ".page-element",
                isFitWidth: !0,
                optimizeFileSize: !0
            }), i && 600 < t ? this.resizeImagesForHorizontalSequence() : t < 601 && this.returnSizesToDefaults(), this.resizeImages(), this.fadeImagesIn()
        },
        columnsInit: function() {
            var e,
                t,
                n;
            e = this, n = parseInt($(".container").data("gutter-between")), t = "horizontal" !== this.constrainingAxis, _.each($(".page-element"), function(e) {
                var t,
                    n,
                    i,
                    r,
                    o;
                n = (t = $(e)).find("img"), r = (i = t.data()).prefix, o = i.suffix, imgUrl = r + "_w200." + o, n.attr("src", imgUrl)
            }), imagesLoaded(this.container, function() {
                t ? e.container.columns({
                    columnWidth: 200,
                    isHorizontal: t,
                    itemSelector: ".page-element",
                    isFitWidth: !0,
                    gutter: n
                }) : e.container.masonry({
                    columnWidth: 200,
                    itemSelector: ".page-element",
                    isFitWidth: !0,
                    gutter: n
                }), e.fadeImagesIn()
            })
        },
        rowsInit: function() {
            var e;
            e = "horizontal" !== this.constrainingAxis, this.container.rows({
                rowHeight: 200,
                isHorizontal: e,
                itemSelector: ".page-element",
                isFitWidth: !0,
                gutter: 80
            }), this.fadeImagesIn()
        },
        gridInit: function() {
            var e;
            e = "horizontal" !== this.constrainingAxis, this.container.rows({
                rowHeight: 200,
                columnWidth: 200,
                isHorizontal: e,
                itemSelector: ".page-element",
                isFitWidth: !0,
                gutter: 80
            }), this.fadeImagesIn()
        },
        pileInit: function() {
            this.container.pile({
                itemSelector: ".page-element",
                isFitWidth: !0,
                gutter: 80
            }), this.fadeImagesIn()
        },
        masonryForHorizontalColumns: function() {
            $(".container").data("gutter-between");
            this.container.masonry({
                columnWidth: 200,
                itemSelector: ".page-element",
                isFitWidth: !0,
                gutter: 20
            })
        },
        masonryForVerticalColumns: function() {
            var e = parseInt($(".container").data("gutter-between"));
            this.container.masonry({
                columnWidth: 200,
                itemSelector: ".page-element",
                isFitWidth: !0,
                gutter: e
            })
        },
        masonryForVerticalColumnsFull: function() {
            var e = parseInt($(".container").data("gutter-between"));
            this.container.masonry({
                columnWidth: 200,
                itemSelector: ".page-element",
                isFitWidth: !0,
                gutter: e
            })
        },
        slideshowOnly: function() {
            Lightbox.initialize(), this.fadeLiveImageIn()
        },
        canvasInit: function() {
            var e;
            e = "horizontal" !== this.constrainingAxis, this.container.canvas({
                itemSelector: ".page-element",
                isHorizontal: e
            }), this.hasCanvas = !0, this.fadeImagesIn()
        },
        findConstrainingAxis: function() {
            return $("#container").attr("class").match(/horizontal/) ? "vertical" : "horizontal"
        },
        returnSizesToDefaults: function() {
            this.resizing || (this.lockResizing(), $(".container").width(""), this.hasCanvas && (maybeSay("Destroying Canvas"), this.container.canvas("destroy"), this.hasCanvas = !1), $(".page-element.image").each(function(e, t) {
                var n;
                (n = $(t)).height(""), n.width(""), $(n.find("img")).height(""), n.css({
                    position: "",
                    opacity: 1
                })
            }), $(".page-element.video").each(function(e, t) {
                var n;
                (n = $(t)).height(""), n.width(""), n.css({
                    position: "",
                    opacity: 1
                }), n.find("iframe").css({
                    position: "absolute",
                    top: 0,
                    left: 0,
                    width: "100%",
                    height: "100%",
                    opacity: 1
                })
            }), $(".page-element.text-block").each(function(e, t) {
                var n;
                (n = $(t)).height(""), n.width(""), n.css({
                    position: "",
                    opacity: 1
                })
            }), this.unLockResizing())
        },
        resizeImagesForHorizontalSequence: function() {
            var o,
                e,
                t,
                n,
                i,
                r,
                s,
                a,
                u,
                l;
            this.resizing || (this.lockResizing(), 0, 10, n = $(".outer-wrapper"), (t = (e = $(".container")).attr("class")).match(/full/) ? (o = t.match(/horizontal/), t.match(/vertical/)) : o = !1, i = $(window).height(), (r = $($("div.image")[0])).length < 1 && (e.append($('<div class="page-element image"><div class="element-inner-wrapper image"><img alt="" class="srcNotLoaded" /><div class="image-caption caption"></div></div></div>')), r = $($("div.image")[0])), s = parseInt(r.css("margin-bottom")), a = e.hasClass("captions-true") ? 50 : 0, u = _.reduce(["padding-top", "margin-top", "padding-bottom", "margin-bottom"], function(e, t) {
                return e += parseInt(n.css(t))
            }, 0), l = i - (u + s + 8 + a), $(".page-element.image").each(function(e, t) {
                var n,
                    i,
                    r;
                n = $(t), r = $(t).data("aspectratio"), i = $(t).data("originalwidth"), l < i / r || o ? (n.height(l), $(n.find("img")).height(l)) : (n.height("auto"), $(n.find("img")).height("auto"))
            }), $(".page-element.text-block").each(function(e, t) {
                $(t).height(l)
            }), $(".page-element.video").each(function(e, t) {
                var n,
                    i;
                o && (n = $(t).find("iframe")) && (i = 16 * l / 9, n.height(l), n.width(i))
            }), this.unLockResizing())
        },
        resizeImagesForHorizontalPortrait: function() {
            var i,
                e,
                n,
                t;
            this.resizing || (this.lockResizing(), i = 0, n = $(".outer-wrapper"), e = $(window).width(), t = _.reduce(["padding-left", "margin-left", "padding-right", "margin-right"], function(e, t) {
                return e += parseInt(n.css(t))
            }, 0), i = e - t, $(".page-element.image").each(function(e, t) {
                var n;
                (n = $(t)).width(i), $(n.find("img")).width(i)
            }), $(".page-element.text-block").each(function(e, t) {
                $(t).width(i)
            }), $(".page-element.video").each(function(e, t) {
                var n;
                (n = $(t).find("iframe")) && (height = 9 * i / 16, n.height(height), n.width(i))
            }), this.unLockResizing())
        },
        canvasResize: function() {
            this.hasCanvas || this.canvasInit()
        },
        resizeContainerForHorizontalSequence: function() {
            var r,
                o;
            r = $(".container"), imagesLoaded(r, function() {
                var e,
                    t,
                    n,
                    i;
                e = $(".page-element.image"), t = $(".page-element.text"), n = $(e[0] || t[0]), i = parseInt(n.css("margin-right")) || 0, o = _.reduce(e, function(e, t) {
                    return e += $(t).width() + i
                }, 0), textWidth = _.reduce(t, function(e, t) {
                    return e += $(t).width() + i
                }, 0), newWidth = o + textWidth, r.width(newWidth), $("body").width(newWidth + 10)
            })
        },
        resizeContainerForHorizontalPortrait: function() {
            var e,
                t,
                n,
                i;
            this.supportsFlexBox || (e = $(".container"), t = $("body"), n = $(".page-element").length * $(window).width(), i = 10, e.width(n), t.width(n + i))
        },
        resizeLightboxOnly: function() {
            Lightbox.resize()
        }
    },
    MobileNav = {
        initialize: function() {
            var t = this;
            this.isOpen = !1, this.isFrozen = !1, this.$container = $("#mobile-nav-wrapper"), this.$header = $(".mobile-header"), this.$body = $("#mobile-nav"), this.$header.click(function(e) {
                t.toggle.call(t, e)
            })
        },
        toggle: function() {
            this.isOpen ? this.close() : this.open()
        },
        open: function() {
            this.isFrozen || (this.isFrozen = !0, this.$container.addClass("nav-open"), this.$body.show(), this.isOpen = !0, this.isFrozen = !1)
        },
        close: function() {
            this.isFrozen || (this.isFrozen = !0, this.$body.hide(), this.$container.removeClass("nav-open"), this.isOpen = !1, this.isFrozen = !1)
        }
    },
    Pages = {
        containerSelector: ".container",
        findContainer: function() {
            this.container = $(this.containerSelector)
        },
        index: function() {},
        show: function() {
            var e,
                t = this;
            e = window.location.pathname, ImageLoading.attachEventListeners(), this.runSetUp(), imagesLoaded(this.container, function() {
                Lightbox.initialize()
            }), MobileNav.initialize(), this.lastWindowWidth = Layout.findWindowWidth(), $(window).resize(function() {
                var e = Layout.findWindowWidth();
                this.lastWindowWidth < 601 && !(e < 601) ? (maybeSay("Changing layout engine!"), t.runSetUp.call(t)) : !(this.lastWindowWidth < 601) && e < 601 && (maybeSay("Changing layout engine!"), t.runSetUp.call(t)), this.lastWindowWidth = e
            })
        },
        runSetUp: function() {
            this.findContainer(), Layout.detectFeatures(), Layout.findContainer(), Layout.findLayoutEngine(), Layout.initialize()
        },
        lbInfo: function() {
            var e;
            (e = $(".lightbox")).width(), e.height()
        }
    };
$(function() {
    page("/:slug", route(Pages, "show")), page("/", route(Pages, "show")), page({
        click: !1
    })
});

